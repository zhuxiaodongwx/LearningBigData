package com.atguigu.collect.flume.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * @author leon
 * @ClassName JsonUtil.java
 * @createTime 2022年08月16日 16:38:00
 */
public class JsonUtil {
    public static Boolean isJSONValidate(String message){

        try {
            JSON.parseObject(message);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
