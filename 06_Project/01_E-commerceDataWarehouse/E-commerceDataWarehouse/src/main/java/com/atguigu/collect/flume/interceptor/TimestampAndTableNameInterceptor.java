package com.atguigu.collect.flume.interceptor;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author 姜来
 * @ClassName TimestampAndTableNameInterceptor.java
 * @createTime 2022年08月20日 14:28:00
 */
public class TimestampAndTableNameInterceptor implements Interceptor {
    @Override
    public void initialize() {

    }

    /**
     * 单个事件拦截
     * @param event
     * 获取数据，转换为json对象，取出表名和时间戳，插入到每个事件的头部
     * @return
     */
    @Override
    public Event intercept(Event event) {
        // 1. 获取body转换为string
        String message = new String(event.getBody(), StandardCharsets.UTF_8);
        // 2. 创建json对象
        JSONObject jsonObject = JSON.parseObject(message);
        // 3. 获取表名和时间戳
        String tableName = jsonObject.getString("table");
        String timeStamp = jsonObject.getString("ts") + "000";
        // 4. 插入头部
        event.getHeaders().put("tableName",tableName);
        event.getHeaders().put("timestamp",timeStamp);
        return event;
    }

    @Override
    public List<Event> intercept(List<Event> events) {
        for (Event event : events) {
            intercept(event);
        }
        return events;
    }

    @Override
    public void close() {

    }

    public static class Builder implements Interceptor.Builder{
        @Override
        public Interceptor build() {
            return new TimestampAndTableNameInterceptor();
        }

        @Override
        public void configure(Context context) {

        }
    }

}
