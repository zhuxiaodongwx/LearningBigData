package com.atguigu.collect.flume.interceptor;

import com.atguigu.collect.flume.util.JsonUtil;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;

import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

/**
 * @author leon
 * @ClassName ETLInterceptor.java
 * @createTime 2022年08月16日 16:35:00
 */
public class ETLInterceptor implements Interceptor {
    @Override
    public void initialize() {

    }

    /**
     * 拦截单个事件
     * @param event
     *  判断Event中的数据是否是符合json格式的字符串
     * @return
     */
    @Override
    public Event intercept(Event event) {
        // 1. 获取数据
        String message = new String(event.getBody(),StandardCharsets.UTF_8);

        // 2. 判断是否符合json格式
        if(JsonUtil.isJSONValidate(message)){
            event.getHeaders().put("isjson","true");
        }else{
            event.getHeaders().put("isjson","false");
        }
        return event;
    }

    /**
     * 拦截批量事件
     * @param events
     * 移除掉头部中“isjson”的值是false的Event
     * @return
     */
    @Override
    public List<Event> intercept(List<Event> events) {
        // 1. 获取迭代器
        Iterator<Event> iterator = events.iterator();
        // 2. 遍历
        while (iterator.hasNext()){
            // 3. 获取Event
            Event next = iterator.next();
            // TODO 我们的严重错误
            Event event = intercept(next);
            // 4. 判断
            if(event.getHeaders().get("isjson").equals("false")){
                // 5. 移除
                iterator.remove();
            }
        }
        return events;
    }

    @Override
    public void close() {

    }

    public static class Builder implements Interceptor.Builder{
        @Override
        public Interceptor build() {
            return new ETLInterceptor();
        }

        @Override
        public void configure(Context context) {

        }
    }
}
