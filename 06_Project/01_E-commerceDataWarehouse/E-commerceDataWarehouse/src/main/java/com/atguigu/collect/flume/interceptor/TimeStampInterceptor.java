package com.atguigu.collect.flume.interceptor;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author leon
 * @ClassName TimeStampInterceptor.java
 * @createTime 2022年08月19日 10:26:00
 */
public class TimeStampInterceptor implements Interceptor {
    @Override
    public void initialize() {

    }

    /**
     * 拦截单个事件
     * @param event
     * 将数据中的时间戳获取并插入到header中
     * @return
     */
    @Override
    public Event intercept(Event event) {
        // 1. 获取数据
        String message = new String(event.getBody(), StandardCharsets.UTF_8);
        // 2. 根据字符串创建json对象
        JSONObject jsonObject = JSON.parseObject(message);
        // 3. 通过json对象获取时间戳
        String ts = jsonObject.getString("ts");
        // 4. 向header中插入属性
        event.getHeaders().put("timestamp",ts);
        return event;
    }

    /**
     * 拦截批量事件
     * @param events
     * @return
     */
    @Override
    public List<Event> intercept(List<Event> events) {
        for (Event event : events) {
            intercept(event);
        }
        return events;
    }

    @Override
    public void close() {

    }

    public static class Builder implements Interceptor.Builder{
        @Override
        public Interceptor build() {
            return new TimeStampInterceptor();
        }

        @Override
        public void configure(Context context) {

        }
    }

}
