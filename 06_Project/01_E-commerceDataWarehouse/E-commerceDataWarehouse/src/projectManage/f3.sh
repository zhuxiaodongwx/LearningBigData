#!/bin/bash

# 数据库增量业务数据，从kafkka采集到HDFS

# 1. 判断是否存在参数
if [ $# == 0 ]; then
  echo -e "请输入参数：\nstart   启动日志采集flume；\nstop   关闭日志采集flume；" && exit
fi

case $1 in
"start")
  {
    flume_count=$(jps -ml | grep kafka_to_hdfs_db | wc -l)
    if [ $flume_count != 0 ]; then
      echo "业务采集flume在运行"
    else
      echo " --------启动 hadoop104 业务数据flume-------"
      ssh hadoop104 "nohup /opt/module/flume/bin/flume-ng agent -n a1 -c /opt/module/flume/conf -f /opt/module/flume/job/kafka_to_hdfs_db.conf >/dev/null 2>&1 &"
    fi
  }
  ;;
"stop")
  {
    flume_count=$(jps -ml | grep kafka_to_hdfs_db | wc -l)
    if [ $flume_count != 0 ]; then
      echo " --------停止 hadoop104 业务数据flume-------"
      ssh hadoop104 "ps -ef | grep kafka_to_hdfs_db.conf | grep -v grep |awk '{print \$2}' | xargs -n1 kill"
    else
      echo "$host 业务采集flume没有运行"
    fi
  }

  ;;
esac
