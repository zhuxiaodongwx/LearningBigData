#!/bin/bash

# 管理数据采集通道

# 1. 判断是否存在参数
if [ $# == 0 ]; then
  echo -e "请输入参数：\nstart   启动全部数据采集通道；\nstop   关闭全部数据采集通道；" && exit
fi

case $1 in
"start") {
  echo ================== 启动 集群 ==================

  #        #启动 Zookeeper集群
  #        zk.sh start
  #
  #        #启动 Hadoop集群
  #        hadoop.sh start
  #
  #        #启动 Kafka采集集群
  #        kafka.sh start

  echo "启动采集 Flume"
  #启动采集 Flume
  f1.sh start

  echo "启动日志消费 Flume"
  #启动日志消费 Flume
  f2.sh start

  echo "启动业务消费 Flume"
  #启动业务消费 Flume
  f3.sh start

  #启动 maxwell
  maxwell.sh start

} ;;
"stop") {
  echo ================== 停止 集群 ==================

  #停止 Maxwell
  maxwell.sh stop

  #停止 业务消费Flume
  f3.sh stop

  #停止 日志消费Flume
  f2.sh stop

  #停止 日志采集Flume
  f1.sh stop

  #        #停止 Kafka采集集群
  #        kafka.sh stop
  #
  #        #停止 Hadoop集群
  #        hadoop.sh stop
  #
  #        #停止 Zookeeper集群
  #        zk.sh stop

} ;;
esac
