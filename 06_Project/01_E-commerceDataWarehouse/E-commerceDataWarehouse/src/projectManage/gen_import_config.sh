#!/bin/bash
# 批量生成maxwell的json的工作配置文件

python /opt/module/projectManage/gen_import_config.py -d gmall -t activity_info
python /opt/module/projectManage/gen_import_config.py -d gmall -t activity_rule
python /opt/module/projectManage/gen_import_config.py -d gmall -t base_category1
python /opt/module/projectManage/gen_import_config.py -d gmall -t base_category2
python /opt/module/projectManage/gen_import_config.py -d gmall -t base_category3
python /opt/module/projectManage/gen_import_config.py -d gmall -t base_dic
python /opt/module/projectManage/gen_import_config.py -d gmall -t base_province
python /opt/module/projectManage/gen_import_config.py -d gmall -t base_region
python /opt/module/projectManage/gen_import_config.py -d gmall -t base_trademark
python /opt/module/projectManage/gen_import_config.py -d gmall -t cart_info
python /opt/module/projectManage/gen_import_config.py -d gmall -t coupon_info
python /opt/module/projectManage/gen_import_config.py -d gmall -t sku_attr_value
python /opt/module/projectManage/gen_import_config.py -d gmall -t sku_info
python /opt/module/projectManage/gen_import_config.py -d gmall -t sku_sale_attr_value
python /opt/module/projectManage/gen_import_config.py -d gmall -t spu_info
