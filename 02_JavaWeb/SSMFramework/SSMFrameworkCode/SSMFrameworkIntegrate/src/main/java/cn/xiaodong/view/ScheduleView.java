package cn.xiaodong.view;

import cn.xiaodong.bean.Schedule;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * ScheduleView类用于展示任务的视图。
 * 它是一个简单的数据传输对象（DTO），包含了任务的基本信息。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleView {
    /**
     * 任务的唯一标识符。
     */
    private Integer id;

    /**
     * 任务的标题。
     */
    private String title;

    /**
     * 任务的完成状态。
     * true表示已完成，false表示未完成。
     */
    private Boolean completed;

    /**
     * 从Schedule对象创建一个ScheduleView对象。
     * 这个静态方法提供了从业务对象到展示对象的转换。
     *
     * @param schedule 任务业务对象，包含了任务的详细信息。
     * @return ScheduleView对象，包含了用于展示的任务信息。
     */
    public static ScheduleView fromSchedule(Schedule schedule) {
        return new ScheduleView(schedule.getId(), schedule.getTitle(), schedule.getCompleted() == 1);
    }

    /**
     * 从Schedule列表创建一个ScheduleView列表。
     * 这个方法通过流操作将业务对象列表转换为展示对象列表，提高了转换的效率和可读性。
     *
     * @param scheduleList 任务业务对象列表，包含了多个任务的详细信息。
     * @return ScheduleView列表，包含了用于展示的多个任务信息。
     */
    public static List<ScheduleView> fromScheduleList(List<Schedule> scheduleList) {
        return scheduleList.stream().map(ScheduleView::fromSchedule).toList();
    }
}
