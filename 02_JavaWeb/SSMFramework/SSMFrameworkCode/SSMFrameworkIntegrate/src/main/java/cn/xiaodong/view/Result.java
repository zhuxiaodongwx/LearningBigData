/**
 * 结果集封装类，用于接口响应的统一格式化。
 * 包含接口返回的状态码、操作成功标识以及数据部分。
 */
package cn.xiaodong.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 接口响应Jason 封装类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {

    /**
     * 状态码，用于表示接口操作的状态，如200表示成功。
     */
    private int code;

    /**
     * 操作标志，用于布尔类型表示操作是否成功。
     */
    private Boolean flag;

    /**
     * 数据部分，接口返回的具体数据内容。
     */
    private Object data;

    /**
     * 构造一个表示成功结果的对象。
     *
     * @param data 成功时返回的数据。
     * @return 成功结果的实例，包含状态码200和成功标识true。
     */
    public static Result success(Object data) {
        return new Result(200, true, data);
    }

    public static Result fail(Object data) {
        return new Result(200, false, data);
    }
}
