package cn.xiaodong.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 封装分页数据
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageData {

    /**
     * 每页数据条数 页大小
     */
    private int pageSize;

    /**
     * 总记录数
     */
    private int total;

    /**
     * 当前页码
     */
    private int currentPage;

    /**
     * 本页数据
     */
    private Object data;
}
