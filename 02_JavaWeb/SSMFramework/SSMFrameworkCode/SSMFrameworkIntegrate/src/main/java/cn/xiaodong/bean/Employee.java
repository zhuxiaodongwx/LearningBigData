package cn.xiaodong.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 员工类，用于表示公司的员工信息。
 * 包含员工的唯一标识、姓名和薪水信息。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    /**
     * 员工的唯一标识符。
     */
    private Integer empId;

    /**
     * 员工的姓名。
     */
    private String empName;

    /**
     * 员工的薪水。
     */
    private Double empSalary;
}
