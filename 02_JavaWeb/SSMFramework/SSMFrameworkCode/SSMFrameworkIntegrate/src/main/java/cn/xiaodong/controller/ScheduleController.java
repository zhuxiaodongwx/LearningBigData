package cn.xiaodong.controller;

import cn.xiaodong.bean.Schedule;
import cn.xiaodong.service.ScheduleService;
import cn.xiaodong.view.PageData;
import cn.xiaodong.view.Result;
import cn.xiaodong.view.ScheduleView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 学习计划操作
 */
@Controller
@ResponseBody
@RequestMapping("/schedule")
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    /**
     * 学习计划分页查询
     */
    @GetMapping("/{pageSize}/{currentPage}")
    public Result getAllScheduleByPage(@PathVariable int pageSize, @PathVariable int currentPage) {
        PageData pageData = scheduleService.getAllScheduleByPage(pageSize, currentPage);

        return Result.success(pageData);
    }

    /**
     * 根据id删除学习计划
     */
    @DeleteMapping ("/{id}")
    public Result delScheduleById(@PathVariable int id) {
        Integer num = scheduleService.delScheduleById(id);
        if (num > 0){
            return Result.success(null);
        }else {
            return Result.fail(null);
        }
    }

    /**
     * 学习计划保存
     */
    @PostMapping
    public Result addSchedule(@RequestBody ScheduleView schedule) {

        Schedule newSchedule = Schedule.fromScheduleView(schedule);
        Integer num = scheduleService.addSchedule(newSchedule);
        if (num > 0){
            return Result.success(null);
        }else {
            return Result.fail(null);
        }
    }

    /**
     * 学习计划修改
     */
    @PutMapping
    public Result putSchedule(@RequestBody ScheduleView schedule) {

        Schedule newSchedule = Schedule.fromScheduleView(schedule);
        Integer num = scheduleService.putSchedule(newSchedule);
        if (num > 0){
            return Result.success(null);
        }else {
            return Result.fail(null);
        }
    }

}
