package cn.xiaodong.service;

import cn.xiaodong.bean.Employee;
import cn.xiaodong.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeMapper employeeMapper;


    /**
     * 查询所有员工
     * @return
     */
    public List<Employee> getAllEmployee() {
        List<Employee> employees = employeeMapper.selectAll();
        return employees;
    }
}
