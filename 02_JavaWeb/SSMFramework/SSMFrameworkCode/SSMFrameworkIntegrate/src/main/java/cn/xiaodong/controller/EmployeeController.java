package cn.xiaodong.controller;

import cn.xiaodong.bean.Employee;
import cn.xiaodong.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    /**
     * 获取所有员工
     * http://localhost:8080/SSMFrameworkIntegrate/employee/getAll
     */
    @RequestMapping("/getAll")
    public List<Employee> getAllEmployee() {
        List<Employee> allEmployee = employeeService.getAllEmployee();
        return allEmployee;
    }
}
