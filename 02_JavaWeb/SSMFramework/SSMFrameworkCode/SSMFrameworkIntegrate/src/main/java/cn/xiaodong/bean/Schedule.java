package cn.xiaodong.bean;

import cn.xiaodong.view.ScheduleView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Schedule类代表了一个待完成的任务或调度项。
 * 它包含了任务的唯一标识、标题以及完成状态。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Schedule {
    /**
     * 任务的唯一标识符。
     */
    private Integer id;

    /**
     * 任务的标题或名称。
     */
    private String title;

    /**
     * 任务的完成状态。
     * 通常，1表示已完成，0表示未完成。
     */
    private Integer completed;

    public static Schedule fromScheduleView(ScheduleView scheduleView)
    {
        Schedule schedule = new Schedule();
        schedule.setId(scheduleView.getId());
        schedule.setTitle(scheduleView.getTitle());
        schedule.setCompleted(scheduleView.getCompleted() ? 1 : 0);
        return schedule;
    }
}
