package cn.xiaodong.mapper;

import java.util.List;

import cn.xiaodong.bean.Schedule;


/**
 * 本接口用于数据库中Schedule表的操作。
 * 提供了对Schedule表的增删改查等基本操作。
 */
public interface ScheduleMapper {

    /**
     * 根据主键删除学习计划。
     *
     * @param id 要删除的记录的主键。
     * @return 删除的行数。
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入一个新的学习计划。
     *
     * @param record 新的学习计划。
     * @return 插入的行数。
     */
    int insert(Schedule record);

    /**
     * 选择性插入一个新的学习计划。
     *
     * @param record 新的学习计划，只插入非空字段。
     * @return 插入的行数。
     */
    int insertSelective(Schedule record);

    /**
     * 根据主键查询一个学习计划。
     *
     * @param id 要查询的记录的主键。
     * @return 查询到的学习计划。
     */
    Schedule selectByPrimaryKey(Integer id);

    /**
     * 根据主键选择性更新学习计划。
     *
     * @param record 更新后的学习计划，只更新非空字段。
     * @return 更新的行数。
     */
    int updateByPrimaryKeySelective(Schedule record);

    /**
     * 根据主键更新学习计划。
     *
     * @param record 更新后的学习计划。
     * @return 更新的行数。
     */
    int updateByPrimaryKey(Schedule record);

    /**
     * 获取所有学习计划。
     *
     * @return 所有学习计划的列表。
     */
    List<Schedule> getAll();
}