package cn.xiaodong.service;

import cn.xiaodong.bean.Schedule;
import cn.xiaodong.mapper.ScheduleMapper;
import cn.xiaodong.view.PageData;
import cn.xiaodong.view.ScheduleView;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * 学习计划服务类
 */
@Service
public class ScheduleService {

    @Autowired
    ScheduleMapper scheduleMapper;

    /**
     * 学习计划分页查询
     */
    public PageData getAllScheduleByPage(int pageSize, int currentPage) {

        // 分页查询，设置分页条件
        PageHelper.startPage(currentPage, pageSize);
        List<Schedule> all = scheduleMapper.getAll();

        // 根据查询结果，获取分页信息
        PageInfo<Schedule> ordersPageInfo = new PageInfo<>(all);

        // 封装分页信息
        PageData pageData = new PageData();
        pageData.setCurrentPage(ordersPageInfo.getPageNum());
        pageData.setTotal((int) ordersPageInfo.getTotal());
        pageData.setPageSize(ordersPageInfo.getPageSize());
        List<ScheduleView> scheduleViews = ScheduleView.fromScheduleList(all);
        pageData.setData(scheduleViews);

        return pageData;
    }


    /**
     * 根据id删除学习计划
     * @param id 主键id
     * @return
     */
    public Integer delScheduleById(int id) {
        int i = scheduleMapper.deleteByPrimaryKey(id);
        return i;
    }

    /**
     * 新增学习计划
     * @param newSchedule 新的学习计划
     * @return
     */
    public Integer addSchedule(Schedule newSchedule) {
        int i = scheduleMapper.insertSelective(newSchedule);
        return i;
    }

    /**
     * 修改学习计划
     * @param schedule  修改的学习计划
     * @return
     */
    public Integer putSchedule(Schedule schedule) {
        int i = scheduleMapper.updateByPrimaryKeySelective(schedule);
        return i;
    }
}
