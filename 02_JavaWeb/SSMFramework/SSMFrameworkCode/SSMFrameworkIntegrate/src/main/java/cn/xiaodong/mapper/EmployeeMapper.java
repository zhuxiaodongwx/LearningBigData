package cn.xiaodong.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

import cn.xiaodong.bean.Employee;

/**
 * 员工数据操作接口。
 * 该接口定义了对员工表进行增、删、改、查等操作的方法。
 */
public interface EmployeeMapper {
    /**
     * 根据员工ID删除员工记录。
     *
     * @param empId 员工ID，作为删除条件。
     * @return 影响的行数，表示删除的记录数。
     */
    int deleteByPrimaryKey(Integer empId);

    /**
     * 插入一个新的员工记录。
     *
     * @param record 员工对象，包含所有字段信息。
     * @return 影响的行数，通常为1，表示插入了一条记录。
     */
    int insert(Employee record);

    /**
     * 选择性插入一个新的员工记录。
     * 仅插入非空字段，可以用于优化插入操作。
     *
     * @param record 员工对象，包含待插入的字段信息。
     * @return 影响的行数，通常为1，表示插入了一条记录。
     */
    int insertSelective(Employee record);

    /**
     * 根据员工ID查询员工详细信息。
     *
     * @param empId 员工ID，作为查询条件。
     * @return 员工对象，包含查询到的员工信息。
     */
    Employee selectByPrimaryKey(Integer empId);

    /**
     * 根据员工ID选择性更新员工信息。
     * 仅更新传入对象中非空的字段。
     *
     * @param record 员工对象，包含待更新的字段信息。
     * @return 影响的行数，表示更新的记录数。
     */
    int updateByPrimaryKeySelective(Employee record);

    /**
     * 根据员工ID更新员工信息。
     * 更新所有传入对象中的字段。
     *
     * @param record 员工对象，包含待更新的字段信息。
     * @return 影响的行数，表示更新的记录数。
     */
    int updateByPrimaryKey(Employee record);

    /**
     * 查询所有员工记录。
     *
     * @return 员工列表，包含所有员工信息。
     */
    List<Employee> selectAll();
}