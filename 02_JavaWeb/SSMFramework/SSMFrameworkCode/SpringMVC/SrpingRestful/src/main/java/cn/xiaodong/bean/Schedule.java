package cn.xiaodong.bean;

import lombok.*;
import org.springframework.stereotype.Component;

/**
 * 学习计划
 */
@Component
//@Data
//@NoArgsConstructor // 无参构造
@RequiredArgsConstructor // 有参构造
@Getter // getter方法
@Setter // setter方法
@ToString // toString方法
public class Schedule {
    /**
     * 学习计划编号
     */
    private int id;
    /**
     * 学习计划内容
     */
    private String title;
    /**
     * 学习计划是否完成
     */
    private boolean isCompleted ;
}
