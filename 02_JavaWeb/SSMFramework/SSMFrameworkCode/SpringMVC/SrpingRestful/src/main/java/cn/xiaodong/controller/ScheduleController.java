package cn.xiaodong.controller;

import cn.xiaodong.bean.Result;
import cn.xiaodong.bean.Schedule;
import cn.xiaodong.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 学习计划
 */
@Controller
@RequestMapping(value ="/schedule")
public class ScheduleController {

    @Autowired
    public ScheduleService scheduleService;

    /**
     * 获取全部学习计划
     * @return
     */
    @GetMapping
    @ResponseBody
    public Result getAll(){
        List<Schedule> allSchedule = scheduleService.getAllSchedule();

        Result result = new Result();
        result.setData(allSchedule);
        result.setFlag(true);
        result.setCode(200);

        return result;
    }

    /**
     * 根据id删除学习计划
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @ResponseBody
    public Result delete(@PathVariable int id){

        scheduleService.deleteById(id);

        Result result = new Result();
        result.setData(null);
        result.setFlag(true);
        result.setCode(200);

        return result;
    }

    /**
     * 添加学习计划
     * @return
     */
    @PostMapping
    @ResponseBody
    public Result add(@RequestBody Schedule schedule){

        scheduleService.add(schedule.getTitle(),schedule.isCompleted());

        Result result = new Result();
        result.setData(null);
        result.setFlag(true);
        result.setCode(200);

        return result;
    }

    /**
     * 添加学习计划
     * @return
     */
    @PutMapping
    @ResponseBody
    public Result put(@RequestBody Schedule schedule){

        scheduleService.put(schedule);

        Result result = new Result();
        result.setData(null);
        result.setFlag(true);
        result.setCode(200);

        return result;
    }
}
