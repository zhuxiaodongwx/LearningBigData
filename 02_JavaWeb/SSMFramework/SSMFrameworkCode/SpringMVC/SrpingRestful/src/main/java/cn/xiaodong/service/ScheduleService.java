package cn.xiaodong.service;

import cn.xiaodong.bean.Schedule;

import java.util.List;

public interface ScheduleService {
    /**
     * 获取所有日程
     */
    public List<Schedule> getAllSchedule();


    /**
     * 根据id删除日程
     * @param id
     */
    public void deleteById(int id);

    public void add(String title, boolean completed);

    public void put(Schedule schedule);
}
