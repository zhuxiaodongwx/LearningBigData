package cn.xiaodong.dao.impl;

import cn.xiaodong.bean.Schedule;
import cn.xiaodong.dao.ScheduleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ScheduleDaoImpl implements ScheduleDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<Schedule> getAllSchedule() {
        String sql = "select * from schedule";

        List<Schedule> query = jdbcTemplate.query(sql, new RowMapper<Schedule>() {
            @Override
            public Schedule mapRow(ResultSet rs, int rowNum) throws SQLException {
                Schedule schedule = new Schedule();
                schedule.setId(rs.getInt("id"));
                schedule.setTitle(rs.getString("title"));
                schedule.setCompleted(rs.getInt("completed")==1?true:false);
                return schedule;
            }
        });
        return query;
    }

    @Override
    public void deleteById(int id) {
        String sql = "delete from schedule where id = ?";
        jdbcTemplate.update(sql,id);
    }

    @Override
    public void add(String title, boolean completed) {
        String sql = "insert into schedule(title,completed) values(?,?)";
        jdbcTemplate.update(sql,title,completed?1:0);
    }

    @Override
    public void put(Schedule schedule) {
        String sql = "update schedule set title = ?,completed = ? where id = ?";
        jdbcTemplate.update(sql,schedule.getTitle(),schedule.isCompleted()?1:0,schedule.getId());
    }

}
