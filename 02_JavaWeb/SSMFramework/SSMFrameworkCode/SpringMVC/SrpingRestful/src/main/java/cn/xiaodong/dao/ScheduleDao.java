package cn.xiaodong.dao;

import cn.xiaodong.bean.Schedule;

import java.util.List;

public interface ScheduleDao {

    public List<Schedule> getAllSchedule();

    public void deleteById(int id);

    public void add(String title, boolean completed);

    public void put(Schedule schedule);
}
