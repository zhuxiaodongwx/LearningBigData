package cn.xiaodong.service.impl;

import cn.xiaodong.bean.Schedule;
import cn.xiaodong.dao.ScheduleDao;
import cn.xiaodong.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    ScheduleDao scheduleDao;

    @Override
    public List<Schedule> getAllSchedule() {
        List<Schedule> allSchedule = scheduleDao.getAllSchedule();

        return allSchedule;
    }

    @Override
    public void deleteById(int id) {
        scheduleDao.deleteById(id);
    }

    @Override
    public void add(String title, boolean completed) {
        scheduleDao.add(title,completed);
    }

    @Override
    public void put(Schedule schedule) {
        scheduleDao.put(schedule);
    }
}
