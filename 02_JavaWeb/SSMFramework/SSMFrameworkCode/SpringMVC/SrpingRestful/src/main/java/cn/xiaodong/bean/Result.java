package cn.xiaodong.bean;

import lombok.*;
import org.springframework.stereotype.Component;

/**
 * 接口相应结果
 */
@Component
@Getter
@Setter
@ToString
@RequiredArgsConstructor
//@NoArgsConstructor
public class Result {

    /**
     * 响应编码，用于标识接口响应的状态。
     */
    private int code;

    /**
     * 响应标识，true表示接口调用成功，false表示失败。
     */
    private boolean flag;

    /**
     * 响应数据，承载接口返回的具体数据。
     */
    private Object data;
}
