package cn.xiaodong.controller;

import cn.xiaodong.bean.Student;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * SpringMVC相应数据
 */
@Controller
@RequestMapping(value = "/response")
public class ResponseController {


    @RequestMapping("/toJSP")
    public String toJSP(){
        System.out.println("toJSP");
        return "success";
    }

    @RequestMapping("/getStudent")
    @ResponseBody
    public Student getStudent(){
        Student student = new Student("小明", 18, "男", "北京", "123456");
        return student;
    }
}
