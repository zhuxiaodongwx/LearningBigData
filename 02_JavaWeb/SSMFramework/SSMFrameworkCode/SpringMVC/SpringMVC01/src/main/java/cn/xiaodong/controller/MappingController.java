package cn.xiaodong.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller的请求地址映射
 */
@Controller
// 指定该类的所有请求地址都以/mapping开头
@RequestMapping("/mapping")
public class MappingController {

    // 指定该方法的页面请求地址
    // 总请求地址=类的请求地址+方法的请求地址
    // 每个方法的请求地址应该是为一个，不能重复
    // method限制了请求方式，对于之外的方式，拒绝访问

    @RequestMapping(value = "/hello" ,method= {RequestMethod.GET})
    // 返回页面元素而不是请求路径
    @ResponseBody
    public String Hello(){
        System.out.println("Hello from SpringMVC");
        return "Hello from SpringMVC";
    }

    // 请求地址/hello1，是一个精确匹配的地址
    @GetMapping(value = "/hello1" )
    @ResponseBody
    public String Hello1(){
        System.out.println("Hello from Hello1");
        return "Hello from Hello1";
    }

    // 匹配/hello/后面一级地址为任意字符的地址
    @RequestMapping(value = "/hello/*" ,method= {RequestMethod.GET})
    @ResponseBody
    public String Hello2(){
        System.out.println("Hello from Hello2");
        return "Hello from Hello2";
    }

    // 匹配/hello/后面多级地址为任意字符的地址
    @RequestMapping(value = "/hello/**" ,method= {RequestMethod.GET})
    @ResponseBody
    public String Hello3(){
        System.out.println("Hello from Hello3");
        return "Hello from Hello3";
    }
}
