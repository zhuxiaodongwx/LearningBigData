package cn.xiaodong.controller2;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 拦截器
 */
//@Controller
//@ResponseBody
@RestController // 等价于 @Controller + @ResponseBody
public class InterceptionController {

    @RequestMapping("/interception")
    public String interceptionController() {
        System.out.println("interceptionController");
        return "interceptionController";
    }

    @RequestMapping("/interception/a")
    public String interceptionController2() {
        System.out.println("interceptionController2");
        return "interceptionController2";
    }

    @RequestMapping("/interception/a/b")
    public String interceptionController3() {
        System.out.println("interceptionController3");
        return "interceptionController3";
    }
}
