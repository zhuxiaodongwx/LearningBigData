package cn.xiaodong.controller2;

import jakarta.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 私有资源控制器
 */
//@Controller
//@ResponseBody
@RestController
@RequestMapping("private")// 等价于 @Controller + @ResponseBody
public class PrivateController {

    @RequestMapping("one")
    public String one() {
        System.out.println("【PrivateController】one()");
        return "【PrivateController】one()";
    }

    @RequestMapping("/two")
    public String two() {
        System.out.println("【PrivateController】two()");
        return "【PrivateController】two()";
    }
}
