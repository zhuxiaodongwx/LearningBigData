package cn.xiaodong.controller3;

import cn.xiaodong.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 参数校验
 */
@Controller
@RequestMapping("/paramCheck")
@ResponseBody
public class ParamCheckController {

    /**
     * produces = "application/json;charset=UTF-8"  解决乱码问题
     * @Validated    User对象，进行参数校验
     * BindingResult  参数校验结果信息
     */
    @RequestMapping(produces = "application/json;charset=UTF-8")
    public String checkParam(@Validated @RequestBody User user, BindingResult result) {

        // 参数校验，将校验的错误信息输出
        if (result.hasErrors()){
            String defaultMessage = result.getFieldError().getDefaultMessage();
            System.out.println("defaultMessage="+defaultMessage);
            return defaultMessage;
        }

        System.out.printf("user=", user.toString());
        return user.toString();
    }
}
