package cn.xiaodong.controller;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * 共享域对象操作
 */
@Controller
@RequestMapping(value = "/share")
public class ShareController {

    /**
     * Request域对象
     */
    @RequestMapping(value ="/getRequest")
    public String getRequest(Model model, ModelMap modelMap, Map<String,String> map, HttpServletRequest request){
        model.addAttribute("name1","model");
        modelMap.addAttribute("name2","modelMap");
        map.put("name3","map");
        request.setAttribute("name4","request");

        return "share";
    }

    /**
     * Request域对象
     */
    @RequestMapping(value ="/getRequest2")
    public ModelAndView getRequest2(){
        ModelAndView moduleAndView = new ModelAndView();
        moduleAndView.addObject("name1","moduleAndView");
        moduleAndView.setViewName("share");

        return moduleAndView;
    }


    /**
     * session域对象
     */
    @RequestMapping(value ="/getSession")
    public String getSession(HttpSession session){
        session.setAttribute("name","session");
        return "share";
    }

    /**
     * Request域对象
     */
    @Autowired
    ServletContext application ;
    @RequestMapping(value ="/getApplication")
    public String getApplication( ){
        application.setAttribute("name", "application");
        return "share";
    }
}
