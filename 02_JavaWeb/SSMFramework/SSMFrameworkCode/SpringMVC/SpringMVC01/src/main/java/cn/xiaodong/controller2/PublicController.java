package cn.xiaodong.controller2;

import jakarta.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 公共资源控制器
 */
//@Controller
//@ResponseBody
@RestController
@RequestMapping("public")// 等价于 @Controller + @ResponseBody
public class PublicController {

    @RequestMapping("/login")
    public String login(HttpSession session) {
        session.setAttribute("user", "xiaodong");
        System.out.println("【PublicController】login()");
        return "【PublicController】login() Sucess!";
    }

    @RequestMapping("one")
    public String one() {
        System.out.println("【PublicController】one()");
        return "【PublicController】one()";
    }

    @RequestMapping("/two")
    public String two() {
        System.out.println("【PublicController】two()");
        return "【PublicController】two()";
    }
}
