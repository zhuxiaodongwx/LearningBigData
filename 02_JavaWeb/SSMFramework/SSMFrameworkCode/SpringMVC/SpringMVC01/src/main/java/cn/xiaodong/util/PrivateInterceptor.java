package cn.xiaodong.util;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 拦截器（私有资源）
 */
public class PrivateInterceptor implements HandlerInterceptor {
    /**
     * 在请求处理之前进行调用（Controller方法调用之前）
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("【PrivateInterceptor】" + "preHandle");

        String user = (String) request.getSession().getAttribute("user");
        if (user == null) {
            response.setContentType("text/html;charset=utf-8");
            //没有登录
            response.getWriter().print("请先登录,再访问!");

            return false;
        } else {
            return true;
        }
    }

    /**
     * 请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("【PrivateInterceptor】" + "postHandle");
    }

    /**
     * 在整个请求结束之后被调用，即使请求发生异常也会被调用
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("【PrivateInterceptor】" + "afterCompletion");
    }
}
