package cn.xiaodong.util;

import org.springframework.web.bind.annotation.*;

/**
 * 异常拦截器
 */
@RestControllerAdvice // @RestControllerAdvice=@ControllerAdvice+@ResponseBody
//@ControllerAdvice // 声明当前类是一个异常拦截器
//@ResponseBody
public class MyExceptionHandler {


    // 声明处理的异常类型
    @ExceptionHandler(ArithmeticException.class)
    public String exceptionHandler2(Exception e) {
        return "ArithmeticException"+"异常处理完毕";
    }

    // 异常类型匹配顺序：子类异常优先于父类异常
    // 先匹配ArithmeticException.class，在匹配Exception.class
    @ExceptionHandler(Exception.class)
    public String exceptionHandler(Exception e) {
        return "Exception"+"异常处理完毕";
    }

}
