package cn.xiaodong.controller2;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 基于注解的异常处理
 */
//@Controller
//@ResponseBody
@RestController // 等价于 @Controller + @ResponseBody
public class ExceptionController {

    @RequestMapping("/exception")
    public String exceptionController() {
        System.out.println("exceptionController");
        int i = 1 / 0;
        return "exceptionController";
    }
}
