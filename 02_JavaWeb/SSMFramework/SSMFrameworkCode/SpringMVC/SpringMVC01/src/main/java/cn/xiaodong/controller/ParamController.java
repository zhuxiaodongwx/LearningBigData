package cn.xiaodong.controller;

import cn.xiaodong.bean.Student;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * 接收参数
 */
@Controller
@RequestMapping("/param")
public class ParamController {

    /**
     * 访问路径中的参数名与方法参数名一致，即可自动获取
     */
    // http://localhost:8080/SpringMVC01/param/getParam2?name=jason&age=18
    @RequestMapping(value = "/getParam")
    @ResponseBody
    public String getParam(String name, int age) {
        String result = "name:" + name + " age:" + age;
        System.out.println(result);
        return result;
    }

    /**
     * @RequestParam对参数进行更进一步的说明 required：参数是否为必须的，如果是必须的但是没有传，或报错
     * value：访问路径中参数的名称，可以与方法参数名不一致，此处进行映射
     * defaultValue：如果前端没有传参数，则使用默认值
     */
    // http://localhost:8080/SpringMVC01/param/getParam2?username=jason&age=18
    @RequestMapping(value = "/getParam2")
    @ResponseBody
    public String getParam2(
            @RequestParam(required = true, value = "username", defaultValue = "xiaodong") String name,
            int age) {
        String result = "name:" + name + " age:" + age;
        System.out.println(result);
        return result;
    }

    /**
     * 访问路径中，一个参数传输多个值
     */
    // http://localhost:8080/SpringMVC01/param/getParam3?like=chifan&like=shuijiao&like=dadoudou
    @RequestMapping(value = "/getParam3")
    @ResponseBody
    public String getParam3(@RequestParam String[] like) {
        List<String> list = new ArrayList<>();
        for (String s : like) {
            list.add(s);
        }

        String result = "like:" + list.toString();
        System.out.println(result);
        return result;
    }

    /**
     * 访问路径中，参数封装到实体类中
     */
    // http://localhost:8080/SpringMVC01/param/getParam4?name=xiaodong&age=15&sex=1&address=shanxi xinhua&phone=13288888888
    @RequestMapping(value = "/getParam4")
    @ResponseBody
    public String getParam4(Student student) {
        String result = student.toString();
        System.out.println(result);
        return result;
    }


    /**
     * 获取请求头的信息
     */
    // http://localhost:8080/SpringMVC01/param/getParam5
    @RequestMapping("/getParam5")
    @ResponseBody
    public String getParam5(
            @RequestHeader("Accept-Encoding") String acceptEncoding,
            @RequestHeader("User-Agent") String userAgent) {

        String result = "acceptEncoding:" + acceptEncoding + " userAgent:" + userAgent;
        System.out.println(result);
        return result;
    }


    /**
     * 添加cookie
     */
    //http://localhost:8080/SpringMVC01/param/addCookie
    @RequestMapping("/addCookie")
    @ResponseBody
    public String addCookie(HttpServletResponse response) {
        Cookie cookie = new Cookie("JSESSIONID", "JSESSIONID8888888");
        cookie.setMaxAge(60 * 60 * 24 * 7);
        response.addCookie(cookie);

        return "success";
    }

    /**
     * 获取Cookie的值
     */
    //http://localhost:8080/SpringMVC01/param/getParam6
    @RequestMapping("/getParam6")
    @ResponseBody
    public String getParam6(@CookieValue String JSESSIONID) {
        String result = "JSESSIONID:" + JSESSIONID;
        System.out.println(result);
        return result;
    }

    /**
     * 接收Json参数
     */
    // http://localhost:8080/SpringMVC01/param/getParam8
    @RequestMapping("/getParam8")
    @ResponseBody
    public String getParam8(@RequestBody Student student) {
        String result = student.toString();
        System.out.println(result);
        return result;
    }

    /**
     * 接收Json参数
     */
    // http://localhost:8080/SpringMVC01/param/getParam9/S00254/xiaodong
    @RequestMapping("/getParam9/{id}/{name}")
    @ResponseBody
    public String getParam9(@PathVariable("id") String id, @PathVariable("name") String name) {
        String result = "id=" + id + " name=" + name;
        System.out.println(result);
        return result;
    }

    /**
     * 原生API操作
     * 该方法演示了如何在Spring MVC中访问不同的请求和响应对象。
     * <p>
     * http://localhost:8080/SpringMVC01/param/getParam10
     *
     * <p>
     * ServletRequest: 代表HTTP请求的内容，可以从中获取请求参数和头信息等。
     * ServletResponse: 用于向客户端发送响应，这里没有直接使用，但可以用于写入响应体。
     * HttpSession: 代表客户端的会话，可以用于存储和检索会话级数据。
     * InputStream: 请求的输入流，可以用于读取客户端发送的数据，如文件上传。
     * OutputStream: 响应的输出流，可以用于直接向客户端写入数据，如下载文件。
     * Reader: 请求的字符输入流，与InputStream类似，但用于读取字符数据。
     * Writer: 响应的字符输出流，与OutputStream类似，但用于写入字符数据。
     * <p>
     * 注意：在这个示例中，并没有实际使用这些参数进行操作，只是为了展示如何访问这些对象。
     *
     * @param servletRequest  代表HTTP请求的对象。
     * @param servletResponse 代表HTTP响应的对象。
     * @param httpSession     客户端的会话对象。
     * @param inputStream     请求的输入流。
     * @param outputStream    响应的输出流。
     * @param reader          请求的字符输入流。
     * @param writer          响应的字符输出流。
     * @return 一个空字符串作为响应。
     */
    @RequestMapping("/getParam10")
    @ResponseBody
    public String getParam10(ServletRequest servletRequest, ServletResponse servletResponse, HttpSession httpSession,
                             InputStream inputStream, OutputStream outputStream, Reader reader, Writer writer) {
        String result = "getParam10";
        System.out.println(result);
        return result;
    }

}
