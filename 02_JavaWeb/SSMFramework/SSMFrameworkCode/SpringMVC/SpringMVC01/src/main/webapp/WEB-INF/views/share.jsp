<%--
  Created by IntelliJ IDEA.
  User: XiaoDong
  Date: 2024/6/15
  Time: 上午10:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>响应成功</title>
</head>
<body>
<h1>share.jsp响应成功</h1>

<table border="1">
    <tr>
        <td colspan="2">request中的对象</td>
    </tr>
    <tr>
        <td>name1</td> <td><%=request.getAttribute("name1")%></td>
    </tr>
    <tr>
        <td>name2</td> <td><%=request.getAttribute("name2")%></td>
    </tr>
    <tr>
        <td>name3</td> <td><%=request.getAttribute("name3")%></td>
    </tr>
    <tr>
        <td>name4</td> <td><%=request.getAttribute("name4")%></td>
    </tr>
</table>

<h2>session中的对象 name=<%=session.getAttribute("name")%></h2>
<h2>application中的对象 name=<%=application.getAttribute("name")%></h2>
</body>
</html>
