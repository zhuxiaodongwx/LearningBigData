package cn.xiaodong.Test;

import cn.xiaodong.bean.Employee;
import cn.xiaodong.dao.EmployeeMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 数据输入测试（SQL条件）
 */
public class MyBatisTest {

    /**
     * MyBatis会话
     */
    SqlSession session;

    @BeforeEach
    public void init() throws IOException {
        // 1.创建SqlSessionFactory对象
        // ①声明Mybatis全局配置文件的路径
        String mybatisConfigFilePath = "mybatis-config.xml";
        // ②以输入流的形式加载Mybatis配置文件
        InputStream inputStream = Resources.getResourceAsStream(mybatisConfigFilePath);


        // ③基于读取Mybatis配置文件的输入流创建SqlSessionFactory对象
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 2.使用SqlSessionFactory对象开启一个会话
        session = sessionFactory.openSession();
    }

    @AfterEach
    public void closeSession() {
        // 4.关闭SqlSession
        session.commit(); //提交事务 [DQL不需要,其他需要]
        session.close(); //关闭会话
    }


    /**
     * 获取全部的员工数量
     *
     * @throws IOException
     */
    @Test
    public void selectCountAll() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);


        // 4. 调用代理类方法既可以触发对应的SQL语句
        int selectCountAll = employeeMapper.selectCountAll();
        System.out.println("员工数量一共有 = " + selectCountAll);

    }

    /**
     * 根据员工id获取员工信息
     *
     * @throws IOException
     */
    @Test
    public void selectEmployeeByEmpId() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);


        // 4. 调用代理类方法既可以触发对应的SQL语句
        Employee employee = employeeMapper.selectEmployeeByEmpId(1);
        System.out.println("employee = " + employee);

    }


    /**
     * 根据员工id和姓名获取员工信息
     *
     * @throws IOException
     */
    @Test
    public void selectEmployeeByIdAndName() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);


        // 4. 调用代理类方法既可以触发对应的SQL语句
        Employee employee = employeeMapper.selectEmployeeByIdAndName(1, "tom");
        System.out.println("employee = " + employee);

    }

    /**
     * 根据员工id和姓名获取员工信息
     *
     * @throws IOException
     */
    @Test
    public void selectEmployeeByMap() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);

        Map<String, Object> map = new HashMap<>();
        map.put("empId", 1);
        map.put("empName", "tom");

        // 4. 调用代理类方法既可以触发对应的SQL语句
        Employee employee = employeeMapper.selectEmployeeByMap(map);
        System.out.println("employee = " + employee);

    }

    @Test
    public void insertEmployee() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);

        Employee xiaodong = new Employee(null, "xiaodong", 1000.0);

        // 4. 调用代理类方法既可以触发对应的SQL语句
        int i = employeeMapper.insertEmployee(xiaodong);
        System.out.println("插入结果 = " + (i > 0 ? "成功" : "失败"));

    }
}
