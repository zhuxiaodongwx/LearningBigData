package cn.xiaodong.Test;

import cn.xiaodong.bean.Employee;
import cn.xiaodong.dao.EmployeeDynamicMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 动态SQL测试
 */
public class DynamicSQLTest {

    /**
     * MyBatis会话
     */
    SqlSession session;

    @BeforeEach
    public void init() throws IOException {
        // 1.创建SqlSessionFactory对象
        // ①声明Mybatis全局配置文件的路径
        String mybatisConfigFilePath = "mybatis-config.xml";
        // ②以输入流的形式加载Mybatis配置文件
        InputStream inputStream = Resources.getResourceAsStream(mybatisConfigFilePath);


        // ③基于读取Mybatis配置文件的输入流创建SqlSessionFactory对象
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 2.使用SqlSessionFactory对象开启一个会话
        session = sessionFactory.openSession();
    }

    @AfterEach
    public void closeSession() {
        // 4.关闭SqlSession
        session.commit(); //提交事务 [DQL不需要,其他需要]
        session.close(); //关闭会话
    }


    /**
     * 动态SQL，if和where标签
     *
     * @throws IOException
     */
    @Test
    public void selectEmployeeByEmpIdAndEmpName() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeDynamicMapper mapper = session.getMapper(EmployeeDynamicMapper.class);

        Employee employee = mapper.selectEmployeeByEmpIdAndEmpName(1, "tom");
        System.out.println(employee);
    }

    /**
     * 动态SQL，set标签
     *
     * @throws IOException
     */
    @Test
    public void setEmployeeByEmpId() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeDynamicMapper mapper = session.getMapper(EmployeeDynamicMapper.class);

        Employee employee = new Employee(1, "xiaodong", 1000.0);
        int i = mapper.updateEmployeeByEmpId(1, employee);
        System.out.println("修改成功的行数="+i);
    }

    /**
     * 动态SQL，foreach循环
     *
     * @throws IOException
     */
    @Test
    public void insertEmployeeList() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeDynamicMapper mapper = session.getMapper(EmployeeDynamicMapper.class);

        Employee employee1 = new Employee(null, "xiaodong1", 1000.0);
        Employee employee2 = new Employee(null, "xiaodong2", 2000.0);
        Employee employee3 = new Employee(null, "xiaodong3", 3000.0);
        List<Employee> employeeList = new ArrayList<Employee>();
        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);

        int i = mapper.insertEmployeeList(employeeList);
        System.out.println("修改成功的行数="+i);
    }

    /**
     * 动态SQL，trim标签
     *
     * @throws IOException
     */
    @Test
    public void selectEmployeeByConditionByTrim() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeDynamicMapper mapper = session.getMapper(EmployeeDynamicMapper.class);

        List<Employee> employees = mapper.selectEmployeeByConditionByTrim("xiaodong", 500.00);
        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }


}
