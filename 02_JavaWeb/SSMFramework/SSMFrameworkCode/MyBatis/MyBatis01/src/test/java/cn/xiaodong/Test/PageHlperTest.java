package cn.xiaodong.Test;

import cn.xiaodong.dao.UnionMapper;
import cn.xiaodong.dao.UnionMapper2;
import cn.xiaodong.view.Orders;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * MyBatis 分页插件
 */
public class PageHlperTest {

    /**
     * MyBatis会话
     */
    SqlSession session;

    @BeforeEach
    public void init() throws IOException {
        // 1.创建SqlSessionFactory对象
        // ①声明Mybatis全局配置文件的路径
        String mybatisConfigFilePath = "mybatis-config.xml";
        // ②以输入流的形式加载Mybatis配置文件
        InputStream inputStream = Resources.getResourceAsStream(mybatisConfigFilePath);


        // ③基于读取Mybatis配置文件的输入流创建SqlSessionFactory对象
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 2.使用SqlSessionFactory对象开启一个会话
        session = sessionFactory.openSession();
    }

    @AfterEach
    public void closeSession() {
        // 4.关闭SqlSession
        session.commit(); //提交事务 [DQL不需要,其他需要]
        session.close(); //关闭会话
    }


    /**
     * MyBatis 分页插件
     *
     * @throws IOException
     */
    @Test
    public void selectCustomerWithOrders() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        UnionMapper mapper = session.getMapper(UnionMapper.class);

        // 设置分页，起始页，每页显示的条数
        PageHelper.startPage(2, 10);
        // 执行查询
        List<Orders> orders = mapper.selectOrders();
        orders.forEach(System.out::println);

        // 根据查询结果，获取分页信息
        PageInfo<Orders> ordersPageInfo = new PageInfo<>(orders);
        // 展示分页相关信息
        System.out.println("总记录数：" + ordersPageInfo.getTotal());
        System.out.println("总页数：" + ordersPageInfo.getPages());
        System.out.println("当前页：" + ordersPageInfo.getPageNum());
        System.out.println("当前页的记录数：" + ordersPageInfo.getSize());
        System.out.println("每页记录数：" + ordersPageInfo.getPageSize());
        System.out.println("是否有上一页：" + ordersPageInfo.isHasPreviousPage());
        System.out.println("是否有下一页：" + ordersPageInfo.isHasNextPage());
    }

}
