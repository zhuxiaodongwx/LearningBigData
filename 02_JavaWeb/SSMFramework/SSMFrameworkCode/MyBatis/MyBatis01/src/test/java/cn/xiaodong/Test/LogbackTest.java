package cn.xiaodong.Test;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * LogBack测试
 */
@Slf4j // 生成日志对象
public class LogbackTest {

//    private Logger logger = (Logger) LoggerFactory.getLogger(LogbackTest.class);

    /**
     * 测试不同的日志等级
     */
    @Test
    public void testLogback() {
//        logger.trace("hello xiaodong from trace");
//        logger.debug("hello xiaodong from debug");
//        logger.info("hello xiaodong from info");
//        logger.warn("hello xiaodong from warn");
//        logger.error("hello xiaodong from error");
        log.trace("hello xiaodong from trace");
        log.debug("hello xiaodong from debug");
        log.info("hello xiaodong from info");
        log.warn("hello xiaodong from warn");
        log.error("hello xiaodong from error");
    }
}
