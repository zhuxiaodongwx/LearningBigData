package cn.xiaodong.Test;

import cn.xiaodong.bean.Employee;
import cn.xiaodong.bean.Employee2;
import cn.xiaodong.dao.EmployeeMapper2;
import cn.xiaodong.dao.UnionMapper;
import cn.xiaodong.view.Customer;
import cn.xiaodong.view.Orders;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * 订单与客户关联查询
 */
public class UnionTest {

    /**
     * MyBatis会话
     */
    SqlSession session;

    @BeforeEach
    public void init() throws IOException {
        // 1.创建SqlSessionFactory对象
        // ①声明Mybatis全局配置文件的路径
        String mybatisConfigFilePath = "mybatis-config.xml";
        // ②以输入流的形式加载Mybatis配置文件
        InputStream inputStream = Resources.getResourceAsStream(mybatisConfigFilePath);


        // ③基于读取Mybatis配置文件的输入流创建SqlSessionFactory对象
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 2.使用SqlSessionFactory对象开启一个会话
        session = sessionFactory.openSession();
    }

    @AfterEach
    public void closeSession() {
        // 4.关闭SqlSession
        session.commit(); //提交事务 [DQL不需要,其他需要]
        session.close(); //关闭会话
    }


    /**
     * 查询全部的顾客，并查询出顾客的订单
     *
     * @throws IOException
     */
    @Test
    public void selectCustomerWithOrders() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        UnionMapper mapper = session.getMapper(UnionMapper.class);

        List<Customer> customers = mapper.selectCustomerWithOrders();
        for (Customer customer : customers) {
            System.out.println("----------------------------------------");
            System.out.println(customer);

            List<Orders> orders = customer.getOrderList();
            for (Orders order : orders) {
                System.out.print("\t");
                System.out.println(order);
            }
        }

    }

    /**
     * 查询全部的订单，并查询出订单的顾客
     *
     * @throws IOException
     */
    @Test
    public void selectEmployeeByEmpId() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        UnionMapper mapper = session.getMapper(UnionMapper.class);

        List<Orders> orders = mapper.selectOrdersWithCustomer();
        for (Orders order : orders) {
            System.out.println("----------------------------------------");
            System.out.println(order);

            Customer customer = order.getCustomer();
            System.out.print("\t");
            System.out.println(customer);
        }

    }



}
