package cn.xiaodong.Test;

import cn.xiaodong.dao.UnionMapper;
import cn.xiaodong.dao.UnionMapper2;
import cn.xiaodong.view.Customer;
import cn.xiaodong.view.Orders;
import cn.xiaodong.view2.Score;
import cn.xiaodong.view2.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 订单与客户关联查询
 */
public class UnionTest2 {

    /**
     * MyBatis会话
     */
    SqlSession session;

    @BeforeEach
    public void init() throws IOException {
        // 1.创建SqlSessionFactory对象
        // ①声明Mybatis全局配置文件的路径
        String mybatisConfigFilePath = "mybatis-config.xml";
        // ②以输入流的形式加载Mybatis配置文件
        InputStream inputStream = Resources.getResourceAsStream(mybatisConfigFilePath);


        // ③基于读取Mybatis配置文件的输入流创建SqlSessionFactory对象
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 2.使用SqlSessionFactory对象开启一个会话
        session = sessionFactory.openSession();
    }

    @AfterEach
    public void closeSession() {
        // 4.关闭SqlSession
        session.commit(); //提交事务 [DQL不需要,其他需要]
        session.close(); //关闭会话
    }


    /**
     * 查询全部的学生信息，包含学生的年级、成绩信息、学科信息
     *
     * @throws IOException
     */
    @Test
    public void selectCustomerWithOrders() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        UnionMapper2 mapper = session.getMapper(UnionMapper2.class);

        List<Student> students = mapper.selectStudentWithGradeAndScoreList();
        for (Student student : students) {
            System.out.println("-----------------------------------------------------------------");
            System.out.println(student);
            System.out.println("年级信息：");
            System.out.println(student.getGrade());

            List<Score> scoreList = student.getScoreList();
            for (Score score : scoreList) {
                System.out.println("成绩信息：");
                System.out.println(score);
                System.out.println("科目信息：");
                System.out.println(score.getSubject());
            }

        }


    }




}
