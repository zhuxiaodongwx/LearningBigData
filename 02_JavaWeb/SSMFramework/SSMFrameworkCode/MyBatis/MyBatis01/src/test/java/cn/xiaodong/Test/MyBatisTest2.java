package cn.xiaodong.Test;

import cn.xiaodong.bean.Employee;
import cn.xiaodong.bean.Employee2;
import cn.xiaodong.dao.EmployeeMapper;
import cn.xiaodong.dao.EmployeeMapper2;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据输出测试（查询结果）
 */
public class MyBatisTest2 {

    /**
     * MyBatis会话
     */
    SqlSession session;

    @BeforeEach
    public void init() throws IOException {
        // 1.创建SqlSessionFactory对象
        // ①声明Mybatis全局配置文件的路径
        String mybatisConfigFilePath = "mybatis-config.xml";
        // ②以输入流的形式加载Mybatis配置文件
        InputStream inputStream = Resources.getResourceAsStream(mybatisConfigFilePath);


        // ③基于读取Mybatis配置文件的输入流创建SqlSessionFactory对象
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 2.使用SqlSessionFactory对象开启一个会话
        session = sessionFactory.openSession();
    }

    @AfterEach
    public void closeSession() {
        // 4.关闭SqlSession
        session.commit(); //提交事务 [DQL不需要,其他需要]
        session.close(); //关闭会话
    }


    /**
     * 根据员工id获取员工信息（Map接收）
     *
     * @throws IOException
     */
    @Test
    public void selectEmployeeByEmpId() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeMapper2 employeeMapper2 = session.getMapper(EmployeeMapper2.class);

        // 4. 调用代理类方法既可以触发对应的SQL语句
        Map<String, Object> map = employeeMapper2.selectEmployeeByEmpId(1);
        // 遍历map
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            System.out.println("Key: " + key + ", Value: " + value);
        }
    }

    /**
     * 查询所有员工信息（List接收）
     *
     * @throws IOException
     */
    @Test
    public void selectAllEmployees() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeMapper2 employeeMapper2 = session.getMapper(EmployeeMapper2.class);

        // 4. 调用代理类方法既可以触发对应的SQL语句
        List<Employee> employees = employeeMapper2.selectAllEmployees();
        employees.forEach(employee -> {
            System.out.println(employee);
        });
    }

    /**
     * 查询所有员工信息（List接收）
     *
     * @throws IOException
     */
    @Test
    public void selectAllEmployees2() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeMapper2 employeeMapper2 = session.getMapper(EmployeeMapper2.class);

        // 4. 调用代理类方法既可以触发对应的SQL语句
        List<Employee> employees = employeeMapper2.selectAllEmployees2();
        employees.forEach(employee -> {
            System.out.println(employee);
        });
    }

    /**
     * 自增主键
     *
     * @throws IOException
     */
    @Test
    public void insertEmployee() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeMapper2 employeeMapper2 = session.getMapper(EmployeeMapper2.class);

        Employee xiaodong = new Employee(null, "xiaodong2", 1000.0);
        // 4. 调用代理类方法既可以触发对应的SQL语句
        employeeMapper2.insertEmployee(xiaodong);
        System.out.println("MyBatis返回的主键是"+xiaodong.getEmpId());
    }

    /**
     * 自增主键
     *
     * @throws IOException
     */
    @Test
    public void insertEmployee2() throws IOException {
        // 3.根据EmployeeMapper接口的Class对象获取Mapper接口类型的对象(动态代理技术)
        EmployeeMapper2 employeeMapper2 = session.getMapper(EmployeeMapper2.class);

        Employee2 xiaodong = new Employee2(null, "xiaodong2", 1000.0);
        // 4. 调用代理类方法既可以触发对应的SQL语句
        employeeMapper2.insertEmployee2(xiaodong);
        System.out.println("MyBatis返回的主键是"+xiaodong.getEmpId());
    }


}
