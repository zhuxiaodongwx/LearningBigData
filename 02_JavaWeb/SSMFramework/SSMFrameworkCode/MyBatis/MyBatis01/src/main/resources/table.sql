# 数据库脚本
# 数据输入与数据输出部分，使用的相关表

CREATE TABLE `t_emp`(
                        emp_id INT AUTO_INCREMENT,
                        emp_name CHAR(100),
                        emp_salary DOUBLE(10,5),
                        PRIMARY KEY(emp_id)
);

CREATE TABLE `t_emp2`(
                         emp_id  VARCHAR(50),
                         emp_name CHAR(100),
                         emp_salary DOUBLE(10,5),
                         PRIMARY KEY(emp_id)
);

# ALTER TABLE t_emp2 MODIFY emp_id VARCHAR(50);

INSERT INTO `t_emp`(emp_name,emp_salary) VALUES("tom",200.33);
INSERT INTO `t_emp`(emp_name,emp_salary) VALUES("jerry",666.66);
INSERT INTO `t_emp`(emp_name,emp_salary) VALUES("andy",777.77);