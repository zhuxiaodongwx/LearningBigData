# 数据库脚本
# 多表映射，需要的表

#  ---------------------------------（一对一）----------------------------------------------
#  ---------------------------------（一对多/多对一）----------------------------------------------
CREATE TABLE orders
(
    `id`          int(11) AUTO_INCREMENT COMMENT '订单id',
    `name`        varchar(100) COLLATE utf8mb4_unicode_ci COMMENT '订单名称',
    `customer_id` int(11),
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE customer
(
    `id`   int(11) AUTO_INCREMENT COMMENT '顾客id',
    `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '顾客姓名',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

INSERT INTO ssmdb.customer (id, name)
VALUES (1, '小东');
INSERT INTO ssmdb.customer (id, name)
VALUES (2, '小明');

INSERT INTO ssmdb.orders (id, name, customer_id)
VALUES (1, '订单1', 1);
INSERT INTO ssmdb.orders (id, name, customer_id)
VALUES (2, '订单2', 1);
INSERT INTO ssmdb.orders (id, name, customer_id)
VALUES (3, '订单3', 1);
INSERT INTO ssmdb.orders (id, name, customer_id)
VALUES (4, '订单1', 2);
INSERT INTO ssmdb.orders (id, name, customer_id)
VALUES (5, '订单2', 2);
INSERT INTO ssmdb.orders (id, name, customer_id)
VALUES (6, '订单3', 2);
#  ---------------------------------（多对多）----------------------------------------------
#  ---------------------------------（四表关联）----------------------------------------------

CREATE TABLE `grade`
(
    `id`   int(11) NOT NULL AUTO_INCREMENT COMMENT '年级id',
    `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '年级名称',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='年级信息';

CREATE TABLE `score`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT COMMENT '考试成绩id',
    `student_id` int(11) DEFAULT NULL COMMENT '学生id',
    `subject_id` int(11) DEFAULT NULL COMMENT '考试科目id',
    `score`      int(11) DEFAULT NULL COMMENT '考试成绩',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='成绩表';

CREATE TABLE `student`
(
    `id`       int(11) NOT NULL AUTO_INCREMENT COMMENT '学生id',
    `name`     varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '学生姓名',
    `grade_id` int(11)                                DEFAULT NULL COMMENT '学生年级',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='学生表';

CREATE TABLE `subject`
(
    `id`   int(11) NOT NULL AUTO_INCREMENT COMMENT '科目id',
    `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '学习科目名称',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='学习科目';

INSERT INTO ssmdb.grade (id, name)
VALUES (1, '一年级');
INSERT INTO ssmdb.grade (id, name)
VALUES (2, '二年级');
INSERT INTO ssmdb.grade (id, name)
VALUES (3, '三年级');

INSERT INTO ssmdb.score (id, student_id, subject_id, score)
VALUES (1, 1, 1, 30);
INSERT INTO ssmdb.score (id, student_id, subject_id, score)
VALUES (2, 1, 1, 65);
INSERT INTO ssmdb.score (id, student_id, subject_id, score)
VALUES (3, 1, 2, 75);
INSERT INTO ssmdb.score (id, student_id, subject_id, score)
VALUES (4, 2, 1, 80);
INSERT INTO ssmdb.score (id, student_id, subject_id, score)
VALUES (5, 2, 2, 60);

INSERT INTO ssmdb.student (id, name, grade_id)
VALUES (1, '小东', 1);
INSERT INTO ssmdb.student (id, name, grade_id)
VALUES (2, '小明', 2);

INSERT INTO ssmdb.subject (id, name)
VALUES (1, '语文');
INSERT INTO ssmdb.subject (id, name)
VALUES (2, '历史');




