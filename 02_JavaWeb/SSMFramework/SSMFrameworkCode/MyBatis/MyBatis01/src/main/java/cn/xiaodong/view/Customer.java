package cn.xiaodong.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 顾客信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

    /**
     * 顾客id
     */
    private int id;

    /**
     * 顾客姓名
     */
    private String name;

    /**
     * 顾客的全部订单
     */
    private List<Orders> orderList;
}
