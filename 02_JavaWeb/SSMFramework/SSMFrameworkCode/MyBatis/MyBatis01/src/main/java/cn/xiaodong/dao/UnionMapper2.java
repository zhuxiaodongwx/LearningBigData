package cn.xiaodong.dao;

import cn.xiaodong.view.Customer;
import cn.xiaodong.view.Orders;
import cn.xiaodong.view2.Student;

import java.util.List;

/**
 * 订单和顾客的联合查询
 */
public interface UnionMapper2 {

    /**
     * 查询全部的学生信息，包含学生的年级、成绩信息、学科信息
     * @return
     */
    List<Student> selectStudentWithGradeAndScoreList();
}
