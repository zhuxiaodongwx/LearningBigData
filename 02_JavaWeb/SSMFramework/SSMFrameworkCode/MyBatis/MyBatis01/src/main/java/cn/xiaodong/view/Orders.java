package cn.xiaodong.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 订单信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Orders {

    /**
     * 订单id
     */
    private int id;

    /**
     * 订单名称
     */
    private String name;

    /**
     * 客户id
     */
    private int customerId;

    /**
     * 订单关联的客户
     */
    Customer customer;
}
