package cn.xiaodong.dao;

import cn.xiaodong.bean.Employee;
import cn.xiaodong.bean.Employee2;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 数据输出（查询结果）案例
 */
public interface EmployeeMapper2 {

    /**
     * 根据员工id查询员工信息,查询结果为Map类型
     * @param empId 员工id
     * @return
     */
    Map<String, Object>  selectEmployeeByEmpId(Integer empId);

    /**
     * 查询所有员工信息
     * @return
     */
    List<Employee> selectAllEmployees();


    /**
     * 查询所有员工信息（自定义映射关系）
     * @return
     */
    List<Employee> selectAllEmployees2();

    /**
     * 插入员工信息（主键自增）
     * @param employee
     * @return
     */
    int insertEmployee(Employee employee);

    /**
     * 插入员工信息（非主键自增）
     * @param employee
     * @return
     */
    int insertEmployee2(Employee2 employee);
}
