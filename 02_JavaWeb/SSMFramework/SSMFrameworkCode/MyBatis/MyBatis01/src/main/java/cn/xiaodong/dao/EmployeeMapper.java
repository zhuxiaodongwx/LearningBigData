package cn.xiaodong.dao;

import cn.xiaodong.bean.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 数据输入（SQL条件）案例
 */
public interface EmployeeMapper {

    /**
     * 查询全部的员工数量
     * @return
     */
    int selectCountAll();

    /**
     * 根据员工id查询员工
     * @param empId 员工id
     * @return
     */
    Employee selectEmployeeByEmpId(Integer empId);

    /**
     * 根据员工id和员工姓名查询员工
     * @param empId 员工id
     * @param empName 员工姓名
     * @return
     */
    Employee selectEmployeeByIdAndName(@Param("empId") Integer empId,   @Param("empName")String empName);

    /**
     * 根据员工id和员工姓名查询员工
     * @return
     */
    Employee selectEmployeeByMap(Map<String, Object> conditionMap);

    /**
     * 插入员工
     * @param employee
     * @return
     */
    int insertEmployee(Employee employee);
}
