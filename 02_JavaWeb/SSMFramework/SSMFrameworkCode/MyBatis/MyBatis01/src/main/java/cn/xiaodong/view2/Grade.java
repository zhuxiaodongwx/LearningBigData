package cn.xiaodong.view2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 年级信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Grade {
    /**
     * 年级id
     */
    private int id;

    /**
     * 年级名称
     */
    private String name;
}
