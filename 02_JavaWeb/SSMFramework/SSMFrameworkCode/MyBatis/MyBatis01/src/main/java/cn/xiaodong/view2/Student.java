package cn.xiaodong.view2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    /**
     * 学生id
     */
    private int id;

    /**
     * 学生姓名
     */
    private String name;

    /**
     * 学生年级
     */
    private int gradeId;

    /**
     * 学生年级
     */
    private Grade grade;

    /**
     * 学生成绩
     */
    List<Score> scoreList;
}
