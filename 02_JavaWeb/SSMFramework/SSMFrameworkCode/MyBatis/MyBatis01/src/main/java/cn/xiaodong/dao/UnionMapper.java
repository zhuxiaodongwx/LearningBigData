package cn.xiaodong.dao;

import cn.xiaodong.view.Customer;
import cn.xiaodong.view.Orders;

import java.util.List;

/**
 * 订单和顾客的联合查询
 */
public interface UnionMapper {

    /**
     * 查询全部的顾客，并查询出顾客的订单
     * @return
     */
    List<Customer> selectCustomerWithOrders();

    /**
     * 查询全部的订单，并查询出订单的顾客
     * @return
     */
    List<Orders> selectOrdersWithCustomer();

    /**
     * 查询全部的订单
     * @return
     */
    List<Orders> selectOrders();
}
