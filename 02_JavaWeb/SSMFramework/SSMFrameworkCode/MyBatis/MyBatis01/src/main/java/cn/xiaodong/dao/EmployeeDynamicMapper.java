package cn.xiaodong.dao;

import cn.xiaodong.bean.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 动态SQL案例
 */
public interface EmployeeDynamicMapper {

    /**
     * 根据员工姓名和员工编号查询员工信息
     * @param empId
     * @param empName
     * @return
     */
    Employee selectEmployeeByEmpIdAndEmpName(@Param("empId") Integer empId, @Param("empName") String empName);

    /**
     * 根据员工编号更新员工信息
     * @param empId
     * @param employee
     * @return
     */
    int updateEmployeeByEmpId(@Param("empId") Integer empId, @Param("employee") Employee employee);

    /**
     * 批量插入员工信息
     */
    int insertEmployeeList(@Param("employeeList") List<Employee> employeeList);


    /**
     * 根据条件查询员工信息 Trim标签的使用
     * @param empName
     * @param empSalary
     * @return
     */
   List<Employee> selectEmployeeByConditionByTrim(@Param("empName") String empName, @Param("empSalary") Double empSalary);
}
