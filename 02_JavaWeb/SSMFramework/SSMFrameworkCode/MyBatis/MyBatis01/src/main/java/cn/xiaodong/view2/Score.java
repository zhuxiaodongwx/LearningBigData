package cn.xiaodong.view2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 单次考试成绩
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Score {

    /**
     * 考试成绩id
     */
    private int id;

    /**
     * 学生id
     */
    private int studentId;

    /**
     * 考试科目id
     */
    private int subjectId;

    /**
     * 考试成绩
     */
    private int score;

    /**
     * 考试科目
     */
    private Subject subject;
}
