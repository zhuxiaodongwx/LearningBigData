package cn.xiaodong.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 员工类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee2 {

    /**
     * 员工编号
     */
    private String empId;

    /**
     * 员工姓名
     */
    private String empName;


    /**
     * 员工工资
     */
    private Double empSalary;

}
