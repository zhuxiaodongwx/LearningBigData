package cn.xiaodong.view2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 学习科目
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Subject {

    /**
     * 科目id
     */
    private int id;
    /**
     * 学习科目名称
     */
    private String name;
}
