/**
 * 学生数据操作接口。
 * 该接口定义了对学生表进行增、删、改、查等操作的方法。
 */
package cn.xiaodong.mapper;

import org.apache.ibatis.annotations.Param;

import cn.xiaodong.bean.Students;

import java.util.List;

/**
 * StudentsMapper接口定义了对学生数据的数据库操作方法。
 */
public interface StudentsMapper {
    /**
     * 根据学生ID删除学生记录。
     *
     * @param id 学生的唯一标识符。
     * @return 影响的行数，表示删除的记录数。
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入一个新的学生记录。
     *
     * @param record 学生对象，包含所有字段信息。
     * @return 影响的行数，表示插入的记录数。
     */
    int insert(Students record);

    /**
     * 选择性插入一个新的学生记录。
     * 只插入非空字段，以优化插入操作。
     *
     * @param record 学生对象，包含待插入的字段信息。
     * @return 影响的行数，表示插入的记录数。
     */
    int insertSelective(Students record);

    /**
     * 根据学生ID查询学生记录。
     *
     * @param id 学生的唯一标识符。
     * @return 学生对象，包含查询到的字段信息。
     */
    Students selectByPrimaryKey(Integer id);

    /**
     * 根据学生ID选择性更新学生记录。
     * 只更新非空字段，以优化更新操作。
     *
     * @param record 学生对象，包含待更新的字段信息。
     * @return 影响的行数，表示更新的记录数。
     */
    int updateByPrimaryKeySelective(Students record);

    /**
     * 根据学生ID更新学生记录。
     * 更新所有字段，即使某些字段值未改变。
     *
     * @param record 学生对象，包含所有待更新的字段信息。
     * @return 影响的行数，表示更新的记录数。
     */
    int updateByPrimaryKey(Students record);

    // ----------------------------------- 以下是根据方法名自动生成对应的SQL -------------------------------------------------

    /**
     * 根据年龄范围查询所有学生。
     *
     * @param minAge 最小年龄。
     * @param maxAge 最大年龄。
     * @return 符合条件的学生列表。
     */
    List<Students> selectAllByAgeBetween(
            @Param("minAge") Integer minAge, @Param("maxAge") Integer maxAge);

    /**
     * 批量插入学生记录。
     *
     * @param list 学生对象列表。
     * @return 影响的行数，表示插入的记录数。
     */
    int insertList(@Param("list") List<Students> list);

    /**
     * 查询所有学生的不同性别。
     *
     * @return 性别列表。
     */
    List<String> findDistinctGender();

    /**
     * 根据ID更新学生信息。
     *
     * @param updated 更新后的学生对象。
     * @param id      学生的唯一标识符。
     * @return 影响的行数。
     */
    int updateById(@Param("updated") Students updated, @Param("id") Integer id);


    List<Students> selectAllByAgeBetweenAndGenderAndName(
            @Param("minAge") Integer minAge, @Param("maxAge") Integer maxAge,
            @Param("gender") String gender, @Param("name") String name);

}
