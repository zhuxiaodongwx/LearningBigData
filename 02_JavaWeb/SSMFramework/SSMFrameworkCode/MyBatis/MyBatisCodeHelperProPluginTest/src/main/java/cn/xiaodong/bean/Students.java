package cn.xiaodong.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Students类代表学生的实体信息。
 * 该类包含了学生的基本属性，如姓名、性别、年龄和班级。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Students {
    /**
     * 学生的唯一标识符。
     */
    private Integer id;

    /**
     * 学生的姓名。
     */
    private String name;

    /**
     * 学生的性别。
     */
    private String gender;

    /**
     * 学生的年龄。
     */
    private Integer age;

    /**
     * 学生所在的班级。
     */
    private String classname;
}