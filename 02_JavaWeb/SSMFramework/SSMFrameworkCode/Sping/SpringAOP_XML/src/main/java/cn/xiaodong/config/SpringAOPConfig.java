package cn.xiaodong.config;


import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

//@Configuration// Spring配置类
//@EnableAspectJAutoProxy// Spring 开启AOP
//@ComponentScan(basePackages = "cn.xiaodong") // Spring扫描包

public class SpringAOPConfig {

    /**
     * 定义一个切入点
     * 切入点的名字就是方法名（全类名）
     */
    @Pointcut("execution(* cn.xiaodong.dao.impl.StudentDaoImpl.addStudent(..))")
    public void myPointCut1() {
    }
}
