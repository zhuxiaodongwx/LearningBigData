package cn.xiaodong.ioc;

import cn.xiaodong.bean.User;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Spring框架，获取管理的对象
 * 加载配置文件获取
 */
public class IOCDemo {

    @Test
    public void test1() {
        // 创建容器（方法一：获取接口ApplicationContext）
        ApplicationContext app = new ClassPathXmlApplicationContext("spring.xml");
        // 创建容器（方法二：获取ClassPathXmlApplicationContext类）
//        ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("spring.xml");

        // 获取类方式1
        User user = app.getBean("user", User.class);
        System.out.println(user);

        // 获取类方式2
        User user2 = (User) app.getBean("user");
        System.out.println(user2);

        // 获取类方式3（前提：容器中同一个类spring.xml只能注册一次）
        User user3 = app.getBean(User.class);
        System.out.println(user3);
    }


    /**
     * 创建容器时不指定配置文件
     * 后期加载配置文件
     */
    @Test
    public void test3() {
        // 创建容器
        ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext();
        // 加载配置文件
        app.setConfigLocation("spring.xml");
        // 强制刷新
        app.refresh();

        User user = app.getBean("user", User.class);
        System.out.println(user);
    }


}
