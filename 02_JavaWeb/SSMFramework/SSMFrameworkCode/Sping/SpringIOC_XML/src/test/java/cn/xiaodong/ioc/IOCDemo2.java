package cn.xiaodong.ioc;

import cn.xiaodong.bean.User;
import cn.xiaodong.bean.User2;
import cn.xiaodong.bean.User3;
import com.alibaba.druid.pool.DruidDataSource;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.SQLException;

/**
 * Spring框架，类的属性赋值
 */
public class IOCDemo2 {

    /**
     * xml外部注册类
     */
    @Test
    public void test1() {
        // 创建容器
        ApplicationContext app = new ClassPathXmlApplicationContext("spring.xml");

        // 获取类方式1
        User user = app.getBean("user", User.class);

        System.out.println(user);
        System.out.println(user.getGrade());

    }

    /**
     * xml内部注册类
     */
    @Test
    public void test2() {
        // 创建容器
        ApplicationContext app = new ClassPathXmlApplicationContext("spring.xml");

        // 获取类方式1
        User2 user = app.getBean("user2", User2.class);

        System.out.println(user);
        System.out.println(user.getGrade());
    }

    /**
     * xml调取有参构造赋值
     */
    @Test
    public void test3() {
        // 创建容器
        ApplicationContext app = new ClassPathXmlApplicationContext("spring.xml");

        // 获取类方式1
        User3 user = app.getBean("user3", User3.class);

        System.out.println(user);
        System.out.println(user.getGrade());
    }


    /**
     * Spring容器管理其他Jar包中的类
     *
     * 获取数据库连接池
     */
    @Test
    public void test4() throws SQLException {
        // 创建容器
        ApplicationContext app = new ClassPathXmlApplicationContext("spring.xml");

        // 获取类
        DruidDataSource db = app.getBean("db", DruidDataSource.class);

        System.out.println(db.getConnection());
    }

}
