package cn.xiaodong.ioc;

import cn.xiaodong.bean.User3;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IOCDemo5 {

    /**
     * JavaBean的生命周期
     */
    @Test
    public void test1() {
        ApplicationContext app = new ClassPathXmlApplicationContext("spring3.xml");

        // 多例模式
        User3 user = (User3) app.getBean("user");
        System.out.println(user);

        //关闭容器
        ConfigurableApplicationContext coc = (ConfigurableApplicationContext)app;
        coc.close();
    }


}
