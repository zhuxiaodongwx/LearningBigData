package cn.xiaodong.ioc;

import cn.xiaodong.bean.User;
import cn.xiaodong.bean.User2;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IOCDemo4 {

    /**
     * 单例模式与多例模式
     */
    @Test
    public void test1() {
        ApplicationContext app = new ClassPathXmlApplicationContext("spring2.xml");

        // 多例模式
        User user_1 = (User) app.getBean("user");
        User user_2 = (User) app.getBean("user");
        System.out.println(user_1);
        System.out.println(user_2);

        // 单例模式
        User2 user2_1 = (User2) app.getBean("user2");
        User2 user2_2 = (User2) app.getBean("user2");
        System.out.println(user2_1);
        System.out.println(user2_2);
    }


}
