package cn.xiaodong.ioc;

import cn.xiaodong.bean.User;
import cn.xiaodong.factorybean.FactoryBeanUser;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IOCDemo3 {

    /**
     * factoryBean获取管理的对象
     */
    @Test
    public void test1() {
        // factoryBean获取管理的对象
        ApplicationContext app = new ClassPathXmlApplicationContext("spring.xml");
        User user = (User) app.getBean("factoryBeanUser");
        System.out.println(user);

        // 获取factoryBean自己
        FactoryBeanUser factoryBeanUser = (FactoryBeanUser) app.getBean("&factoryBeanUser");
        System.out.println(factoryBeanUser);
    }


}
