package cn.xiaodong.bean;

/**
 * 代表年级信息的类。
 * 该类用于封装年级的相关属性和行为。
 */
public class Grade {

    // 年级的属性，用于存储年级的名称或标识。
    private String grade;

    /**
     * 获取年级的属性值。
     *
     * @return 当前年级的名称或标识。
     */
    public String getGrade() {
        return grade;
    }

    public Grade(String grade) {
        this.grade = grade;
    }

    public Grade() {
    }

    /**
     * 设置年级的属性值。
     *
     * @param grade 要设置的年级名称或标识。
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * 重写toString方法，以提供更直观的年级信息表示。
     *
     * @return 返回年级的字符串表示，格式为"Grade{grade='xxx'}"。
     */
    @Override
    public String toString() {
        return "Grade{" +
                "grade='" + grade + '\'' +
                '}';
    }
}
