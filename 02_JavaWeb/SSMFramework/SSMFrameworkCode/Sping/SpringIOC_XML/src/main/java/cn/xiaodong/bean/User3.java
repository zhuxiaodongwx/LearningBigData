package cn.xiaodong.bean;

/**
 * 用户类，代表系统中的一个用户。
 * 包含用户的基本信息，如身份证号、姓名、密码、电子邮件、性别和生日。
 */
public class User3 {

    private int age; // 用户年龄
    private String name; // 用户姓名
    private Grade grade;// 用户的年级

    /**
     * 初始化方法
     */
    public void Init() {
        System.out.println("User3初始化方法");
    }

    /**
     * 初始化方法
     */
    public void Destroy() {
        System.out.println("User3销毁方法");
    }


    /**
     * 获取用户的年级信息。
     *
     * @return 用户所在的年级对象。
     */
    public Grade getGrade() {
        return grade;
    }

    /**
     * 设置用户的年级信息。
     *
     * @param grade 用户所在的年级对象。
     */
    public void setGrade(Grade grade) {
        this.grade = grade;
    }


    /**
     * 获取用户姓名。
     *
     * @return 用户的姓名。
     */
    public String getName() {
        return name;
    }

    /**
     * 设置用户姓名。
     *
     * @param name 要设置的用户姓名。
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * 获取用户年龄。
     *
     * @return 用户的年龄。
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置用户年龄。
     *
     * @param age 要设置的用户年龄。
     */
    public void setAge(int age) {
        this.age = age;
    }


    public User3() {
        System.out.println("User3无参构造方法");
    }

    public User3(int age, String name, Grade grade) {
        System.out.println("User3有参构造方法");
        this.age = age;
        this.name = name;
        this.grade = grade;
    }

    /**
     * 返回用户信息的字符串表示。
     *
     * @return 用户信息的字符串，包括ID、年龄、姓名、密码、电子邮件、性别和生日。
     */
    @Override
    public String toString() {
        return "User3{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", grade=" + grade +
                '}';
    }
}
