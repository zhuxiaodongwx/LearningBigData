package cn.xiaodong.beanPostProcessor;

import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor {

    public Object postProcessBeforeInitialization(Object bean, String beanName)
    {
        System.out.println(beanName+"初始化之前执行");
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName)
    {
        System.out.println(beanName+"初始化之后执行");
        return bean;
    }
}
