package cn.xiaodong.bean;

/**
 * 用户类，代表系统中的一个用户。
 * 包含用户的基本信息，如身份证号、姓名、密码、电子邮件、性别和生日。
 */
public class User {

    private int id; // 用户唯一标识
    private int age; // 用户年龄
    private String name; // 用户姓名
    private String password; // 用户密码
    private String email; // 用户电子邮件地址
    private String sex; // 用户性别
    private String birthday; // 用户生日

    private Grade grade;// 用户的年级

    /**
     * 获取用户的年级信息。
     * @return 用户所在的年级对象。
     */
    public Grade getGrade() {
        return grade;
    }

    /**
     * 设置用户的年级信息。
     * @param grade 用户所在的年级对象。
     */
    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    /**
     * 获取用户ID。
     * @return 用户的唯一标识ID。
     */
    public int getId() {
        return id;
    }

    /**
     * 设置用户ID。
     * @param id 要设置的用户ID。
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 获取用户姓名。
     * @return 用户的姓名。
     */
    public String getName() {
        return name;
    }

    /**
     * 设置用户姓名。
     * @param name 要设置的用户姓名。
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取用户密码。
     * @return 用户的密码。
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置用户密码。
     * @param password 要设置的用户密码。
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取用户电子邮件地址。
     * @return 用户的电子邮件地址。
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置用户电子邮件地址。
     * @param email 要设置的用户电子邮件地址。
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取用户性别。
     * @return 用户的性别。
     */
    public String getSex() {
        return sex;
    }

    /**
     * 设置用户性别。
     * @param sex 要设置的用户性别。
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 获取用户生日。
     * @return 用户的生日。
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * 设置用户生日。
     * @param birthday 要设置的用户生日。
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * 获取用户年龄。
     * @return 用户的年龄。
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置用户年龄。
     * @param age 要设置的用户年龄。
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 返回用户信息的字符串表示。
     * @return 用户信息的字符串，包括ID、年龄、姓名、密码、电子邮件、性别和生日。
     */
//    @Override
//    public String toString() {
//        return "User{" +
//                "id=" + id +
//                ", age=" + age +
//                ", name='" + name + '\'' +
//                ", password='" + password + '\'' +
//                ", email='" + email + '\'' +
//                ", sex='" + sex + '\'' +
//                ", birthday='" + birthday + '\'' +
//                '}';
//    }
}
