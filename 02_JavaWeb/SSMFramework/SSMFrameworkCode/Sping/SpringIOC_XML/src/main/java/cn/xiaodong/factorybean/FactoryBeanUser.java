package cn.xiaodong.factorybean;

import cn.xiaodong.bean.User;
import cn.xiaodong.bean.Grade;
import org.springframework.beans.factory.FactoryBean;

public class FactoryBeanUser implements FactoryBean<User> {


    //获取工厂生成的对象,并对对象进行初始化或者其他操作
    @Override
    public User getObject() throws Exception {
        User user = new User();

        // 设置User的属性
        user.setAge(18);
        user.setName("小东");
        user.setGrade(new Grade("1"));
        user.setSex("男");

        return user;
    }

    //查看对象的类型
    @Override
    public Class<?> getObjectType() {
        return User.class;
    }


    //标记工厂生成出的对象是否是单例的,true表示单例
    // false 表示没调取一次getbean 就会创建一个新的对象
    @Override
    public boolean isSingleton() {
        return FactoryBean.super.isSingleton();
//        return false;
    }

}
