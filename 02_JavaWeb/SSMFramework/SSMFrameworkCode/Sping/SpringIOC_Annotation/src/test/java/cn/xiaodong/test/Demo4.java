package cn.xiaodong.test;

import cn.xiaodong.config.SpringConfig;
import cn.xiaodong.controller.StudentController;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 配置类,代替xml配置文件
 */
public class Demo4 {

    @Test
    public void test1() {
        // 创建容器
        ApplicationContext app = new AnnotationConfigApplicationContext(SpringConfig.class);

        // 获取类方式1
        StudentController student = app.getBean("studentController", StudentController.class);
        student.addStudent();
    }
}
