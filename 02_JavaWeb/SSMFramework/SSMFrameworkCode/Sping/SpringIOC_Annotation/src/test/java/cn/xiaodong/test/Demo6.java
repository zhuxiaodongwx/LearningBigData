package cn.xiaodong.test;

import cn.xiaodong.bean.Student;
import cn.xiaodong.config.SpringConfig;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.sql.SQLException;

/**
 * 测试对象之间的相互引用
 */
public class Demo6 {

    @Test
    public void test1() throws SQLException {
        // 创建容器
        ApplicationContext app = new AnnotationConfigApplicationContext(SpringConfig.class);

        // 获取类方式
        Student s2 = app.getBean("s2", Student.class);
        Student s3 = app.getBean("s3", Student.class);
        System.out.println(s2);
        System.out.println(s3);
    }
}
