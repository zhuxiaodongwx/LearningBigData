package cn.xiaodong.test;

import cn.xiaodong.bean.Grade;
import cn.xiaodong.bean.Student;
import cn.xiaodong.config.ASpringConfig;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.sql.SQLException;

/**
 * 配置类引入其他的配置类@Import
 */
public class Demo7 {

    @Test
    public void test1() throws SQLException {
        // 创建容器
        ApplicationContext app = new AnnotationConfigApplicationContext(ASpringConfig.class);

        // 获取类方式
        Student studentA = app.getBean("studentA", Student.class);
        Grade gradeB = app.getBean("gradeB", Grade.class);
        System.out.println(studentA);
        System.out.println(gradeB);
    }
}
