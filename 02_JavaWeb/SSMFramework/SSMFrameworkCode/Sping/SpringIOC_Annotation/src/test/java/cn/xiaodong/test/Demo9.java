package cn.xiaodong.test;

import cn.xiaodong.bean.Student;
import cn.xiaodong.config.ConditionalSpringConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

/**
 * Spring Test测试环境
 */

@SpringJUnitConfig(ConditionalSpringConfig.class)
public class Demo9 {

    @Autowired
    private Student studentA;


    @Test
    public void test1() {
        // 创建容器

        // 获取类方式

        System.out.println(studentA);

//        Student studentB = app.getBean("studentB", Student.class);
//        System.out.println(studentB);
    }
}
