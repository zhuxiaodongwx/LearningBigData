package cn.xiaodong.test;

import cn.xiaodong.bean.Student;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试@Component注解、自动注入默认值
 */
public class Demo1 {

    @Test
    public void test1() {
        // 创建容器（方法一：获取接口ApplicationContext）
        ApplicationContext app = new ClassPathXmlApplicationContext("SpringAnnotation.xml");

        // 获取类方式1
//        Student student = app.getBean("student", Student.class);
        Student student = app.getBean("s1", Student.class);
        System.out.println(student);
    }
}
