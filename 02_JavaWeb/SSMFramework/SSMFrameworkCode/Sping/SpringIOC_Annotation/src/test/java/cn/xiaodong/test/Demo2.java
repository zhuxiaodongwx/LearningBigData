package cn.xiaodong.test;

import cn.xiaodong.bean.Student;
import cn.xiaodong.controller.StudentController;
import cn.xiaodong.dao.impl.StudentDaoImpl;
import cn.xiaodong.service.impl.StudentServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试四个注解标签(@Component、@Repository、@Service、@Controller)，是否成功注册到容器中
 */
public class Demo2 {

    @Test
    public void test1() {
        // 创建容器（方法一：获取接口ApplicationContext）
        ApplicationContext app = new ClassPathXmlApplicationContext("SpringAnnotation.xml");

        // 获取类方式1
        Student student = app.getBean("s1", Student.class);
        StudentController studentController = app.getBean("studentController", StudentController.class);
        StudentDaoImpl studentDaoImpl = app.getBean("studentDaoImpl", StudentDaoImpl.class);
        StudentServiceImpl studentServiceImpl = app.getBean("studentServiceImpl", StudentServiceImpl.class);

        System.out.println(student);
        System.out.println(studentController);
        System.out.println(studentDaoImpl);
        System.out.println(studentServiceImpl);
    }
}
