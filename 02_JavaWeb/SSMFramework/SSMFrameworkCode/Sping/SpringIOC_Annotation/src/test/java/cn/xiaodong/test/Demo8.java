package cn.xiaodong.test;

import cn.xiaodong.bean.Student;
import cn.xiaodong.config.ConditionalSpringConfig;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.sql.SQLException;

/**
 * @Conditional  根据不同条件，创建不同的对象
 */
public class Demo8 {

    @Test
    public void test1() {
        // 创建容器
        ApplicationContext app = new AnnotationConfigApplicationContext(ConditionalSpringConfig.class);

        // 获取类方式
        Student studentA = app.getBean("studentA", Student.class);
        System.out.println(studentA);

//        Student studentB = app.getBean("studentB", Student.class);
//        System.out.println(studentB);
    }
}
