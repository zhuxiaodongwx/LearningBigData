package cn.xiaodong.test;

import cn.xiaodong.controller.StudentController;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 自动注入@Autowired
 * 测试Controller、Service、Dao能否正常调用
 */
public class Demo3 {

    @Test
    public void test1() {
        // 创建容器（方法一：获取接口ApplicationContext）
        ApplicationContext app = new ClassPathXmlApplicationContext("SpringAnnotation.xml");

        // 获取类方式1
        StudentController student = app.getBean("studentController", StudentController.class);
        student.addStudent();
    }
}
