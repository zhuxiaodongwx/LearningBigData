package cn.xiaodong.test;

import cn.xiaodong.config.SpringConfig;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * 测试配置中管理的数据源
 */
public class Demo5 {

    @Test
    public void test1() throws SQLException {
        // 创建容器
        ApplicationContext app = new AnnotationConfigApplicationContext(SpringConfig.class);

        // 获取类方式1
        DataSource dataSource = app.getBean("createDataSource", DataSource.class);
        System.out.printf(String.valueOf(dataSource.getConnection()));
    }
}
