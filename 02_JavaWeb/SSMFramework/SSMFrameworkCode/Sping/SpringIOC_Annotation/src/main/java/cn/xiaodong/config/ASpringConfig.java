package cn.xiaodong.config;

import cn.xiaodong.bean.Grade;
import cn.xiaodong.bean.Student;
import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

import javax.sql.DataSource;

//标注当前类是配置类，替代application.xml
@Configuration

//使用注解读取外部配置，替代 <context:property-placeholder标签
@PropertySource({"classpath:data.properties", "classpath:jdbc.properties"})

//使用@ComponentScan注解,可以配置扫描包,替代<context:component-scan标签
@ComponentScan(basePackages = {"cn.xiaodong.bean"})

// 引入其他配置类
// 这样加载配置类ASpringConfig时，会同时加载BSpringConfig配置类中的配置信息
@Import({BSpringConfig.class})

public class ASpringConfig {


    @Bean(name="studentA")
    public Student studentA() {
        Student studentA = new Student("小明", 18, 1);
        return studentA;
    }


}
