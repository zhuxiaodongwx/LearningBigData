package cn.xiaodong.bean;

import org.springframework.stereotype.Component;

/**
 * 代表学生年级的类。
 * 该类用于封装学生年级的相关信息，主要是年级名称。
 */
@Component  (value = "grade")
public class Grade {

    /**
     * 年级名称。
     */
    private String gname;

    /**
     * 默认构造方法。
     * 用于创建一个不指定年级名称的年级对象。
     */
    public Grade() {
    }

    /**
     * 带参数的构造方法。
     * 用于创建一个指定年级名称的年级对象。
     *
     * @param gname 年级名称。
     */
    public Grade(String gname) {
        this.gname = gname;
    }

    /**
     * 获取年级名称。
     *
     * @return 年级名称。
     */
    public String getGname() {
        return gname;
    }

    /**
     * 设置年级名称。
     *
     * @param gname 新的年级名称。
     */
    public void setGname(String gname) {
        this.gname = gname;
    }

    /**
     * 重写toString方法。
     * 用于以字符串形式表示年级对象，包含年级名称。
     *
     * @return 字符串形式的年级信息。
     */
    @Override
    public String toString() {
        return "Grade{" +
                "gname='" + gname + '\'' +
                '}';
    }
}
