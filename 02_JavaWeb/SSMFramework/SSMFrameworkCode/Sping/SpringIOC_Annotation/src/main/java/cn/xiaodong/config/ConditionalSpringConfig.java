package cn.xiaodong.config;

import cn.xiaodong.bean.Student;
import cn.xiaodong.util.IsLinux;
import cn.xiaodong.util.IsWindows;
import org.springframework.context.annotation.*;

//标注当前类是配置类，替代application.xml
@Configuration

//使用注解读取外部配置，替代 <context:property-placeholder标签
@PropertySource({"classpath:data.properties", "classpath:jdbc.properties"})

//使用@ComponentScan注解,可以配置扫描包,替代<context:component-scan标签
@ComponentScan(basePackages = {"cn.xiaodong.bean"})

/**
 * 根据操作系统，创建不同的对象
 */
public class ConditionalSpringConfig {


    @Bean
    @Conditional(IsWindows.class)
    public Student studentA() {
        Student student = new Student("小明A", 18, 1);
        return student;
    }

    @Bean
    @Conditional(IsLinux.class)
    public Student studentB() {
        Student student = new Student("小明B", 18, 1);
        return student;
    }


}
