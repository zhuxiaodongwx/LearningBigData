package cn.xiaodong.service.impl;

import cn.xiaodong.bean.Student;
import cn.xiaodong.dao.StudentDao;
import cn.xiaodong.service.StudentService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService{

    @Qualifier("studentDaoImpl")// 当有多个类可以装配时，需要手动指定装配的类
    @Autowired
//    @Resource(name = "studentDaoImpl")   // 这里的Resource注解，相当于@Qualifier("studentDaoImpl")+@Autowired
    private StudentDao studentDao;

    @Override
    public void addStudent(Student student) {
        System.out.println("StudentServiceImpl.addStudent");
        studentDao.addStudent(student);
    }
}
