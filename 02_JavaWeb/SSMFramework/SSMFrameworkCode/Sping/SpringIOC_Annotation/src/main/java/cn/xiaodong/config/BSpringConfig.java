package cn.xiaodong.config;

import cn.xiaodong.bean.Grade;
import cn.xiaodong.bean.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

//标注当前类是配置类，替代application.xml
@Configuration

//使用注解读取外部配置，替代 <context:property-placeholder标签
@PropertySource({"classpath:data.properties", "classpath:jdbc.properties"})

//使用@ComponentScan注解,可以配置扫描包,替代<context:component-scan标签
@ComponentScan(basePackages = {"cn.xiaodong.bean"})

public class BSpringConfig {


    @Bean
    public Grade gradeB() {
        Grade grade = new Grade();
        grade.setGname("B班");
        return grade;
    }


}
