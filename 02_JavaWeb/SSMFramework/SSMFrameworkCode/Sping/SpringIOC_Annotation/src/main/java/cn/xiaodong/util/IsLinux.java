package cn.xiaodong.util;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 判断当前系统是否是Linux系统
 */
public class IsLinux implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        System.out.println("判断当前系统是否是Linux系统");
        return context.getEnvironment().getProperty("os.name").contains("Linux");
    }
}
