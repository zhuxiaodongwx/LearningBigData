package cn.xiaodong.dao;


import cn.xiaodong.bean.Student;

public interface StudentDao {

    public void addStudent(Student student);
}
