package cn.xiaodong.config;

import cn.xiaodong.bean.Grade;
import cn.xiaodong.bean.Student;
import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

//标注当前类是配置类，替代application.xml
@Configuration

//使用注解读取外部配置，替代 <context:property-placeholder标签
@PropertySource({"classpath:data.properties", "classpath:jdbc.properties"})

//使用@ComponentScan注解,可以配置扫描包,替代<context:component-scan标签
@ComponentScan(basePackages = {"cn.xiaodong"})

public class SpringConfig {

//    @Value("${jdbc.user}")
//    private String username;
//    @Value("${jdbc.password}")
//    private String password;
//    @Value("${jdbc.url}")
//    private String url;
//    @Value("${jdbc.driver}")
//    private String driverClassName;
//
//    //如果第三方类进行IoC管理,无法直接使用@Component相关注解
//    //解决方案: xml方式可以使用<bean>标签
//    //解决方案: 配置类方式,可以使用方法返回值+@Bean注解
//    @Bean
//    public DataSource createDataSource() {
//        //使用Java代码实例化
//        DruidDataSource dataSource = new DruidDataSource();
//        dataSource.setUsername(username);
//        dataSource.setPassword(password);
//        dataSource.setUrl(url);
//        dataSource.setDriverClassName(driverClassName);
//        //返回结果即可
//        return dataSource;
//    }


    //如果第三方类进行IoC管理,无法直接使用@Component相关注解
    //解决方案: xml方式可以使用<bean>标签
    //解决方案: 配置类方式,可以使用方法返回值+@Bean注解
    @Bean
    public DataSource createDataSource(@Value("${jdbc.user}") String username,
                                       @Value("${jdbc.password}") String password,
                                       @Value("${jdbc.url}") String url,
                                       @Value("${jdbc.driver}") String driverClassName) {
        //使用Java代码实例化
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setUrl(url);
        dataSource.setDriverClassName(driverClassName);
        //返回结果即可
        return dataSource;
    }

    // ------------------------------------对象之间相互引用--------------------------------------------

    @Bean
    public Student s2() {
        Student s2 = new Student("小明", 18, 1);
        s2.setGrade(g1());
        return s2;
    }

    @Bean
    public Student s3(Grade g1) {
        Student s2 = new Student("小明", 18, 1);
        s2.setGrade(g1);
        return s2;
    }

    @Bean
    public Grade g1() {
        Grade g1 = new Grade("一年级");
        return g1;
    }



}
