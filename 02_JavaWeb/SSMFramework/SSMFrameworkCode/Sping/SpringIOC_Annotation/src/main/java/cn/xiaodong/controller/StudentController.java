package cn.xiaodong.controller;


import cn.xiaodong.bean.Student;
import cn.xiaodong.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService ;

    public void addStudent() {

        // 调用Service
        System.out.println("StudentController--addStudent");

        studentService.addStudent(new Student("小明", 18, 1));
    }

    ;
}
