package cn.xiaodong.test;

import cn.xiaodong.bean.Student;
import cn.xiaodong.config.SpringAOPConfig;
import cn.xiaodong.dao.StudentDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

// 加载Spring的配置
// 使用配置类，配置AOP
@SpringJUnitConfig(SpringAOPConfig.class)
public class Demo2 {

    @Autowired
    StudentDao studentDao;

    @Test
    public void test1() {
        studentDao.addStudent(new Student("小明", 18, 1));
    }
}
