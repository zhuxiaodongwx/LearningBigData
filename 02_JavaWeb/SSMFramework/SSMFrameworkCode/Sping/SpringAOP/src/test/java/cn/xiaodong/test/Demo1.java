package cn.xiaodong.test;

import cn.xiaodong.bean.Student;
import cn.xiaodong.dao.StudentDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

// 加载Spring的配置
// 使用XML配置AOP
@SpringJUnitConfig(locations = "classpath:SpringAOP.xml")
public class Demo1 {

    @Autowired
    StudentDao studentDao;

    @Test
    public void test1() {
        studentDao.addStudent(new Student("小明", 18, 1));
    }
}
