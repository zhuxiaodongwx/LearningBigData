package cn.xiaodong.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Student类表示一个学生，包含了学生的姓名、年龄和ID。
 */
@Component("student") // 默认对象名，类名首字母小写
public class Student {

    // 学生的姓名
//    @Value("${sname}")//给属性赋值，${sname}在data.properties中
    private String name;

    // 学生的年龄
//    @Value("${sage}")
    private int age;

    // 学生的唯一标识ID
    private int id;

//    // 学生的所属年级
//    private Grade grade;

    /**
     * 默认构造方法，用于创建一个空的学生对象。
     */
    public Student() {
    }

    /**
     * 带参数的构造方法，用于创建一个具有指定姓名、年龄和ID的学生对象。
     *
     * @param name 学生的姓名
     * @param age 学生的年龄
     * @param id 学生的唯一标识ID
     */
    public Student(String name, int age, int id) {
        this.name = name;
        this.age = age;
        this.id = id;
    }

    /**
     * 获取学生的姓名。
     *
     * @return 学生的姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置学生的姓名。
     *
     * @param name 要设置的学生的姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取学生的年龄。
     *
     * @return 学生的年龄
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置学生的年龄。
     *
     * @param age 要设置的学生的年龄
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取学生的唯一标识ID。
     *
     * @return 学生的唯一标识ID
     */
    public int getId() {
        return id;
    }

    /**
     * 设置学生的唯一标识ID。
     *
     * @param id 要设置的学生的唯一标识ID
     */
    public void setId(int id) {
        this.id = id;
    }

//    public Grade getGrade() {
//        return grade;
//    }
//
//    public void setGrade(Grade grade) {
//        this.grade = grade;
//    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", id=" + id +
//                ", grade=" + grade +
                '}';
    }
}
