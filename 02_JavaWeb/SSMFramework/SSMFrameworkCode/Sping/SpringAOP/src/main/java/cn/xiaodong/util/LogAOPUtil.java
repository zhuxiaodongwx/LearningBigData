package cn.xiaodong.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
// 注册为切面编程
@Aspect

public class LogAOPUtil {


    //前置通知：在被代理的目标方法前执行
    // 切入点（PointCut）表达式
    // 此处引入其他地方定义的切入点表达式
    @Before(value = "cn.xiaodong.config.SpringAOPConfig.myPointCut1()")
    public void printLogBeforeCore(JoinPoint joinPoint) {
        // 1.通过JoinPoint对象获取目标方法签名对象
        // 方法的签名：一个方法的全部声明信息
        Signature signature = joinPoint.getSignature();

        // 2.通过方法的签名对象获取目标方法的详细信息
        String methodName = signature.getName();
        System.out.println("【切面层】[AOP前置通知] methodName = " + methodName);

        int modifiers = signature.getModifiers();
        System.out.println("【切面层】[AOP前置通知] modifiers = " + modifiers);

        String declaringTypeName = signature.getDeclaringTypeName();
        System.out.println("【切面层】[AOP前置通知] declaringTypeName = " + declaringTypeName);

        // 3.通过JoinPoint对象获取外界调用目标方法时传入的实参列表
        Object[] args = joinPoint.getArgs();

        // 4.由于数组直接打印看不到具体数据，所以转换为List集合
        List<Object> argList = Arrays.asList(args);

        System.out.println("【切面层】[AOP前置通知]  " + methodName + "方法开始了，参数列表：" + argList);

        System.out.println("【切面层】[AOP前置通知] 方法开始了");
    }

    // 返回通知：在被代理的目标方法成功结束后执行（寿终正寝）
    // targetMethodReturnValue是方法返回值
    @AfterReturning(value = "cn.xiaodong.config.SpringAOPConfig.myPointCut1()", returning = "targetMethodReturnValue")
    public void printLogAfterSuccess(JoinPoint joinPoint, Object targetMethodReturnValue) {
        if (targetMethodReturnValue != null) {
            System.out.println("【切面层】[AOP返回通知] 方法返回结果" + targetMethodReturnValue.toString());
        } else {
            System.out.println("【切面层】[AOP返回通知] 方法返回结果为空");
        }
        System.out.println("【切面层】[AOP返回通知] 方法成功返回了");
    }

    // 异常通知：在被代理的目标方法异常结束后执行（死于非命）
    @AfterThrowing(value = "cn.xiaodong.config.SpringAOPConfig.myPointCut1()", throwing = "e")
    public void printLogAfterException(JoinPoint joinPoint, Exception e) {
        System.out.println("【切面层】[AOP异常通知] 异常为" + e.toString());
        System.out.println("【切面层】[AOP异常通知] 方法抛异常了");
    }

    // 后置通知：在被代理的目标方法最终结束后执行（盖棺定论）
    @After(value = "cn.xiaodong.config.SpringAOPConfig.myPointCut1()")
    public void printLogFinallyEnd(JoinPoint joinPoint) {
        System.out.println("【切面层】[AOP后置通知] 方法最终结束了");
    }

    @Around(value = "cn.xiaodong.config.SpringAOPConfig.myPointCut1()")
    public Object printLogSurround(ProceedingJoinPoint joinPoint) {
        // 通过ProceedingJoinPoint对象获取外界调用目标方法时传入的实参数组
        Object[] args = joinPoint.getArgs();

        // 通过ProceedingJoinPoint对象获取目标方法的签名对象
        Signature signature = joinPoint.getSignature();

        // 通过签名对象获取目标方法的方法名
        String methodName = signature.getName();

        // 声明变量用来存储目标方法的返回值
        Object targetMethodReturnValue = null;

        try {

            // 在目标方法执行前：开启事务（模拟）
            System.out.println("【切面层】[AOP 环绕通知] 开启事务，方法名：" + methodName + "，参数列表：" + Arrays.asList(args));

            // 过ProceedingJoinPoint对象调用目标方法
            // 目标方法的返回值一定要返回给外界调用者
            targetMethodReturnValue = joinPoint.proceed(args);

            // 在目标方法成功返回后：提交事务（模拟）
            System.out.println("【切面层】[AOP 环绕通知] 提交事务，方法名：" + methodName + "，方法返回值：" + targetMethodReturnValue);

        } catch (Throwable e) {

            // 在目标方法抛异常后：回滚事务（模拟）
            System.out.println("【切面层】[AOP 环绕通知] 回滚事务，方法名：" + methodName + "，异常：" + e.getClass().getName());

        } finally {

            // 在目标方法最终结束后：释放数据库连接
            System.out.println("【切面层】[AOP 环绕通知] 释放数据库连接，方法名：" + methodName);

        }

        return targetMethodReturnValue;
    }

}
