/**
 * 学生数据访问接口
 * 该接口定义了对学生数据的增删操作，通过实现此接口的类可以具体实现这些操作。
 */
package cn.xiaodong.dao;

import cn.xiaodong.bean.Student;

public interface StudentDao {
    /**
     * 添加学生信息
     * @param student 待添加的学生对象，包含学生的所有信息
     * 此方法用于将一个新的学生对象添加到数据源中，实现具体的数据存储逻辑。
     */
    public void addStudent(Student student);

    /**
     * 删除学生信息
     * @param student 待删除的学生对象，通过此对象的信息定位并删除对应的学生数据
     * 此方法用于从数据源中删除一个学生对象，实现具体的数据删除逻辑。
     */
    public void delStudent(Student student);
}
