package cn.xiaodong.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 切面优先级测试
 */

@Component
// 注册为切面编程
@Aspect
//// 切面优先级, 值越小优先级越高
@Order(5)
public class LogAOPUtil2 {


    //前置通知：在被代理的目标方法前执行
    // 切入点（PointCut）表达式
    // 此处引入其他地方定义的切入点表达式
    @Before(value = "cn.xiaodong.config.SpringAOPConfig.myPointCut1()")
    public void printLogBeforeCore(JoinPoint joinPoint) {

        System.out.println("【切面层】[AOP前置通知--2] 方法开始了");
    }

    // 返回通知：在被代理的目标方法成功结束后执行（寿终正寝）
    // targetMethodReturnValue是方法返回值
    @AfterReturning(value = "cn.xiaodong.config.SpringAOPConfig.myPointCut1()", returning = "targetMethodReturnValue")
    public void printLogAfterSuccess(JoinPoint joinPoint, Object targetMethodReturnValue) {
        System.out.println("【切面层】[AOP返回通知--2] 方法成功返回了");
    }


}
