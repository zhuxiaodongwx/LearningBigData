package cn.xiaodong.dao.impl;

import cn.xiaodong.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * 学生数据访问对象的实现类，使用JdbcTemplate进行数据库操作。
 * @author Xiaodong
 */
@Repository
public class StudentDaoImpl implements StudentDao {

    /**
     * 自动注入JdbcTemplate，用于执行SQL操作。
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 根据ID更新学生的姓名。
     * @param name 新的姓名
     * @param id 学生的ID
     */
    @Override
    public void updateNameById(String name, Integer id) {
        // 定义SQL语句，更新学生的姓名
        String sql = "update students set name = ? where id = ?";
        // 使用JdbcTemplate执行SQL更新操作
        jdbcTemplate.update(sql, name, id);
    }

    /**
     * 根据ID更新学生的年龄。
     * @param age 新的年龄
     * @param id 学生的ID
     */
    @Override
    public void updateAgeById(Integer age, Integer id) {
        // 定义SQL语句，更新学生的年龄
        String sql = "update students set age = ? where id = ?";
        // 使用JdbcTemplate执行SQL更新操作
        jdbcTemplate.update(sql, age, id);
    }
}
