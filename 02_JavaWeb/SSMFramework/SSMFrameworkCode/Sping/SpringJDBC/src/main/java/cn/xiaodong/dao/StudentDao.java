package cn.xiaodong.dao;

public interface StudentDao {

    public void updateNameById(String name, Integer id) ;

    public void updateAgeById(Integer age, Integer id);
}
