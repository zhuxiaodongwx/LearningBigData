package cn.xiaodong.service.impl;

import cn.xiaodong.dao.StudentDao;
import cn.xiaodong.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentDao studentDao;


    /**
     * 基于XML配置的事务管理
     */
    //    @Transactional
    @Override
    public void changeInfo() {
        studentDao.updateAgeById(25, 1);
//        System.out.println(5 / 0);
        studentDao.updateNameById("李四", 1);
    }

    /**
     * 基于注解的事务管理
     */
    @Transactional
    @Override
    public void changeInfo2() {
        studentDao.updateAgeById(25, 1);
//        System.out.println(5 / 0);
        studentDao.updateNameById("李四", 1);
    }
}
