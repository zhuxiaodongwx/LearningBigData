package cn.xiaodong.bean;

import org.springframework.stereotype.Component;

/**
 * 学生类，代表一个学生的信息。
 * 使用@Component注解将其注册为一个Spring Bean，命名为"student"。
 */
@Component(value = "student")
public class Student {

    /**
     * 学生的唯一标识符。
     */
    private int id;

    /**
     * 学生的姓名。
     */
    private String name;

    /**
     * 学生的性别。
     */
    private String gender;

    /**
     * 学生的年龄
     */
    private Integer age;

    /**
     * 学生所在的班级名称。
     */
    private String className;


    /**
     * 获取学生的ID。
     * @return 学生的唯一标识ID。
     */
    public int getId() {
        return id;
    }

    /**
     * 设置学生的ID。
     * @param id 要设置的学生的唯一标识ID。
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 获取学生的名字。
     * @return 学生的姓名。
     */
    public String getName() {
        return name;
    }

    /**
     * 设置学生的名字。
     * @param name 要设置的学生的姓名。
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取学生的性别。
     * @return 学生的性别。
     */
    public String getGender() {
        return gender;
    }

    /**
     * 设置学生的性别。
     * @param gender 要设置的学生的性别。
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * 获取学生的年龄。
     * @return 学生的年龄，可能为null。
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 设置学生的年龄。
     * @param age 要设置的学生的年龄，允许为null。
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * 获取学生所在的班级名称。
     * @return 学生所在的班级名称。
     */
    public String getClassName() {
        return className;
    }

    /**
     * 设置学生所在的班级名称。
     * @param className 要设置的 student 所在的班级名称。
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * 返回学生信息的字符串表示。
     * @return 包含学生ID、姓名、性别、年龄和班级名称的字符串。
     */
    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", className='" + className + '\'' +
                '}';
    }
}
