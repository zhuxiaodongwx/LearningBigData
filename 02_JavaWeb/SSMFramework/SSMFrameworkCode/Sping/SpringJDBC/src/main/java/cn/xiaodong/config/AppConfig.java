package cn.xiaodong.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * projectName: com.atguigu.config
 *
 * description: 项目整体配置类
 */
@Import(value = {DataSourceConfig.class, TxConfig.class})
@ComponentScan(basePackages = "cn.xiaodong")
@Configuration
public class AppConfig {

}
