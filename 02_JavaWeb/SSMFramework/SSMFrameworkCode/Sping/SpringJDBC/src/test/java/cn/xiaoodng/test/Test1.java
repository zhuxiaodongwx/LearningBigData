package cn.xiaoodng.test;

import cn.xiaodong.service.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

/**
 * 测试XML配置，注册事务
 */
@SpringJUnitConfig(locations = "classpath:SpringJDBC.xml")
public class Test1 {

    @Autowired
    StudentService studentService;

    @Test
    public void test1() {
        // 测试前，需要删掉事务的注解
        studentService.changeInfo();
    }
}
