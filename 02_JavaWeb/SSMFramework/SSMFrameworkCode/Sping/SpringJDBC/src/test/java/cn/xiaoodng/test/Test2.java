package cn.xiaoodng.test;

import cn.xiaodong.config.AppConfig;
import cn.xiaodong.service.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

/**
 * 测试纯注解方式，注册事务
 */
@SpringJUnitConfig(AppConfig.class)
public class Test2 {

    @Autowired
    StudentService studentService;

    @Test
    public void test1() {
        // 测试前，需要添加事务的注解
        studentService.changeInfo2();
    }
}
