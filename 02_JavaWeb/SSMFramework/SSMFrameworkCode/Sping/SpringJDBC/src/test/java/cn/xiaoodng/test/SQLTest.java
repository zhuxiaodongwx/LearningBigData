package cn.xiaoodng.test;

import cn.xiaodong.bean.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.List;

/**
 * JdbcTemplate 执行SQL语句
 */
@SpringJUnitConfig(locations = "classpath:SpringJDBC.xml")
public class SQLTest {

    @Autowired
    private JdbcTemplate  jdbcTemplate;

    /**
     * 使用jdbcTemplate实现DML操作
     */
    @Test
    public void testDML(){

        //执行插入一条学员数据
        String sql = "insert into students (id,name,gender,age,classname) values (?,?,?,?,?);";
    /*
        参数1: sql语句
        参数2: 可变参数,占位符的值
     */
        int rows = jdbcTemplate.update(sql, 9,"十一", "男", 18, "二年三班");

        System.out.println("rows = " + rows);
    }

    /**
     * 返回单个简单类型
     */
    @Test
    public void testDQLForString(){

        String sql = "select name from students where id = ? ;";
    /*
        参数1: sql语句
        参数2: 返回单值类型
        参数3: 可变参数,占位符的值
     */
        String name = jdbcTemplate.queryForObject(sql, String.class, 1);

        System.out.println("name = " + name);
    }

    /**
     * 查询单条实体对象
     */
    @Test
    public void testDQLForPojo(){

        String sql = "select id , name , age , gender , classname from students where id = ? ;";

        Student student = jdbcTemplate.queryForObject(sql,  (rs, rowNum) -> {
            //自己处理结果映射
            Student stu = new Student();
            stu.setId(rs.getInt("id"));
            stu.setName(rs.getString("name"));
            stu.setAge(rs.getInt("age"));
            stu.setGender(rs.getString("gender"));
            stu.setClassName(rs.getString("classname"));
            return stu;
        }, 1);

        System.out.println("student = " + student);
    }

    /**
     * 查询实体类集合
     */
    @Test
    public void testDQLForListPojo(){

        String sql = "select id , name , age , gender , classname  from students where id > ? ;";

    /*
        query可以返回集合!
        BeanPropertyRowMapper就是封装好RowMapper的实现,要求属性名和列名相同即可
     */
        List<Student> studentList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Student.class), 1);

        System.out.println("studentList = " + studentList);
    }
}
