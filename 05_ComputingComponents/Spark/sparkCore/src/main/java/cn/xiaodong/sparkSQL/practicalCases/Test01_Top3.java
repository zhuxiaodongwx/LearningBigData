package cn.xiaodong.sparkSQL.practicalCases;

import cn.xiaodong.sparkSQL.practicalCases.function.CityMark;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.*;

public class Test01_Top3 {

    public static void main(String[] args) {
        // 1. 创建sparkConf配置对象
        SparkConf conf = new SparkConf().setAppName("Test01_Top3").setMaster("local[*]");

        // 2. 创建sparkSession连接对象
        SparkSession sparkSession = SparkSession.builder().enableHiveSupport().config(conf).getOrCreate();

        //注册自定义函数
        CityMark cityMark = new CityMark();
        sparkSession.udf().register("cityMark", functions.udaf(cityMark, Encoders.STRING()) );

        // 数据源 Hive Test数据库

        // （1）多表联查，获取基本数据 （包括城市备注列）
        Dataset<Row> t1dataset = sparkSession.sql("select area, product_name, count(*) count,cityMark(c.city_name) mark\n" +
                "from test.user_visit_action v\n" +
                "         left join test.city_info c on v.city_id = c.city_id\n" +
                "         left join test.product_info p on p.product_id = v.click_product_id\n" +
                "where v.click_product_id != '-1'\n" +
                "group by product_name, area");

        t1dataset.createOrReplaceTempView("t1");

        // （2）取出前三的数据
        Dataset<Row> t2dataset = sparkSession.sql("select t2.area, t2.product_name, t2.count, t2.mark\n" +
                "from (select t1.area, t1.product_name, t1.count, t1.mark, rank() over (partition by area order by count desc) rank\n" +
                "      from  t1) t2\n" +
                "where t2.rank <=3");

        // 输出结果
//        t2dataset.write().mode(SaveMode.Overwrite).csv("output/top3");
        t2dataset.show(100,false);

        // 4、关闭连接
        sparkSession.close();
    }
}
