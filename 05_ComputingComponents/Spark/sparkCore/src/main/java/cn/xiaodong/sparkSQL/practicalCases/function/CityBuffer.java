package cn.xiaodong.sparkSQL.practicalCases.function;

import java.io.Serializable;
import java.util.HashMap;

/**
 * CityMark函数，自定义计算缓冲
 */
public class CityBuffer implements Serializable {

    /**
     * 总点击次数
     */
    private Long count;

    /**
     * 每个城市的点击次数
     */
    private HashMap<String,Long> records;

    public Long getCount() {
        return count;
    }

    public CityBuffer() {
    }

    public CityBuffer(Long count, HashMap<String, Long> records) {
        this.count = count;
        this.records = records;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public HashMap<String, Long> getRecords() {
        return records;
    }

    public void setRecords(HashMap<String, Long> records) {
        this.records = records;
    }
}
