package cn.xiaodong.sparkSQL.practicalCases.function;


import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.expressions.Aggregator;

import java.text.DecimalFormat;
import java.util.*;

/**
 * 自定义函数 CityMark
 */
public class CityMark extends Aggregator<String, CityBuffer, String> {
    /**
     * 初始化
     *
     * @return
     */
    @Override
    public CityBuffer zero() {
        return new CityBuffer(0L, new HashMap<String, Long>());
    }

    /**
     * 单个分区，多条数据汇总
     *
     * @param cityBuffer
     * @param cityName
     * @return
     */
    @Override
    public CityBuffer reduce(CityBuffer cityBuffer, String cityName) {
        // 更新总点击次数
        long count = cityBuffer.getCount() + 1;
        cityBuffer.setCount(count);

        // 更新城市点击次数
        HashMap<String, Long> records = cityBuffer.getRecords();
        Long cityCount = records.getOrDefault(cityName, 0L);

        records.put(cityName, cityCount + 1);
        cityBuffer.setRecords(records);

        return cityBuffer;
    }

    /**
     * 不同分区数据聚合
     *
     * @param cb1
     * @param cb2
     * @return
     */
    @Override
    public CityBuffer merge(CityBuffer cb1, CityBuffer cb2) {
        CityBuffer cityBuffer = new CityBuffer();

        // 计算点击次数总和
        cityBuffer.setCount(cb1.getCount() + cb2.getCount());

        // 汇总城市的点击次数
        HashMap<String, Long> cb1Records = cb1.getRecords();
        HashMap<String, Long> cb2Records = cb2.getRecords();

        // 遍历第二个Map，与第一个合并，将点击次数相加
        for (Map.Entry<String, Long> cb2entry : cb2Records.entrySet()) {
            String cityName2 = cb2entry.getKey();
            Long cityCount2 = cb2entry.getValue();

            Long cityCount1 = cb1Records.getOrDefault(cityName2, 0L);

            if (cityCount1 == 0L) {
                cb1Records.put(cityName2, cityCount2);
            } else {
                cb1Records.put(cityName2, cityCount1 + cityCount2);
            }
        }
        cityBuffer.setRecords(cb1Records);

        return cityBuffer;
    }

    /**
     * 最终计算结果展示
     * @param cityBuffer
     * @return
     */
    @Override
    public String finish(CityBuffer cityBuffer) {
        // 最终显示结果
        String result = "";

        // 每个城市的记录
        HashMap<String, Long> records = cityBuffer.getRecords();
        // 全部点击总数
        Long count = cityBuffer.getCount();

        // 每个城市的记录，按照value值（点击数），从大到小排序，并取出前两个值
        // 将HashMap中的Entry对象存储到List中
        List<Map.Entry<String, Long>> entryList = new ArrayList<>(records.entrySet());

        // 使用Comparator对List中的Entry对象按Long值从大到小排序
        Collections.sort(entryList, new Comparator<Map.Entry<String, Long>>() {
            @Override
            public int compare(Map.Entry<String, Long> entry1, Map.Entry<String, Long> entry2) {
                return entry2.getValue().compareTo(entry1.getValue());
            }
        });

        // 其他城市点击数的百分比
        Double restPercent = 100D;

        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        // 取出前两个记录
        for (int i = 0; i < Math.min(entryList.size(), 2); i++) {
            Map.Entry<String, Long> entry = entryList.get(i);
            Long value = entry.getValue();
            // 当前城市的百分比
            double cityPercent = (double) entry.getValue() / count * 100;

            // 城市百分比，保留一位小数
            String persentFormatted = decimalFormat.format(cityPercent);

            result += entry.getKey() + persentFormatted + "%" + ",";
            restPercent -= cityPercent;
        }

        // 其他城市的点击数百分比
        String restPersentFormatted = decimalFormat.format(restPercent);
        result += "其他" + restPersentFormatted + "%";

        return result;
    }

    /**
     * 中间计算缓冲对象的编码器
     * @return
     */
    @Override
    public Encoder<CityBuffer> bufferEncoder() {
        return Encoders.kryo(CityBuffer.class);
    }

    /**
     * 返回结果类型
     * @return
     */
    @Override
    public Encoder<String> outputEncoder() {
        return Encoders.STRING();
    }
}
