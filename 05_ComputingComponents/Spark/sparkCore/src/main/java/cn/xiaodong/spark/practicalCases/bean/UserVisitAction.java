package cn.xiaodong.spark.practicalCases.bean;

import java.io.Serializable;

/**
 * 用户行为日志
 */
public class UserVisitAction implements Serializable {

    /**
     * 用户点击行为的日期
     */
    private String date;


    /**
     * 用户的ID
     */
    private String user_id;


    /**
     * session_id
     */
    private String session_id;


    /**
     * 某个页面的ID
     */
    private String page_id;


    /**
     * 动作的时间点     5
     */
    private String action_time;

    /**
     * 用户搜索的关键词
     */
    private String search_keyword;

    /**
     * 点击某一个商品品类的ID     7
     */
    private String click_category_id;

    /**
     * 某一个商品的ID
     */
    private String click_product_id;

    /**
     * 一次订单中所有品类的ID集合
     */
    private String order_category_ids;

    /**
     * 一次订单中所有商品的ID集合      10
     */
    private String order_product_ids;

    /**
     * 一次支付中所有品类的ID集合
     */
    private String pay_category_ids;

    /**
     * 一次支付中所有商品的ID集合
     */
    private String pay_product_ids;

    /**
     * 城市 id
     */
    private String city_id;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getPage_id() {
        return page_id;
    }

    public void setPage_id(String page_id) {
        this.page_id = page_id;
    }

    public String getAction_time() {
        return action_time;
    }

    public void setAction_time(String action_time) {
        this.action_time = action_time;
    }

    public String getSearch_keyword() {
        return search_keyword;
    }

    public void setSearch_keyword(String search_keyword) {
        this.search_keyword = search_keyword;
    }

    public String getClick_category_id() {
        return click_category_id;
    }

    public void setClick_category_id(String click_category_id) {
        this.click_category_id = click_category_id;
    }

    public String getClick_product_id() {
        return click_product_id;
    }

    public void setClick_product_id(String click_product_id) {
        this.click_product_id = click_product_id;
    }

    public String getOrder_category_ids() {
        return order_category_ids;
    }

    public void setOrder_category_ids(String order_category_ids) {
        this.order_category_ids = order_category_ids;
    }

    public String getOrder_product_ids() {
        return order_product_ids;
    }

    public void setOrder_product_ids(String order_product_ids) {
        this.order_product_ids = order_product_ids;
    }

    public String getPay_category_ids() {
        return pay_category_ids;
    }

    public void setPay_category_ids(String pay_category_ids) {
        this.pay_category_ids = pay_category_ids;
    }

    public String getPay_product_ids() {
        return pay_product_ids;
    }

    public void setPay_product_ids(String pay_product_ids) {
        this.pay_product_ids = pay_product_ids;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    @Override
    public String toString() {
        return "CategoryCountInfo{" +
                "date='" + date + '\'' +
                ", user_id='" + user_id + '\'' +
                ", session_id='" + session_id + '\'' +
                ", page_id='" + page_id + '\'' +
                ", action_time='" + action_time + '\'' +
                ", search_keyword='" + search_keyword + '\'' +
                ", click_category_id='" + click_category_id + '\'' +
                ", click_product_id='" + click_product_id + '\'' +
                ", order_category_ids='" + order_category_ids + '\'' +
                ", order_product_ids='" + order_product_ids + '\'' +
                ", pay_category_ids='" + pay_category_ids + '\'' +
                ", pay_product_ids='" + pay_product_ids + '\'' +
                ", city_id='" + city_id + '\'' +
                '}';
    }

    /**
     * 默认构造函数
     * @param date
     * @param user_id
     * @param session_id
     * @param page_id
     * @param action_time
     * @param search_keyword
     * @param click_category_id
     * @param click_product_id
     * @param order_category_ids
     * @param order_product_ids
     * @param pay_category_ids
     * @param pay_product_ids
     * @param city_id
     */
    public UserVisitAction(String date, String user_id, String session_id, String page_id, String action_time,
                           String search_keyword, String click_category_id, String click_product_id,
                           String order_category_ids, String order_product_ids, String pay_category_ids,
                           String pay_product_ids, String city_id) {
        this.date = date;
        this.user_id = user_id;
        this.session_id = session_id;
        this.page_id = page_id;
        this.action_time = action_time;
        this.search_keyword = search_keyword;
        this.click_category_id = click_category_id;
        this.click_product_id = click_product_id;
        this.order_category_ids = order_category_ids;
        this.order_product_ids = order_product_ids;
        this.pay_category_ids = pay_category_ids;
        this.pay_product_ids = pay_product_ids;
        this.city_id = city_id;
    }

    /**
     * 用一行日志构造
     * @param logLine 一行用户日志
     */
    public UserVisitAction(String logLine) {

        String[] split = logLine.split("_");

        this.date = split[0];
        this.user_id = split[1];
        this.session_id = split[2];
        this.page_id = split[3];
        this.action_time = split[4];
        this.search_keyword = split[5];
        this.click_category_id = split[6];
        this.click_product_id = split[7];
        this.order_category_ids = split[8];
        this.order_product_ids = split[9];
        this.pay_category_ids = split[10];
        this.pay_product_ids = split[11];
        this.city_id = split[12];
    }
}
