package cn.xiaodong.sparkSQL.readWrite;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import java.util.Properties;

/**
 * sparksql写入数据
 */
public class Test02_WriteData {

    /**
     *   sparksql写入数据有两种方式:
     *          1、ds.write()
     *              .mode(SaveMode.XXX) --指定保存的模式
     *              .format("text/json/orc/parquet/csv/jdbc") --指定数据的写入格式
     *              [.option(k,v)...]
     *              .save([path])
     *         2、ds.write().mode(SaveMode.XXX) [.option(k,v)...].text/json/orc/parquet/csv/jdbc
     */
    public static void main(String[] args) {

        SparkSession spark = SparkSession.builder().master("local[4]").appName("test").getOrCreate();


        Dataset<Row> ds = spark.read().option("header", "true").option("sep", "#").csv("input/datas.txt");

        //保存为json文件
        //常用的写入模式:
        //      SaveMode.Overwrite: 如果保存数据的目录/表已经存在则覆盖数据
        //      SaveMode.Append: 如果保存数据的目录/表已经存在则追加数据
        ds.write().mode(SaveMode.Overwrite).json("output/json");

        //保存为csv文件
        //   sep: 指定保存数据到csv文件的时候字段之间的分隔符
        //   header: 指定保存数据到csv文件的时候将列名作为文件第一行保存
        ds.write().mode(SaveMode.Overwrite).option("sep","\t").option("header","true").csv("output/csv");

        //保存为文本【数据源有多个列不能保存为文本,只有一个列才行】
        ds.write().mode(SaveMode.Overwrite).text("output/text");

        //保存数据到mysql
        Properties props = new Properties();
        props.setProperty("user","root");
        props.setProperty("password","root");
        ds.write().mode(SaveMode.Overwrite).jdbc("jdbc:mysql://hadoop102:3306/test","test",props);
    }
}
