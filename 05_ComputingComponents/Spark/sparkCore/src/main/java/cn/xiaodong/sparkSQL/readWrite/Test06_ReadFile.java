package cn.xiaodong.sparkSQL.readWrite;

import org.apache.spark.sql.SparkSession;

/**
 * sparkSQL 文件读取
 */
public class Test06_ReadFile {

    /**
     * sparksql读取文件有两种方式
     *      1、spark.read()
     *              .format("csv/text/parquet/orc/jdbc/json") --指定读取的数据格式
     *              [.option(k,v).option(k,v)...]  --指定数据读取需要的参数
     *              .load([path]) --加载数据
     *      2、spark.read()[.option(k,v).option(k,v)...].json/csv/text/parquet/orc(path) 【工作常用】
     *
     */
    public static void main(String[] args) {

        SparkSession spark = SparkSession.builder().master("local[4]").appName("test").getOrCreate();

        //读取文本文件
        //==================第一种方式读取==================
        spark.read().format("text").load("input/datas.txt").show();

        //==================第二种方式==================
        spark.read().text("input/datas.txt").show();

        //读取json[行]文件[以json中的属性名作为列名,属性值作为列的值]
        spark.read().json("input/js.txt").show();

        //读取csv文件
        //文件中列之间以固定分隔符分割的就可以当做csv文件读取
        //读取csv文件常用的option:
        //      sep: 指定字段之间的分隔符
        //      header: 指定以文件的第一行作为列名
        spark.read().option("sep","#").option("header","true").csv("input/datas.txt").show();

        //读取parquet/orc
        //spark.read().parquet("path");
        //spark.read().orc("path");


    }
}
