package cn.xiaodong.sparkSQL.hive;

import org.apache.spark.sql.SparkSession;

/**
 * sparkSQL连接Hive，执行SQL
 */
public class Test03_SparkHive {

    /**
     *   idea操作hive
     *          1、将spark-hive、mysql依赖加入pom文件
     *          2、将hive-site.xml文件放入resource目录中
     *          3、在创建sparksession对象的时候开启hive的支持
     *          4、通过sql操作hive数据
     * @param args
     */
    public static void main(String[] args) {

        // HDFSl连接设置
        System.setProperty("HADOOP_USER_NAME","atguigu");

        // 获取sparkSession，开启Hive支持
        SparkSession sparkSession = SparkSession.builder()
                .master("local[4]")
                .appName("test")
                .enableHiveSupport() //开启hive支持
                .getOrCreate();

        //读取hive数据
        sparkSession.sql("select * from gmall0509.tmp_dim_date_info").show();

        //写入数据到hive
        sparkSession.sql("insert into table person select * from person");
    }
}
