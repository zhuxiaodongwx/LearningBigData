package cn.xiaodong.spark.rdd.wordCount;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;

/**
 * RDD实现WordCount
 *
 * 运行环境：本地
 */
public class WordCount {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("WordCount");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码

        // 从文件中创建RDD
        JavaRDD<String> wordCount = sc.textFile("wordCount", 2);

        // 根据空格，对每一行进行切分
        JavaRDD<String> flatMap = wordCount.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String s) throws Exception {
                String[] s1 = s.split(" ");
                return Arrays.asList(s1).iterator();
            }
        });

        // 将map变更为键值对的形式
        JavaPairRDD<String, Integer> pairRDD = flatMap.mapToPair(new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String s) throws Exception {
                return new Tuple2<>(s, 1);
            }
        });

        // 对RDD进行聚合
        JavaPairRDD<String, Integer> pairRDD1 = pairRDD.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer v1, Integer v2) throws Exception {
                return v1 + v2;
            }
        });

        // 结果遍历输出
        pairRDD1.collect().forEach(System.out::println);

        // 4. 关闭sc
        sc.stop();
    }
}
