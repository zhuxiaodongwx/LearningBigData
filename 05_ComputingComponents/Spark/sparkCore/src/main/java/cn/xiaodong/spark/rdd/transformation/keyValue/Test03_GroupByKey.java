package cn.xiaodong.spark.rdd.transformation.keyValue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.Arrays;

/**
 * 统计单词出现频率
 */
public class Test03_GroupByKey {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test03_GroupByKey");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        // 创建单词库
        JavaRDD<String> rdd = sc.parallelize(Arrays.asList("hello", "hello", "spark", "spark", "spark", "spark", "xiaodong"));

        // 将单词库转为键值对的形式
        JavaPairRDD<String, Integer> pairRDD = rdd.mapToPair(new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String s) throws Exception {
                return new Tuple2<>(s, 1);
            }
        });

        // 对RDD按照Key进行分组
        JavaPairRDD<String, Iterable<Integer>> pairRDDGroup = pairRDD.groupByKey();

        // 将RDD values中的词频进行相加
        JavaPairRDD<String, Integer> rddCount = pairRDDGroup.mapValues(new Function<Iterable<Integer>, Integer>() {
            @Override
            public Integer call(Iterable<Integer> v1) throws Exception {
                Integer sum = 0;
                for (Integer i : v1) {
                    sum += i;
                }
                return sum;
            }
        });

        // 遍历统计结果
        rddCount.collect().forEach(System.out::println);

        // 4. 关闭sc
        sc.stop();
    }
}
