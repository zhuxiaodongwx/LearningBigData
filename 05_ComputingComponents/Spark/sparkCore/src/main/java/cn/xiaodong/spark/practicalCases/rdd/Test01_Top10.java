package cn.xiaodong.spark.practicalCases.rdd;

import cn.xiaodong.spark.practicalCases.bean.CategoryCountInfo;
import cn.xiaodong.spark.practicalCases.bean.UserVisitAction;
import cn.xiaodong.spark.practicalCases.util.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 按照每个品类的点击、下单、支付的量来统计热门品类
 */
public class Test01_Top10 {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test01_Top10");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码

        // (1) 根据日志文件获得RDD
        JavaRDD<String> textFileRDD = sc.textFile("input/user_visit_action.txt");

        // (2) 将一行日志RDD转换为UserVisitAction对象RDD
        JavaRDD<UserVisitAction> visitActionJavaRDD = textFileRDD.map(new Function<String, UserVisitAction>() {
            @Override
            public UserVisitAction call(String v1) throws Exception {
                return new UserVisitAction(v1);
            }
        });

        // (3) 将UserVisitAction对象RDD 转换为 CategoryCountInfo对象RDD的键值对形式
        JavaRDD<CategoryCountInfo> infoJavaRDD = visitActionJavaRDD.flatMap(new FlatMapFunction<UserVisitAction, CategoryCountInfo>() {
            @Override
            public Iterator<CategoryCountInfo> call(UserVisitAction userVisitAction) throws Exception {

                //商品类目统计信息
                ArrayList<CategoryCountInfo> categoryCountInfos = new ArrayList<>();


                // 点击的商品品类id
                String clickCategoryId = userVisitAction.getClick_category_id();
                if (StringUtils.isNotNullOrEmpty(clickCategoryId)){
                    categoryCountInfos.add(new CategoryCountInfo(clickCategoryId,1L,0L,0L,0D));
                }

                // 订单中的商品品类id
                String orderCategoryIds = userVisitAction.getOrder_category_ids();
                if (StringUtils.isNotNullOrEmpty(orderCategoryIds)){
                    // 获取单个订单id
                    String[] split = orderCategoryIds.split(",");
                    for (String s : split) {
                        categoryCountInfos.add(new CategoryCountInfo(s,0L,1L,0L,0D));
                    }
                }
                // 支付的商品品类id
                String payCategoryIds = userVisitAction.getPay_category_ids();
                if (StringUtils.isNotNullOrEmpty(payCategoryIds)){
                    // 获取单个订单id
                    String[] split = payCategoryIds.split(",");
                    for (String s : split) {
                        categoryCountInfos.add(new CategoryCountInfo(s,0L,0L,1L,0D));
                    }
                }

                return categoryCountInfos.iterator();
            }
        });


        // 转换为KV形式
        JavaPairRDD<String, CategoryCountInfo> countInfoJavaPairRDD = infoJavaRDD.mapToPair(
                new PairFunction<CategoryCountInfo, String, CategoryCountInfo>() {
            @Override
            public Tuple2<String, CategoryCountInfo> call(CategoryCountInfo categoryCountInfo) throws Exception {
                return new Tuple2<>(categoryCountInfo.getCategory(), categoryCountInfo);
            }
        });

        // (4) 对CategoryCountInfo对象RDD 进行聚合 （聚合点击数、下单数、支付数）
        JavaPairRDD<String, CategoryCountInfo> categoryCountInforReduceByKey = countInfoJavaPairRDD.reduceByKey(
                new Function2<CategoryCountInfo, CategoryCountInfo, CategoryCountInfo>() {
            @Override
            public CategoryCountInfo call(CategoryCountInfo v1, CategoryCountInfo v2) throws Exception {
                return new CategoryCountInfo(v1.getCategory(),
                        v1.getClicks()+v2.getClicks(),
                        v1.getOrders()+v2.getOrders(),
                        v1.getPayments()+v2.getPayments(),
                        v1.getWeights()+v2.getWeights());
            }
        });


        // (5) 对CategoryCountInfo对象RDD 计算综合排名权重
        JavaPairRDD<String, CategoryCountInfo> categoryCountInforMap = categoryCountInforReduceByKey.mapValues(new Function<CategoryCountInfo, CategoryCountInfo>() {
            @Override
            public CategoryCountInfo call(CategoryCountInfo v1) throws Exception {
                v1.refeshWeights();
                return v1;
            }
        });

        JavaRDD<CategoryCountInfo> countInfoJavaRDD = categoryCountInforMap.map(new Function<Tuple2<String, CategoryCountInfo>, CategoryCountInfo>() {
            @Override
            public CategoryCountInfo call(Tuple2<String, CategoryCountInfo> v1) throws Exception {
                return v1._2;
            }
        });

        // (6) 对CategoryCountInfo对象RDD 按照综合排名权重 进行排序（从大到小）
        JavaRDD<CategoryCountInfo> countInfoJavaRDDSort = countInfoJavaRDD.sortBy(new Function<CategoryCountInfo, Double>() {
            @Override
            public Double call(CategoryCountInfo v1) throws Exception {
                return v1.getWeights();
            }
        },false,1);

        // (7) 取出前10个RDD，进行遍历展示
        List<CategoryCountInfo> take = countInfoJavaRDDSort.take(10);
        take.forEach(System.out::println);

        // 4. 关闭sc
        sc.stop();
    }
}
