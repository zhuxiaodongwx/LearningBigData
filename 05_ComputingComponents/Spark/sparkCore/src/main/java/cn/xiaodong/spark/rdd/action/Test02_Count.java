package cn.xiaodong.spark.rdd.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.Arrays;

public class Test02_Count {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test01_Collect");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(Arrays.asList(new Tuple2<>("k1", 1), new Tuple2<>("k2", 2), new Tuple2<>("k3", 3)));

        //RDD个数计算
        long count = pairRDD.count();

        // 遍历输出
        System.out.println("pairRDD元素个数是：" + count);

        // 4. 关闭sc
        sc.stop();
    }
}
