package cn.xiaodong.sparkSQL.function;


import cn.xiaodong.sparkSQL.bean.Buffer;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.expressions.Aggregator;

/***
 * 自定义UDAF函数的计算逻辑实现
 */
public class MyAvg extends Aggregator<Long, Buffer,Double> {

    // 数据初始化
    @Override
    public Buffer zero() {
        return new Buffer(0L,0L);
    }

    // 单行数据合并
    @Override
    public Buffer reduce(Buffer b, Long a) {
        b.setSum(b.getSum() + a);
        b.setCount(b.getCount() + 1);
        return b;
    }

    // 计算结果合并
    @Override
    public Buffer merge(Buffer b1, Buffer b2) {

        b1.setSum(b1.getSum() + b2.getSum());
        b1.setCount(b1.getCount() + b2.getCount());

        return b1;
    }

    // 返回最终计算结果
    @Override
    public Double finish(Buffer reduction) {
        return reduction.getSum().doubleValue() / reduction.getCount();
    }

    @Override
    public Encoder<Buffer> bufferEncoder() {
        // 可以用kryo进行优化
        return Encoders.kryo(Buffer.class);
    }

    @Override
    public Encoder<Double> outputEncoder() {
        return Encoders.DOUBLE();
    }

}
