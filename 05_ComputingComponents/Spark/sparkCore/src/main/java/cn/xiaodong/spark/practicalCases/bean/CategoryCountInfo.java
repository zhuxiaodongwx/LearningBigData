package cn.xiaodong.spark.practicalCases.bean;

import java.io.Serializable;
import java.io.StringReader;

/**
 * 商品类目统计信息
 */
public class CategoryCountInfo implements Serializable {

    /**
     * 产品分类
     */
    private String category;

    /**
     * 点击数
     */
    private Long clicks;

    /**
     * 订单数
     */
    private Long orders;

    /**
     * 支付数
     */
    private Long payments;

    /**
     * 权重
     */
    private Double weights;


    public CategoryCountInfo(String category, Long clicks, Long orders, Long payments, Double weights) {
        this.category = category;
        this.clicks = clicks;
        this.orders = orders;
        this.payments = payments;
        this.weights = weights;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getClicks() {
        return clicks;
    }

    public void setClicks(Long clicks) {
        this.clicks = clicks;
    }

    public Long getOrders() {
        return orders;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }

    public Long getPayments() {
        return payments;
    }

    public void setPayments(Long payments) {
        this.payments = payments;
    }

    public Double getWeights() {
        return weights;
    }

    public void setWeights(Double weights) {
        this.weights = weights;
    }

    /**
     * 重新计算品类的权重
     */
    public void refeshWeights() {
        this.weights = this.clicks * 0.2 + this.orders * 0.3 + this.payments * 0.5;
    }

    @Override
    public String toString() {
        return "CategoryCountInfo{" +
                "category='" + category + '\'' +
                ", clicks=" + clicks +
                ", orders=" + orders +
                ", payments=" + payments +
                ", weights=" + weights +
                '}';
    }
}
