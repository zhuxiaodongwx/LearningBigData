package cn.xiaodong.spark.rdd.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;

public class Test06_Save {
    public static void main(String[] args) {
        // 设置Hadoop连接信息
        System.setProperty("HADOOP_USER_NAME","atguigu");

        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        JavaRDD<Integer> integerJavaRDD = sc.parallelize(Arrays.asList(1, 2, 3, 4),2);

        // 文本保存在本地
        integerJavaRDD.saveAsTextFile("output1");
        // 文本保存到HDFS
        integerJavaRDD.saveAsTextFile("hdfs://hadoop104:8020/output1");
        // 序列化保存到本地
        integerJavaRDD.saveAsObjectFile("output2");


        // 4. 关闭sc
        sc.stop();

    }
}
