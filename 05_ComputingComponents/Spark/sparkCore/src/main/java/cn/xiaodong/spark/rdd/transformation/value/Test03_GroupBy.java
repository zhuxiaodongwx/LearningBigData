package cn.xiaodong.spark.rdd.transformation.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import java.util.Arrays;

/**
 * RDD GroupBy
 * <p>
 * 根据函数返回值，进行分组
 */
public class Test03_GroupBy {
    public static void main(String[] args) throws InterruptedException {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test03_GroupBy");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码

        // 根据集合创建RDD
        JavaRDD<Integer> rdd = sc.parallelize(Arrays.asList(1, 2, 3, 4, 5, 6), 1);

        // RDD元素根据是否能被2整除分组
        JavaPairRDD<Integer, Iterable<Integer>> newRdd = rdd.groupBy(new Function<Integer, Integer>() {
            @Override
            public Integer call(Integer integer) throws Exception {
                return integer % 2;
            }
        });

        // 遍历打印新的RDD
        newRdd.collect().forEach(System.out::println);

        // 第二种写法
//        JavaPairRDD<String, Iterable<Integer>> newRdd2 = rdd.groupBy(new Function<Integer, String>() {
//            @Override
//            public String call(Integer integer) throws Exception {
//                return integer % 2 == 0 ? "偶数" : "奇数";
//            }
//        });
//        newRdd2.collect().forEach(System.out::println);


        // 睡眠进程，看spark web后台  http://localhost:4040/
        Thread.sleep(600000);

        // 4. 关闭sc
        sc.stop();
        }
    }
