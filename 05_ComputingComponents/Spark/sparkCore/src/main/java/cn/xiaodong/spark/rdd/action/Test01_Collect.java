package cn.xiaodong.spark.rdd.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.util.Arrays;

public class Test01_Collect {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test01_Collect");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(Arrays.asList(new Tuple2<>("k1", 1), new Tuple2<>("k2", 2), new Tuple2<>("k3", 3)));

        // 对原有RDD的值加1
        JavaPairRDD<String, Integer> mapValuesRDD = pairRDD.mapValues(new Function<Integer, Integer>() {
            @Override
            public Integer call(Integer v1) throws Exception {
                System.out.println("数据计算中.....");
                return v1 + 1;
            }
        });

        // 遍历输出
        System.out.println("遍历结果输出：");
        mapValuesRDD.collect().forEach(System.out::println);

        // 4. 关闭sc
        sc.stop();
    }
}
