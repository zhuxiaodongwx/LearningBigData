| 包名                    | 内容               | 备注                                   |
| ----------------------- | ------------------ | -------------------------------------- |
| spark.rdd               | RDD编程            |                                        |
| spark.accumulator       | 累加器             |                                        |
| spark.broadcast         | 广播变量           |                                        |
| spark.practicalCases    | 实战案例           | 根据用户行为日志，计算品类top10        |
|                         |                    |                                        |
| sparkSQL.bean           | javaBean           |                                        |
| sparkSQL.sql            | sparkSQL的调用     |                                        |
| sparkSQL.function       | 自定义函数         |                                        |
| sparkSQL.readWrite      | 文件读取与保存     |                                        |
| sparkSQL.mysql          | SparkSQL连接数据库 |                                        |
| sparkSQL.hive           | SparkSQL操作Hive表 |                                        |
| sparkSQL.practicalCases | 实战案例           | 根据行为日志（hive表）计算商品热度top3 |

| 包名                              | 内容                                   | 备注 |
| --------------------------------- | -------------------------------------- | ---- |
| spark.rdd.create                  | RDD的创建                              |      |
| spark.rdd.partition               | RDD的分区                              |      |
| spark.rdd.transformation.value    | Transformation转换算子   Value类型     |      |
| spark.rdd.transformation.keyValue | Transformation转换算子   Key-Value类型 |      |
| spark.rdd.action                  | Action 行动算子                        |      |
| spark.rdd.wordCount               | wordCount案例                          |      |
| spark.rdd.serializable            | RDD序列化                              |      |
| spark.rdd.dependency              | RDD血缘依赖                            |      |
| spark.rdd.cache                   | RDD持久化                              |      |
| spark.rdd.partitioner             | 键值RDD分区                            |      |
