package cn.xiaodong.sparkSQL.bean;

import java.io.Serializable;

/**
 * 自定义UDAF函数的计算中间缓存对象
 */
public class Buffer implements Serializable {

    /**
     * 求和
     */
    private Long sum;

    /**
     * 计数
     */
    private Long count;

    public Buffer() {
    }

    public Buffer(Long sum, Long count) {
        this.sum = sum;
        this.count = count;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
