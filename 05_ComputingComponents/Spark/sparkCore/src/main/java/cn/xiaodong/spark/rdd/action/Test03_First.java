package cn.xiaodong.spark.rdd.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.Arrays;

/**
 * 获取RDD第一个元素
 */
public class Test03_First {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test03_First");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(Arrays.asList(new Tuple2<>("k1", 1), new Tuple2<>("k2", 2), new Tuple2<>("k3", 3)));


        // 获取RDD第一个元素
        Tuple2<String, Integer> first = pairRDD.first();

        // 遍历输出
        System.out.println(first.toString());

        // 4. 关闭sc
        sc.stop();
    }
}
