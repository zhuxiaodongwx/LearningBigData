package cn.xiaodong.spark.rdd.serializable;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import java.util.Arrays;

/**
 * Java序列化
 */
public class Test01_JavaSerializable {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test01_JavaSerializable");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        JavaRDD<User> rdd = sc.parallelize(
                Arrays.asList(new User("xiaodong", 18), new User("xiaohong", 14)));

        // 修改没人的年龄+1
        JavaRDD<User> mapped = rdd.map(new Function<User, User>() {
            @Override
            public User call(User v1) throws Exception {
                return new User(v1.getName(), v1.getAge() + 1);
            }
        });

        // 输出结果
        mapped.collect().forEach( user -> System.out.println(user.toString()));


        // 4. 关闭sc
        sc.stop();
    }
}
