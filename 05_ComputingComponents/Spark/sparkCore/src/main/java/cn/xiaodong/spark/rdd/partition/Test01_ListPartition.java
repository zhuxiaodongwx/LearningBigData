package cn.xiaodong.spark.rdd.partition;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;

/**
 * 集合RDD分区规则
 */
public class Test01_ListPartition {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test01_ListPartition");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        // 默认环境的核数
        // 可以手动填写参数控制分区的个数
        JavaRDD<String> stringRDD = sc.parallelize(Arrays.asList("hello1", "spark2", "hello3", "spark4", "hello5"), 2);

        // 数据分区的情况
        // 0 => 1,2  1 => 3,4,5
        // 利用整数除机制  左闭右开
        // 0 => start 0*5/2  end 1*5/2
        // 1 => start 1*5/2  end 2*5/2
        stringRDD.saveAsTextFile("output1");


        // 4. 关闭sc
        sc.stop();
    }
}
