package cn.xiaodong.sparkSQL.function;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF2;
import org.apache.spark.sql.expressions.UserDefinedFunction;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.udf;

public class Test04_UDF {

    public static void main(String[] args) {
        //1. 创建配置对象
        SparkConf conf = new SparkConf().setAppName("sparksql").setMaster("local[*]");

        //2. 获取sparkSession
        SparkSession spark = SparkSession.builder().config(conf).getOrCreate();

        //3. 编写代码
        Dataset<Row> lineRDD = spark.read().json("input/user.json");

        lineRDD.createOrReplaceTempView("user");

        // 定义一个函数
        // 需要首先导入依赖import static org.apache.spark.sql.functions.udf;
        UserDefinedFunction addName = udf(new UDF2<String, String, String>() {
            @Override
            public String call(String s, String s2) throws Exception {
                return s + s2;
            }
        }, DataTypes.StringType);

        spark.udf().register("addName",addName);

        spark.sql("select addName(name,'_ssr') newName from user")
                .show();

        // lambda表达式写法
//        spark.udf().register("addName1",(UDF1<String,String>) name -> name + " 大侠", DataTypes.StringType);


        //4. 关闭sparkSession
        spark.close();

    }
}
