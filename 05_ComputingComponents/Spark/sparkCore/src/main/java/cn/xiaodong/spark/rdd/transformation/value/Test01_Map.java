package cn.xiaodong.spark.rdd.transformation.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import java.util.List;

/**
 * RDD map映射
 *
 * 遍历RDD中的每一个元素，进行操作
 */
public class Test01_Map {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test01_Map");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        JavaRDD<String> lineRDD = sc.textFile("input/1.txt");

        // 需求:每行结尾拼接||

        // 写法1：lambda表达式写法(匿名函数)
//        JavaRDD<String> mapRDD = lineRDD.map(s -> s + "||");

        // 写法2：匿名函数写法
        JavaRDD<String> mapRDD = lineRDD.map(new Function<String, String>() {
            @Override
            public String call(String v1) throws Exception {
                return v1 + "||";
            }
        });

        // RDD收集、打印
        // 写法1：
//        List<String> mapRDDcollect = mapRDD.collect();
//        for (String s : mapRDDcollect) {
//            System.out.println(s);
//        }

        // 写法2：输出数据的函数写法
//        mapRDD.collect().forEach(a -> System.out.println(a));
        // 写法3：
        mapRDD.collect().forEach(System.out::println);

        // 4. 关闭sc
        sc.stop();
    }
}
