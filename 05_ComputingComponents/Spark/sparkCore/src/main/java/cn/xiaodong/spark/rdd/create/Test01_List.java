package cn.xiaodong.spark.rdd.create;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;
import java.util.List;

/**
 * 集合创建RDD
 */
public class Test01_List {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test01_List");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        JavaRDD<String> stringRDD = sc.parallelize(Arrays.asList("hello", "spark"));

        List<String> result = stringRDD.collect();

        for (String s : result) {
            System.out.println(s);
        }

        // 4. 关闭sc
        sc.stop();
    }
}
