package cn.xiaodong.spark.rdd.transformation.keyValue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.Arrays;

public class Test05_SortByKey {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test05_SortByKey");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(Arrays.asList(new Tuple2<>("k1", 1), new Tuple2<>("k2", 2), new Tuple2<>("k3", 3)));

        // 排序
        JavaPairRDD<String, Integer> pairRDDSortByKey = pairRDD.sortByKey();

        // 输出结果
        pairRDDSortByKey.collect().forEach(System.out::println);

        // 4. 关闭sc
        sc.stop();
    }
}
