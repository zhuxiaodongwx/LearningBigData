package cn.xiaodong.spark.practicalCases.util;

public class StringUtils {

    /**
     * 判断字符串为空
     *
     * @param str
     * @return
     */
    public static boolean isNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty() || str.equals("null") || str.equals("-1");
    }

    /**
     * 判断字符串非空
     *
     * @param str
     * @return
     */
    public static boolean isNotNullOrEmpty(String str) {
        return !isNullOrEmpty(str);
    }


}
