package cn.xiaodong.spark.rdd.transformation.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import java.util.Arrays;

/**
 * RDD filter过滤
 *
 * 根据函数的返回值决定是否移除元素
 */
public class Test04_Filte {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test04_Filte");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码

        // 根据集合创建RDD
        JavaRDD<Integer> rdd = sc.parallelize(Arrays.asList(1, 2, 3, 4, 5), 1);

        // 过滤RDD，保留对2取余等于0的数据
        JavaRDD<Integer> newRdd = rdd.filter(new Function<Integer, Boolean>() {
            @Override
            public Boolean call(Integer integer) throws Exception {
                return integer % 2 == 0;
            }
        });

        // 遍历新的RDD
        newRdd.collect().forEach(System.out::println);

        // 4. 关闭sc
        sc.stop();
    }
}
