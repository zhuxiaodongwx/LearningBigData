package cn.xiaodong.sparkSQL.bean;

/**
 * 用户
 */
public class User {

    /**
     * 年龄
     */
    public Long age;

    /**
     * 姓名
     */
    public String name;

    public User() {
    }

    public User(Long age, String name) {
        this.age = age;
        this.name = name;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
