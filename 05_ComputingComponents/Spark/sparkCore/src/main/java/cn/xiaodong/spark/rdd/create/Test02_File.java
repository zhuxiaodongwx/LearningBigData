package cn.xiaodong.spark.rdd.create;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.List;

/**
 * 文件创建RDD
 */
public class Test02_File {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test02_File");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        // 根据本地 文本文件创建RDD
        JavaRDD<String> lineRDD1 = sc.textFile("input");
        //从HDFS 文本文件创建RDD
        JavaRDD<String> lineRDD2 = sc.textFile("hdfs://hadoop104:8020/input");

        List<String> result = lineRDD1.collect();

        for (String s : result) {
            System.out.println(s);
        }

        // 4. 关闭sc
        sc.stop();
    }
}
