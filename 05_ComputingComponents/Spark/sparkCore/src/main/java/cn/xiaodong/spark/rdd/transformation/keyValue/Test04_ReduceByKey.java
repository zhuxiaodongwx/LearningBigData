package cn.xiaodong.spark.rdd.transformation.keyValue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.Arrays;

/**
 * 统计单词出现频率
 *
 * ReduceByKey 自动聚合
 */
public class Test04_ReduceByKey {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test03_GroupByKey");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        // 创建单词库
        JavaRDD<String> rdd = sc.parallelize(Arrays.asList("hello", "hello", "spark", "spark", "spark", "spark", "xiaodong"));

        // 将单词库转为键值对的形式
        JavaPairRDD<String, Integer> pairRDD = rdd.mapToPair(new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String s) throws Exception {
                return new Tuple2<>(s, 1);
            }
        });

        // 对RDD按照Key进行分组
        JavaPairRDD<String, Integer> pairRDDReduce = pairRDD.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer v1, Integer v2) throws Exception {
                return v1+v2;
            }
        });

        // 遍历结果
        pairRDDReduce.collect().forEach(System.out::println);

        // 4. 关闭sc
        sc.stop();
    }

}
