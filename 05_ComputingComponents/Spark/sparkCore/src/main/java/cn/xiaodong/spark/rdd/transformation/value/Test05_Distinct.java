package cn.xiaodong.spark.rdd.transformation.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;

/**
 * RDD去重 distinct
 */
public class Test05_Distinct {
    public static void main(String[] args) throws InterruptedException {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test05_Distinct");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码

        // 创建RDD
        JavaRDD<Integer> rdd = sc.parallelize(Arrays.asList(1, 1, 2, 2, 3, 4, 5), 1);

        // RDD过滤
        JavaRDD<Integer> newRdd = rdd.distinct();

        // 遍历新的RDD
        newRdd.collect().forEach(System.out::println);


        // 睡眠进程，看spark web后台  http://localhost:4040/
        Thread.sleep(600000);

        // 4. 关闭sc
        sc.stop();
    }
}
