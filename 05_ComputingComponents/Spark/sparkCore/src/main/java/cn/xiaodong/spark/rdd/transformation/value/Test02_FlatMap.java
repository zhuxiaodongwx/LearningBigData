package cn.xiaodong.spark.rdd.transformation.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * RDD FlatMap扁平化
 * <p>
 * 将RDD中个每个元素，拆分成新的多个元素
 */
public class Test02_FlatMap {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test02_FlatMap");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        ArrayList<List<String>> arrayLists = new ArrayList<>();

        // ============================  案例一  ====================================

        System.out.println("// ============================  案例一  ====================================");

        // 创建一个包含两个集合的RDD
        arrayLists.add(Arrays.asList("1", "2", "3"));
        arrayLists.add(Arrays.asList("4", "5", "6"));

        JavaRDD<List<String>> listJavaRDD = sc.parallelize(arrayLists, 2);

        // 将RDD进行扁平化
        JavaRDD<String> stringJavaRDD = listJavaRDD.flatMap(new FlatMapFunction<List<String>, String>() {
            @Override
            // List<String> strings    List<String>为原RDD单个元素的类型
            // Iterator<String>        String为新RDD元素类型
            public Iterator<String> call(List<String> strings) throws Exception {
                return strings.iterator();
            }
        });

        // 遍历新的RDD
        stringJavaRDD.collect().forEach(System.out::println);

        // ============================  案例二  ====================================
        System.out.println("// ============================  案例二  ====================================");

        // 根据文件创建RDD
        JavaRDD<String> lineRDD = sc.textFile("input/2.txt");

        // 将RDD扁平化，生成新的RDD
        JavaRDD<String> stringJavaRDD1 = lineRDD.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String s) throws Exception {
                // 将原有RDD中的每个元素，按照空格进行切分
                String[] s1 = s.split(" ");
                return Arrays.asList(s1).iterator();
            }
        });

        // 遍历打印新的RDD
        stringJavaRDD1.collect().forEach(System.out::println);

        // 4. 关闭sc
        sc.stop();
    }
}
