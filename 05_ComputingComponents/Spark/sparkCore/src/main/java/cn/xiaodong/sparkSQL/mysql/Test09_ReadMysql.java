package cn.xiaodong.sparkSQL.mysql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Properties;

/**
 * 读取MySQL数据的三种方法
 */
public class Test09_ReadMysql {
    public static void main(String[] args) {

        // 获取连接
        SparkSession sparkSession = SparkSession.builder().master("local[4]").appName("test").getOrCreate();


        //mysql 的url
        String url = "jdbc:mysql://hadoop102:3306/gmall";
        //读取的表名
        //读取整表所有数据
        String tableName = "user_info";
        //以子查询的方式只读取需要的数据
        //String tableName = "(select * from user_info where id>=500) user";

        //读取mysql的参数配置
        Properties props = new Properties();
        props.setProperty("user","root");
        props.setProperty("password","root123");

        //==================读取mysql第一种方式==============================
        //此种方式读取mysql生成的DataSet只有一个分区,只适合用于表数据量很小的场景
        Dataset<Row> ds = sparkSession.read().jdbc(url, tableName, props);
        System.out.println("第一种读取方式的数据分区个数是");
        System.out.println(ds.toJavaRDD().getNumPartitions());

        //==================读取mysql第二种方式[一般不用]==================
        //此种方式读取mysql生成的dataset的分区数 = conditions数组的长度
        //conditions数组中的每个元素代表每个分区拉取数据的where条件。
        //分区数太多的时候,写where条件会很麻烦
        String[] conditions = { "id<=100","id>100 and id<=400","id>400 and id<=800","id>800" };
        sparkSession.read().jdbc(url,tableName,conditions,props).write().json("output/json");


        //==================读取mysql的第三种方式[大数据量场景常用]==================

        //动态获取lowerBound与upperBound的值
        Dataset<Row> ds4 = sparkSession.read().jdbc(url, "(SELECT MIN(id) min_id,MAX(id) max_id FROM user_info) user_info", props);
        // 获取第一条数据
        Row row = ds4.first();
        //ROW类型取值: row.getAs("列名")
        Long min_id = (Long)row.getAs("min_id");
        Long max_id = (Long)row.getAs("max_id");

        //此种方式读取mysql生成的dataset的分区数 = (upperBound-lowerBound)>=numPartitions?  numPartitions : upperBound-lowerBound
        //后续用于分区的where条件的mysql列名,必须是数字、日期、时间戳类型[推荐用主键]
        String columnName = "id";
        //用于决定分区跨距的columnName字段的最小值[推荐使用columnName列的最小值]
        Long lowerBound = min_id;
        //用于决定分区跨距的columnName字段的最大值[推荐使用columnName列的最大值]
        Long upperBound = max_id;
//        System.out.println(min_id);
//        System.out.println(max_id);
        Dataset<Row> ds3 = sparkSession.read().jdbc(url, tableName, columnName, lowerBound, upperBound, 2000, props);
        System.out.println("第三种读取方式的数据分区个数是");
        System.out.println(ds3.toJavaRDD().getNumPartitions());


        // 关闭连接
        sparkSession.close();
    }
}
