package cn.xiaodong.spark.rdd.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;

/**
 * 获取RDD指定的前几个元素
 */
public class Test04_Take {
    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test04_Take");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(Arrays.asList(new Tuple2<>("k1", 1), new Tuple2<>("k2", 2), new Tuple2<>("k3", 3)));


        // 获取RDD的前两个元素
        List<Tuple2<String, Integer>> take = pairRDD.take(2);

        // 遍历输出
        take.forEach(sot -> System.out.println(sot.toString()));

        // 4. 关闭sc
        sc.stop();
    }
}
