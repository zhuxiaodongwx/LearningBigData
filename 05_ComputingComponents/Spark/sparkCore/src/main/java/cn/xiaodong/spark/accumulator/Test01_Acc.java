package cn.xiaodong.spark.accumulator;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.util.LongAccumulator;
import scala.Tuple2;



import java.util.Arrays;

/**
 * 累加器
 */
public class Test01_Acc {

    public static void main(String[] args) {
        // 1.创建配置对象
        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("sparkCore");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        JavaPairRDD<String, Integer> tupleRDD = sc.parallelizePairs(
                Arrays.asList(
                        new Tuple2<>("a", 1), new Tuple2<>("a", 2), new Tuple2<>("a", 3), new Tuple2<>("a", 1)));

        // 使用变量求和
        final int[] i = {0};

        tupleRDD.foreach(new VoidFunction<Tuple2<String, Integer>>() {
            @Override
            public void call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
                i[0] += stringIntegerTuple2._2;
            }
        });

        System.out.println("i[0]="+i[0]);


        // 转换为scala的累加器
        LongAccumulator longAccumulator = JavaSparkContext.toSparkContext(sc).longAccumulator();

        // 在foreach中使用累加器
        tupleRDD.foreach(new VoidFunction<Tuple2<String, Integer>>() {
            @Override
            public void call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
                longAccumulator.add(stringIntegerTuple2._2);
            }
        });

        System.out.println("longAccumulator="+longAccumulator.value());


        // 4. 关闭sc
        sc.stop();

    }

}
