package cn.xiaodong.spark.rdd.serializable;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import java.util.Arrays;

/**
 * Kryo序列化
 */
public class Test02_KryoSerializable {
    public static void main(String[] args) throws ClassNotFoundException {
        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test01_JavaSerializable")
                // 替换默认的序列化机制
                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
                // 注册需要使用kryo序列化的自定义类
                .registerKryoClasses(new Class[]{Class.forName("cn.xiaodong.spark.rdd.serializable.User2")});


        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        JavaRDD<User2> rdd = sc.parallelize(
                Arrays.asList(new User2("xiaodong", 18), new User2("xiaohong", 14)));

        // 修改没人的年龄+1
        JavaRDD<User2> mapped = rdd.map(new Function<User2, User2>() {
            @Override
            public User2 call(User2 v1) throws Exception {
                return new User2(v1.getName(), v1.getAge() + 1);
            }
        });

        // 输出结果
        mapped.collect().forEach( user -> System.out.println(user.toString()));


        // 4. 关闭sc
        sc.stop();
    }
}
