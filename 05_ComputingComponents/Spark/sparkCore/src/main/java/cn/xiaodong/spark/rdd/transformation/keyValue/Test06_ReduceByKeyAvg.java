package cn.xiaodong.spark.rdd.transformation.keyValue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;

import java.util.Arrays;

/**
 * 求平均数
 *
 * 先进行数据聚合，在计算
 */
public class Test06_ReduceByKeyAvg {
    public static void main(String[] args) {

        // 1.创建配置对象
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("Test03_GroupByKey");

        // 2. 创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 3. 编写代码
        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(
                Arrays.asList(
                        new Tuple2<>("张三", 88), new Tuple2<>("张三", 55), new Tuple2<>("张三", 66),
                        new Tuple2<>("李四", 25), new Tuple2<>("李四", 99), new Tuple2<>("李四", 77),
                        new Tuple2<>("王五", 43), new Tuple2<>("王五", 88), new Tuple2<>("王五", 98)));

        // 数据类型转换  "姓名", (成绩,1)
        JavaPairRDD<String, Tuple2<Integer, Integer>> pairRDDMap = pairRDD.mapValues(new Function<Integer, Tuple2<Integer, Integer>>() {
            @Override
            public Tuple2<Integer, Integer> call(Integer v1) throws Exception {
                return new Tuple2<>(v1, 1);
            }
        });

        // 数据聚合 成绩、计数分别相加
        // 如果此处不使用reduceByKey，使用groupByKey，则分组完成后，要mapValues再计算一次，才能获取总成绩和总次数
        JavaPairRDD<String, Tuple2<Integer, Integer>> pairRDDReduce = pairRDDMap.reduceByKey(new Function2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>() {
            @Override
            public Tuple2<Integer, Integer> call(Tuple2<Integer, Integer> v1, Tuple2<Integer, Integer> v2) throws Exception {
                return new Tuple2<>(v1._1 + v2._1, v1._2 + v2._2);
            }
        });

        // 计算平均数 成绩/计数
        JavaPairRDD<String, Double> mapValues = pairRDDReduce.mapValues(new Function<Tuple2<Integer, Integer>, Double>() {
            @Override
            public Double call(Tuple2<Integer, Integer> v1) throws Exception {
                return (double) v1._1 / v1._2;
            }
        });

        // 输出结果
        mapValues.collect().forEach(System.out::println);


        // 4. 关闭sc
        sc.stop();
    }
}
