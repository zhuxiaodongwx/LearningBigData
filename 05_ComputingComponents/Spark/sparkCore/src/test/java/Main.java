import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * HashMap<String, Long> records按照Long值，从大到小排序，并取出前两个
 */
public class Main {
    public static void main(String[] args) {
        // 创建一个示例的HashMap
        Map<String, Long> records = new HashMap<>();
        records.put("A", 100L);
        records.put("B", 50L);
        records.put("C", 200L);
        records.put("D", 75L);

        // 将HashMap中的Entry对象存储到List中
        List<Map.Entry<String, Long>> entryList = new ArrayList<>(records.entrySet());

        // 使用Comparator对List中的Entry对象按Long值从大到小排序
        Collections.sort(entryList, new Comparator<Map.Entry<String, Long>>() {
            @Override
            public int compare(Entry<String, Long> entry1, Entry<String, Long> entry2) {
                return entry2.getValue().compareTo(entry1.getValue());
            }
        });

        // 取出前两个记录
        for (int i = 0; i < Math.min(entryList.size(), 2); i++) {
            Map.Entry<String, Long> entry = entryList.get(i);
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}
