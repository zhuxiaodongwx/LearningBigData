package cn.xiaodong.hdfs.tools;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;
import java.net.URI;


/**
 * 上传文件夹到HDFS
 */
public class CopyLocalFolderToHDFS {

    /**
     * 遍历文件夹
     * @param localFolder 本地文件夹
     * @param hdfsFolder HDFS上传目录
     * @param fileSystem HDFS操作对象
     * @throws IOException
     */
    public static void copyFiles(File localFolder, Path hdfsFolder, FileSystem fileSystem) throws IOException {
        if (localFolder.isDirectory()) {
            File[] files = localFolder.listFiles();
            if (files != null) {
                for (File file : files) {
                    copyFiles(file, new Path(hdfsFolder, file.getName()), fileSystem);
                }
            }
        } else {
            fileSystem.copyFromLocalFile(new Path(localFolder.getAbsolutePath()), hdfsFolder);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        // Hadoop连接地址
        String hadoopURI = "hdfs://hadoop103:8020";
        // 连接用户名
        String userName = "atguigu";

        // 本地文件夹路径
        String localFolderPath = "D:\\Cache\\HDFSFile";
        // HDFS目录地址
        String hdfsFolderPath = "/test";

        // 1. 新建HDFS对象
        Configuration hdfsConfiguration = new Configuration();
        FileSystem fileSystem = FileSystem.get(URI.create(hadoopURI), hdfsConfiguration, userName);

        // 2. 操作集群
        copyFiles(new File(localFolderPath), new Path(hdfsFolderPath), fileSystem);

        // 3. 关闭资源
        fileSystem.close();
    }
}
