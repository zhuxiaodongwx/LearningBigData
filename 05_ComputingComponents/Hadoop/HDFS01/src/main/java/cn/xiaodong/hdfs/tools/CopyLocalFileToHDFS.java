package cn.xiaodong.hdfs.tools;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;


/**
 * 上传单个文件到HDFS
 */
public class CopyLocalFileToHDFS {

    /**
     * 测试文件上传
     */
    public static void testCopyFromLocal() throws IOException, InterruptedException {

        // Hadoop连接地址
        String hadoopURI = "hdfs://hadoop103:8020";
        // 连接用户名
        String userName = "atguigu";

        // 文件路径
        String filePath = "D:\\Cache\\HDFSFile";
        // 文件名称
        String fileName = "log.txt";
        // HDFS目录地址
        String HDFSPath = "/test";

        // 1.新建HDFS对象
        Configuration hdfsConfiguration = new Configuration();
        FileSystem fileSystem = FileSystem.get(URI.create(hadoopURI), hdfsConfiguration, userName);


        // 2、操作集群
        fileSystem.copyFromLocalFile(new Path(filePath+"//"+fileName), new Path(HDFSPath+"/"+fileName));

        // 3、关闭资源
        fileSystem.close();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        new CopyLocalFileToHDFS().testCopyFromLocal();
    }
}
