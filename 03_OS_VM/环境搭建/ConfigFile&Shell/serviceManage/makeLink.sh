#!/bin/bash

# 脚本说明：为/opt/module/serviceManage目录下的三个脚本在/bin目录创建符号链接。

# 遍历 /opt/module/serviceManage 目录中的三个脚本
for script in jpsall.sh xcall.sh xsync.sh
do
    # 检查文件是否存在
    if [ -f "/opt/module/serviceManage/$script" ]; then
        # 创建符号链接到/bin目录，并删除.sh后缀
        ln -s /opt/module/serviceManage/$script /bin/$(basename $script .sh)
    else
        echo "File $script does not exist!"
    fi
done

echo "Symbolic links created in /bin directory."
