#!/bin/bash

# 启动Hadoop集群(HDFS+Yarn+Zookeeper)

# 前提：
# 1、集群之间需要配置ssh免密登录
# 2、网络名称已经修改，并且配置好host解析


# 获取当前用户名
current_user=$(whoami)

# 或者你也可以使用：
# current_user=$USER

# 检查当前用户是否是 'atguigu'
if [ "$current_user" != "atguigu" ]; then
  echo "错误: 当前用户不是 'atguigu'，脚本结束执行"
  exit 1
fi

echo "==========     启动HadoopHA集群     =========="
ssh hadoop102 "/opt/module/hadoop/sbin/start-all.sh"

echo "==========     启动timeLineServer     =========="
ssh hadoop103 "yarn --daemon start timelineserver"

echo "==========     启动historyServer     =========="
ssh hadoop102 "mapred --daemon start historyserver"

# 切换namenode
ssh hadoop102 "/opt/module/serviceManage/switchNamenode.sh"