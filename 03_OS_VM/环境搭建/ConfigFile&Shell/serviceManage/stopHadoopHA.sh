#!/bin/bash

# 关闭Hadoop集群(HDFS+Yarn+Zookeeper)

# 前提：
# 1、集群之间需要配置ssh免密登录
# 2、网络名称已经修改，并且配置好host解析


# 获取当前用户名
current_user=$(whoami)

# 或者你也可以使用：
# current_user=$USER

# 检查当前用户是否是 'atguigu'
if [ "$current_user" != "atguigu" ]; then
  echo "错误: 当前用户不是 'atguigu'，脚本结束执行"
  exit 1
fi



echo "==========     停止HadoopHA集群     =========="
ssh hadoop102 "/opt/module/hadoop/sbin/stop-all.sh"

echo "==========     停止timeLineServer     =========="
ssh hadoop103 "yarn --daemon stop timelineserver"

echo "==========     停止historyServer     =========="
ssh hadoop102 "mapred --daemon stop historyserver"
