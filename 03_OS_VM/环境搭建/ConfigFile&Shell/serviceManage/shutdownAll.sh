#!/bin/bash

# 集群全部关机

# 前提：
# 1、集群之间需要配置ssh免密登录
# 2、网络名称已经修改，并且配置好host解析

 ssh hadoop104 "sudo shutdown now"
 ssh hadoop103 "sudo shutdown now"
 ssh hadoop102 "sudo shutdown now"