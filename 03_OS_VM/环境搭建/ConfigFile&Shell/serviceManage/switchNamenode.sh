#!/bin/bash

# 启动Hadoop集群(HDFS+Yarn+Zookeeper)

# 前提：
# 1、集群之间需要配置ssh免密登录
# 2、网络名称已经修改，并且配置好host解析

echo "==========     NameNode切换中     =========="
# 获取nn1和nn2的状态
nn1_status=$(hdfs haadmin -getServiceState nn1)
nn2_status=$(hdfs haadmin -getServiceState nn2)

# 如果nn1是active，并且nn2是standby，执行切换到nn2操作
if [ "$nn1_status" == "active" ] && [ "$nn2_status" == "standby" ]; then
    echo "NameNode nn1处于活动状态，启动切换到nn2操作"
    hdfs haadmin -failover nn1 nn2
    if [ $? -eq 0 ]; then
        echo "NameNode成功切换到nn2"
    else
        echo "NameNode切换到nn2失败"
    fi
fi

if [ "$nn1_status" == "standby" ] && [ "$nn2_status" == "active" ]; then
    echo "NameNode nn2处于活动状态，无需操作"
fi

if [ "$nn1_status" == "standby" ] && [ "$nn2_status" == "standby" ]; then
    echo "【错误】：两个NameNode均处于standby，请检查"
fi
