#!/bin/bash

# 设置HIVE日志目录路径
HIVE_LOG_DIR=$HIVE_HOME/logs

# 如果HIVE日志目录不存在，则创建该目录
mkdir -p $HIVE_LOG_DIR

# 定义一个函数来检查进程是否正常运行
# 参数1：进程名
function check_process()
{
    # 使用 jps -ml命令查找服务名，并提取PID
    pid=$(jps -ml | grep "$1" | awk '{print $1}')

    # 如果找到的PID存在，则返回PID，否则返回0
    [ -n "$pid" ] && echo "$pid" || echo ""
}

# 定义一个函数来启动HIVE
function hive_start()
{
    # 检查HiveMetaStore进程是否正在运行
    metapid=$(check_process HiveMetaStore )

    # 如果HiveMetaStore进程没有运行，启动它
    cmd="nohup hive --service metastore >$HIVE_LOG_DIR/metastore.log 2>&1 &"
    cmd=$cmd" sleep 4; hdfs dfsadmin -safemode wait >/dev/null 2>&1"
    [ -z "$metapid" ] && eval $cmd || echo "HiveMetaStore服务已启动"

    # 检查HiveServer2进程是否正在运行
    server2pid=$(check_process HiveServer2 )
    # 如果HiveServer2进程没有运行，启动它
    cmd="nohup hive --service hiveserver2 >$HIVE_LOG_DIR/hiveServer2.log 2>&1 &"
    [ -z "$server2pid" ] && eval $cmd || echo "HiveServer2服务已启动"

    echo "Hive启动。操作完成！"
}

# 定义一个函数来停止HIVE
function hive_stop()
{
    # 检查HiveMetaStore进程是否正在运行，如果是，则杀死它
    metapid=$(check_process HiveMetaStore )
    [ -n "$metapid" ] && kill $metapid || echo "HiveMetaStore服务未启动"

    # 检查HiveServer2进程是否正在运行，如果是，则杀死它
    server2pid=$(check_process HiveServer2 )
    [ -n "$server2pid" ] && kill $server2pid || echo "HiveServer2服务未启动"

    echo "Hive关闭，操作完成！"
}

# 根据命令行参数决定执行的操作
case $1 in
"start")
    hive_start
    ;;
"stop")
    hive_stop
    ;;
"restart")
    hive_stop
    sleep 2
    hive_start
    ;;
"status")
    metapid=$(check_process HiveMetaStore )
    server2pid=$(check_process HiveServer2 )

    [ -n "$metapid" ] && echo "运行正常--HiveMetaStore服务" || echo "服务停止--HiveMetaStore服务"
    [ -n "$server2pid" ] && echo "运行正常--HiveServer2服务" || echo "服务停止--HiveServer2服务"
    ;;
*)
    # 如果提供了无效的参数，则显示错误消息和使用方法
    echo "参数不正确!"
    echo 'Usage: '$(basename $0)' start|stop|restart|status'
    ;;
esac