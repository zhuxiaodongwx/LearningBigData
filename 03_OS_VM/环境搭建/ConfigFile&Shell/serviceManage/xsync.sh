#!/bin/bash

# 该脚本用于集群之间的文件同步
# 前提：
# 1、集群之间需要配置ssh免密登录
# 2、网络名称已经修改，并且配置好host解析

# 1. 判断参数个数
# 检查传递给脚本的参数数量。如果没有提供参数，脚本将退出并显示错误消息。
if [ $# -lt 1 ]
then
  echo "Not Enough Arguement!"
  exit;
fi

# 2. 遍历集群所有机器
# 对于在集群中的每台机器（hadoop102、hadoop103、hadoop104），执行以下操作
for host in hadoop102 hadoop103 hadoop104
do
  echo "====================  $host  ===================="

  # 3. 遍历所有目录，挨个发送
  # 遍历传递给脚本的所有文件或目录
  for file in $@
  do
    # 4. 判断文件是否存在
    # 检查文件或目录是否在当前机器上存在
    if [ -e $file ]
    then
      # 5. 获取父目录
      # 使用`dirname`命令获取文件或目录的父目录，然后使用`pwd`获取其完整路径
      pdir=$(cd -P $(dirname $file); pwd)

      # 6. 获取当前文件的名称
      # 使用`basename`命令获取文件或目录的基本名称
      fname=$(basename $file)

      # 使用ssh远程命令在目标机器上创建相应的目录结构
      ssh $host "mkdir -p $pdir"

      # 使用rsync命令将文件或目录同步到目标机器的相应位置
      rsync -av $pdir/$fname $host:$pdir
    else
      # 如果文件或目录在当前机器上不存在，显示错误消息
      echo "$file does not exists!"
    fi
  done
done
