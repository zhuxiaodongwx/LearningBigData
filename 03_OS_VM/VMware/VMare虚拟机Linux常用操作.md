# 虚拟磁盘释放空间



VMware Workstation17的Linux虚拟机磁盘文件在删除虚拟机中的文件后，可能并不会自动收缩。要释放这部分空间，可以采取以下几个步骤：

- 首先，在虚拟机的Linux系统中，用`dd`命令创建一个包含零的大文件（注意：这个文件要放置到你清理的磁盘所挂载的目录），这将覆盖磁盘上的空闲空间。例如，使用以下命令：

```
sudo dd if=/dev/zero of=/java/data/zero-fill.file bs=1M
```

这将创建一个名为`zero-fill.file`的文件，包含虚拟磁盘上的所有未使用空间。运行这个命令可能需要一段时间，当空间填充完毕后，命令将报告没有足够的空间来创建文件。

- 接下来，删除创建的零填充文件：

```
sudo rm /java/data/zero-fill.file
```

- 然后，关闭虚拟机并打开VMware Workstation。
- 在VMware Workstation中，选中您要压缩磁盘文件的虚拟机，然后转到“编辑虚拟机设置“>“选择清理的磁盘”>“压缩”。
- VMware Workstation将开始压缩虚拟磁盘文件，这可能需要一段时间。

完成这些步骤后，虚拟磁盘文件的大小应该会减小，从而释放它之前占用的空间。请注意，虚拟磁盘文件的实际大小可能仍然略大于实际数据所需的空间，因为磁盘文件可能包含一些元数据和文件系统结构。



# 虚拟磁盘扩容

在vmware workstation中，有一台centOS虚拟机，原硬盘大小是256G，现在我已经调整为512G，文件系统xfs，在虚拟机中如何进行扩容操作，扩容不能删除我的数据 



在CentOS上扩展一个已存在的XFS文件系统，不会删除任何数据。以下是逐步说明：

### 第一步：找出需要扩展的逻辑卷和物理卷

通过运行 `lvdisplay` 和 `pvdisplay`，找出哪个逻辑卷（LV）和物理卷（PV）需要扩展。

```bash
lvdisplay
```

```bash
pvdisplay
```

### 第二步：使用`fdisk`或`parted`来扩展物理卷

1. 使用 `fdisk` 删除旧的分区，并创建一个更大的新分区。

    ```bash
    fdisk /dev/nvme0n1
    ```

    注意：删除分区并重新创建一个更大的分区并不会删除数据，但请务必谨慎操作。

2. 在 `fdisk` 中，使用 `d` 删除旧的分区（一般是 `/dev/nvme0n1p2`），然后使用 `n` 创建一个新的分区，从相同的开始点到磁盘的末端。

3. 修改新分区的类型，使其与旧的分区类型相同（通常是 Linux LVM，ID是8e）。

4. 最后，使用 `w` 保存更改并退出。

### 第三步：扫描新的分区，并扩展物理卷

```bash
partprobe /dev/nvme0n1
```

然后，扩展物理卷：

```bash
pvresize /dev/nvme0n1p2
```

### 第四步：扩展逻辑卷

现在，扩展逻辑卷。通常这可以用以下命令完成：

```bash
lvextend -l +100%FREE /dev/mapper/centos-root
```

这将使用所有剩余的未分配空间来扩展逻辑卷。

### 第五步：扩展XFS文件系统

```bash
xfs_growfs /dev/mapper/centos-root
```

这将使文件系统扩展到逻辑卷的全部大小。

### 第六步：确认更改

最后，用 `df -h` 确认文件系统已成功扩展。

```bash
df -h
```

这样，你就在不删除任何数据的情况下完成了XFS文件系统的扩展。这些命令通常需要root权限来执行，所以确保你以root用户身份运行它们。

**注意：在进行此类操作之前，务必做好完整的数据备份以防万一。**





# 新增硬盘并自动挂载

在 VMware Workstation 17 中为 CentOS 7 虚拟机添加 SCSI 硬盘并挂载到 /java/data 目录下，可以按照以下步骤操作：

1. 确保您已经为虚拟机添加了 SATA 硬盘。如果没有，请在虚拟机设置中添加。

2. 启动 CentOS 7 虚拟机。

3. 打开终端并使用 `fdisk -l` 命令查看所有磁盘设备。新添加的硬盘可能是 `/dev/sdb` 或类似的设备名。

4. 使用 `fdisk` 命令对新硬盘进行分区：

```
sudo fdisk /dev/sdb
```

接下来，按照提示创建一个新分区，然后保存并退出。

5. 格式化新分区。这里我们使用 `ext4` 文件系统：

```
sudo mkfs.ext4 /dev/sdb1
```

请将 `/dev/sdb1` 替换为您实际创建的分区名。

6. 创建挂载点：

```
sudo mkdir -p /java/data
```

7. 挂载新分区到 `/java/data` 目录：

```
sudo mount /dev/sdb1 /java/data
```

8. 为了在系统重启后自动挂载该分区，需要编辑 `/etc/fstab` 文件。首先，使用以下命令查看分区的 UUID：

```
sudo blkid /dev/sdb1
```

记下 UUID，然后使用文本编辑器（如 vim 或 nano）打开 `/etc/fstab` 文件：

```
sudo vim /etc/fstab
```

在文件末尾添加以下内容（请用实际的 UUID 替换 `<UUID>`）：

```
UUID=<UUID> /java/data ext4 defaults 0 0
```

保存并关闭文件。

9. 最后，执行以下命令使更改生效：

```
sudo mount -a
```

现在新添加的 SATA 硬盘已经成功挂载到 `/java/data` 目录下。在该目录下创建、读取和写入文件时，数据将存储在新硬盘上。





# 磁盘格式化

当您使用 `fdisk` 命令对新硬盘进行分区时，需要遵循以下步骤。

这里我们以 `/dev/sdb` 为例，这是一个典型的新添加的硬盘设备名称。请根据实际情况替换为您的设备名。

1. 以 root 权限运行 fdisk 命令：

```
sudo fdisk /dev/sdb
```

进入 fdisk 交互模式。

2. 创建一个新分区：

首先，键入 `n`（new）以创建新分区。

```
Command (m for help): n
```

3. 选择分区类型：

您可以选择主分区（primary）或扩展分区（extended）。通常情况下，选择主分区即可。键入 `p`（primary）以创建主分区。

```
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
```

4. 分区号：

接下来，为新分区分配一个分区号。通常情况下，系统会自动选择第一个可用的分区号。按回车键接受默认值。

```
Partition number (1-4, default 1): 
```

5. 设定分区起始位置：

系统会建议一个默认的起始位置。您可以直接按回车键接受默认值。

```
First sector (2048-xxxxxx, default 2048): 
```

6. 设定分区结束位置：

系统会建议一个默认的结束位置，您可以直接按回车键接受默认值。如果您想要设定一个特定大小的分区，可以输入 `+size{K,M,G}`（例如：+10G 创建一个 10GB 的分区）。

```
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-xxxxxx, default xxxxxx): 
```

7. 查看分区：

键入 `p`（print），查看创建的分区。

```
Command (m for help): p
```

8. 保存分区表并退出 fdisk：

键入 `w`（write）以保存更改并退出 fdisk。

```
Command (m for help): w
```

这样，您就成功地使用 `fdisk` 命令为新硬盘创建了一个新分区。在上面的例子中，新分区的设备名为 `/dev/sdb1`。接下来，您可以按照之前的回答中的步骤，对新分区进行格式化、挂载和配置自动挂载。





# 业务数据迁移到新磁盘并映射路径

CentOs7中，目录/java/hadoop/datda是一个重要的数据存储目录，之前是使用磁盘a，因为容量问题，新增磁盘b，挂载为/mnt/java/hadoop/datda,但是/java/hadoop/datda这个目录涉及非常多的配置文件，如何把数据迁移过去，并且无需修改配置文件

------



要将数据从旧磁盘迁移到新磁盘，并使配置文件保持不变，可以使用以下步骤：

1. 首先，确保所有使用 `/java/hadoop/datda` 的服务和程序都已停止，以避免在迁移过程中发生数据损坏或不一致。

2. 使用`rsync`命令将数据从旧目录复制到新目录。这将确保数据的完整性和一致性。

```bash
sudo rsync -avhP /java/hadoop/datda/ /mnt/java/hadoop/datda/
```

3. 将旧目录重命名或移动，以便稍后检查和回滚（如有必要）：

```bash
sudo mv /java/hadoop/datda /java/hadoop/datda_old
```

4. 使用`mount`命令将新磁盘（已挂载为`/mnt/java/hadoop/datda`）绑定到旧目录。这将使得所有配置文件仍然指向`/java/hadoop/datda`，但实际上数据已存储在新磁盘上。

```bash
sudo mount --bind /mnt/java/hadoop/datda /java/hadoop/datda
```

5. 为了使绑定永久生效，请将以下行添加到`/etc/fstab`文件：

```
/mnt/java/hadoop/datda  /java/hadoop/datda  none  bind  0  0
```

6. 保存更改并检查是否已正确挂载：

```bash
sudo mount -a
df -h | grep /java/hadoop/datda
```

7. 如果一切正常，可以重启之前停止的服务和程序。这样，所有配置文件将继续使用原始路径，但数据实际上已经迁移到了新磁盘。

8. 在确认一切正常工作后，可以删除旧目录以释放磁盘空间：

```bash
sudo rm -rf /java/hadoop/datda_old
```

现在，您已经成功地将数据从磁盘a迁移到磁盘b，而无需更改任何配置文件。

------



sudo mount --bind /mnt/java/hadoop/datda /java/hadoop/datda
这是什么意思，和创建软连接、硬链接有什么区别？

------



`sudo mount --bind /mnt/java/hadoop/datda /java/hadoop/datda` 命令用于将一个目录绑定到另一个目录。这将使源目录（/mnt/java/hadoop/datda）在目标目录（/java/hadoop/datda）上呈现其内容。这意味着，访问目标目录时，实际上会查看和操作源目录中的内容。这是一种在不更改配置文件的情况下将数据迁移到新位置的方法。

绑定挂载与软链接和硬链接有一些区别：

1. 绑定挂载：
   - 作用于目录，而不仅仅是单个文件。
   - 可以跨文件系统使用。
   - 创建的绑定挂载在文件系统层面生效，而不是在磁盘层面。

2. 软链接（符号链接）：
   - 是一个特殊的文件，它指向另一个文件或目录。
   - 可以跨文件系统。
   - 如果源文件或目录被删除，软链接将不再有效，因为它只是指向源的路径。

3. 硬链接：
   - 是一个指向文件内容的引用，与源文件具有相同的索引节点（inode）。
   - 不能跨文件系统。
   - 只能链接到文件，不能链接到目录。
   - 当源文件被删除时，硬链接仍然有效，因为它直接指向文件内容。硬链接只有在没有任何引用指向文件内容时，文件内容才会被删除。

在这个场景中，我们使用绑定挂载的原因是：

- 您需要在目录级别操作，而硬链接无法实现。
- 您希望在文件系统级别处理迁移，以便在不更改任何配置文件的情况下实现平滑迁移。尽管软链接在某些情况下可能适用，但它可能导致某些程序或服务遇到问题，因为它们无法处理软链接。