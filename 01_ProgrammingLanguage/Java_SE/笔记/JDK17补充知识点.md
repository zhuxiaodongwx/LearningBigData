## Java模块化系统（Jigsaw项目）

**1. Java模块化系统简介**

Java 9引入了模块化系统，这是Java平台的重大变革。模块化系统的目标是使Java应用程序更易于开发、打包、部署和维护。

**2. 模块的概念**

一个模块是包含一组相关类和接口的封闭包，以及一个称为`module-info.java`的模块描述文件。在`module-info.java`文件中，你可以定义模块的名称，以及它所需要的其他模块和它允许访问的包。

**3. 创建和使用模块**

- 定义模块：在源代码目录下创建`module-info.java`文件，定义模块名称，需要的模块，要导出的包。
- 编译模块：使用Java编译器（`javac`）编译模块。
- 打包模块：使用`jar`命令打包模块，生成包含所有类文件和`module-info.class`文件的JAR文件。
- 运行应用程序：使用`java`命令，并加上`--module-path`或`-p`选项，来指定模块的位置，然后指定主模块和主类。

**4. 模块的访问控制**

模块内部的包，除非被明确地声明为`exports`，否则即使包内的类和接口声明为`public`，它们也不能被该模块之外的其他模块访问。这提高了封装性和代码的可维护性。

**示例**

```java
module com.example.myModule {
    requires java.logging;
    requires java.sql;
    exports com.example.myModule.api;
    exports com.example.myModule.model;
}
```

在这个示例中，`com.example.myModule`模块依赖`java.logging`和`java.sql`模块，并导出了`com.example.myModule.api`和`com.example.myModule.model`包。只有导出的包中的`public`或`protected`类和接口，以及它们的公共和受保护成员，才可以被其他模块访问。

以上就是Java模块化系统的基础，理解并掌握这些内容，可以帮助你更好地组织和管理你的Java代码，以及优化应用程序的结构和性能。