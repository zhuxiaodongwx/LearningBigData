package cn.xiaodong.javase.chapter02;

/**
 * Java常量
 * <p>
 * 在程序过程中，值不会发生改变的量
 */
public class Constant {

    public static void main(String[] args) {
        //使用双引号包裹的内容都是字符串
        //字符串常量
        System.out.println("Hello");


        //整型常量
        System.out.println(100);


        //浮点类型 小数
        System.out.println(3.14);


        //使用单引号包裹是字符 里面只能有单个内容
        // 字符型常量
        System.out.println('中');
        System.out.println('A');


        //布尔类型常量
        System.out.println(true);
        System.out.println(false);

    }
}
