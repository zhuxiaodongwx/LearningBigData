package cn.xiaodong.javase.chapter02;

/**
 * Java中的类注释
 */
public class Annotation {

    //  (1)单行注释

    /*
        (2)多行注释
     */

    /**
     *   (3)文档注释
     */
}
