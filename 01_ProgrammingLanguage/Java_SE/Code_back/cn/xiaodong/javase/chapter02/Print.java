package cn.xiaodong.javase.chapter02;

/**
 * print输出语句
 */
public class Print {

    public static void main(String[] args) {

        /*
        System.out.println("内容");
         打印完一行换行
        System.out.println(没有内容); 等价于换行

        System.out.print();
         打印完一行不换行 （在一行打印输出）
        */

        System.out.println("tom");
        System.out.println(18);

        System.out.print("jack");
        System.out.print(19);

        //--------------------------
        System.out.println("jack"+19);//jack19
        System.out.println(100+19+"jack");//119jack
    }
}
