package cn.xiaodong.javase.chapter02.operator;

/**
 * 条件运算符
 */
public class OperatorDemo07 {
    public static void main(String[] args) {
        int i = (1 == 2 ? 100 : 200);
        System.out.println(i);//200
        int j = (3 <= 4 ? 500 : 600);
        System.out.println(j);//500
    }
}
