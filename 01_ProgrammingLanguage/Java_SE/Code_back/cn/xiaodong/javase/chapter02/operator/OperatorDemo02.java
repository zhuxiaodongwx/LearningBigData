package cn.xiaodong.javase.chapter02.operator;

/**
 * 运算符+
 */
public class OperatorDemo02 {


    public static void main(String[] args) {

        /**
         *  第一种：对于`+`两边都是数值的话，`+`就是加法的意思
         *
         *  第二种：对于`+`两边至少有一边是字符串得话，`+`就是拼接的意思
         */

        // 字符串类型的变量基本使用
        // 数据类型 变量名称 = 数据值;
        String str1 = "Hello";
        System.out.println(str1); // Hello

        System.out.println("Hello" + "World"); // HelloWorld

        String str2 = "Java";
        // String + int --> String
        System.out.println(str2 + 520); // Java520
        // String + int + int
        // String		+ int
        // String
        System.out.println(str2 + 5 + 20); // Java520
    }
}
