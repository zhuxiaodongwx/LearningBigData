package cn.xiaodong.javase.chapter02.operator;

/**
 * 赋值运算符
 */
public class OperatorDemo04 {

    public static void main(String[] args) {
        int a = 3;
        int b = 4;
        a = a + b;
        System.out.println(a); // 7
        System.out.println(b); // 4
    }

    public static void main2(String[] args) {
        int a = 3;
        int b = 4;
        b += a;// 相当于 b = b + a ;
        System.out.println(a); // 3
        System.out.println(b); // 7

        short s = 3;
        // s = s + 4; 代码编译报错，因为将int类型的结果赋值给short类型的变量s时，可能损失精度
        s += 4; // 代码没有报错
        //因为在得到int类型的结果后，JVM自动完成一步强制类型转换，将int类型强转成short
        System.out.println(s);

        int j = 1;
        j += ++j * j++;//相当于  j = j + (++j * j++);
        System.out.println(j);//5
    }
}
