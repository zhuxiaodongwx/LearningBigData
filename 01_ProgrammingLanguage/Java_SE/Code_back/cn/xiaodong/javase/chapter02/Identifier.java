package cn.xiaodong.javase.chapter02;

/**
 * Java标识符
 */

/*
    标识符,定义；java 对类名 包名 方法名 变量名 等命名时采用的字符序列。
		在java中凡是需要自己命名的位置 都称之为标识符

    规则：（法律法规）
        （1）组成：英文大小写字母，数字，下划线_，美元符号$
        （2）数字不能开头
        （3）严格区分大小写
        （4）不能使用Java的关键字（包含保留字）

    规范：（约定成俗）
         （1）见名知意
         （2）类名、接口名等：每个单词的首字母都大写（大驼峰法则），形式：XxxYyyZzz， 例如：HelloWorld，String，System等
         （3）变量、方法名等：从第二个单词开始首字母大写（小驼峰法则），其余字母小写，形式：xxxYyyZzz， 例如：age,name,bookName,main
         （4）包名等：每一个单词都小写，单词之间使用点.分割，形式：xxx.yyy.zzz， 例如：java.lang
         （5）常量名等：每一个单词都大写，单词之间使用下划线_分割，形式：XXX_YYY_ZZZ， 例如：MAX_VALUE,PI


*/
public class Identifier {


    public static void main(String[] args) {

        //java严格区分大小写
        int a = 10;
        int A = 30;

        System.out.println("a = " + a);
        System.out.println("A = " + A);


        int age = 30;
        int b = 30;
        int bb = 40;
        int bbb = 50;
        int bbbb = 60;


    }

}
