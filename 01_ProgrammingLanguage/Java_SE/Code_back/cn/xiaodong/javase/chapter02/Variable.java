package cn.xiaodong.javase.chapter02;

/**
 * 变量
 */
public class Variable {

    public static void main(String [] args){
        //声明了一个变量 d 值为3.14
        double d = 3.14;

        d = 6.28;

        System.out.println(d);
    }
}
