package cn.xiaodong.javase.chapter02;

/**
 * Java数据类型
 */

/**
 * java是强类型的语言  想要声明变量 必须指明类型
 *
 *  基本数据类型： 必须掌握的内容
 * 	四类八种
 * 	   整数型：表示整数 1 20  1000
 * 			byte
 * 			short
 * 			int
 * 			long
 * 	   浮点型
 * 		    float
 * 			double
 * 	   字符型
 * 	        char
 * 	   布尔型
 * 	        boolean
 *
 *  引用数据类型: 只要不是基本数据类型 那么就是引用数据类型
 * 		String 字符串  数组  对象。。。
 */
public class DataType {


}
