/**
 * 声明 代码 包
 */
package cn.xiaodong.javase.chapter01;

/**
 * 声明一个类
 */
public class HelloWord {
    /**
     * 声明一个成员变量
     */
    static String out = "Hello World!";

    /**
     * 程序 主函数
     */
    public static void main(String[] args) {
        System.out.println(out);
    }
}
