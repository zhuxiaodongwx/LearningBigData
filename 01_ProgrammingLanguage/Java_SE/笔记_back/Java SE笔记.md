

# 第一章 Java概述

## 今日内容

- Java语言的发展历史

- 安装JDK

- 配置环境变化

- 编写HelloWorld程序

- Java注释

  

## 学习目标

* [ ] 能够使用常见的DOS命令
* [ ] 理解Java语言的跨平台实现原理
* [ ] 理解JDK/JRE/JVM的组成和作用
* [ ] 能够配置环境变量JAVA_HOME（会参照笔记配置）
* [ ] 能够编写HelloWorld程序编译并执行
* [ ] 会使用单行注释和多行注释

## 1.1 JavaSE课程体系介绍

JavaSE知识图解

![1561379629326](Java SE笔记.assets/JavaSE课程内容介绍.png)

JavaSE知识模块介绍

* **第一部分：计算机编程语言核心结构：**`数据类型`、`运算符`、`流程控制`、`数组`、…
* **第二部分：Java面向对象核心逻辑：**`类和对象`、`封装`、`继承`、`多态`、`抽象`、`接口`、…
* **第三部分：JavaSE核心高级应用：**`集合`、`I/O`、`多线程`、`网络编程`、`反射机制`、…
* **第四部分：Java新特性：**`Lambda表达式`、`函数式编程`、`新Date/Time API`、`接口的默认、静态和私有方法`、…
* **第五部分：MySQL/JDBC核心技术：**`SQL语句`、`数据库连接池`、`DBUtils`、`事务管理`、`批处理`、…

## 1.2 计算机语言介绍（了解）

### 计算机语言是什么

所谓计算机编程语言，就是人们可以使用编程语言对计算机下达命令，让计算机完成人们需要的功能。

### 计算机语言发展

![1602830735505](Java SE笔记.assets/1602830735505.png)



***·第一代语言：机器语言(相当于人类的原始阶段)***

机器语言由数字组成所有指令。当让你使用数字编程，写几百个数字、甚至几千个数字，每天面对的是纯数字，我大胆预测：“程序员群体100%会有精神问题”。

机器语言通常由数字串组成（最终被简化成01），对于人类来说，机器语言过于繁琐。使用机器语言，人类无法编出复杂的程序。如下为一段典型的机器码：

\1. 0000,0000,000000010000 代表 LOAD A, 16

\2. 0000,0001,000000000001 代表 ADD    B, 1

\3. 0001,0001,000000010000 代表 STORE C, 16

 

***·第二代语言：汇编语言（相当于人类的手工业阶段）***

为了编程的方便，以及解决更加复杂的问题。程序员开始改进机器语言，使用英文缩写的助记符来表示基本的计算机操作。这些助记符构成了汇编语言的基础。如下是一些常见的汇编语言助记符(单词)比如：LOAD、MOVE之类，这样人更容易使用了。识别几百、几千个单词，感觉要比几百几千个数字，美妙多了。汇编语言相当于人类的手工业社会，需要技术极其娴熟的工匠，但是开发效率也非常低。

汇编语言虽然能编写高效率的程序，但是学习和使用都不是易事，并且很难调试。另一个复杂的问题，汇编语言以及早期的计算机语言（Basic、Fortran等）没有考虑结构化设计原则，而是使用goto语句来作为程序流程控制的主要方法。这样做的后果是：一大堆混乱的调转语句使得程序几乎不可能被读懂。对于那个时代的程序员，能读懂上个月自己写的代码都成为一种挑战。

 

***·第三代：高级语言（相当于人类的工业阶段）***

对于简单的任务，汇编语言可以胜任。但是随着计算机的发展，渗透到了工作生活的更多的方面，一些复杂的任务出现了，汇编语言就显得力不从心（应该说是程序员使用汇编语言解决复杂问题出现了瓶颈）。于是，出现了高级语言。像我们熟知的C、C++、Java等等都是高级语言。

高级语言允许程序员使用接近日常英语的指令来编写程序。例如，实现一个简单的任务：A+B=C  , 使用机器语言、汇编语言和高级语言的的实现如下图1-2所示。

| ![image-20220925105003288](Java SE笔记.assets/image-20220925105003288.png) |
| ------------------------------------------------------------ |
| ***三代计算机语言的直观对比***                               |

 

***从上面这个简单的加法计算，可以看出越到高级语言，越接近人的思维，人使用起来就越方便。***

高级语言的出现，尤其是面向对象语言的出现，相当于人类的工业社会，高级语言极其易用，编程门槛和难度大大降低，大量的人员进入软件开发行业，为软件爆发性的增长提供了充足的人力资源。目前以及可预见的将来，计算机语言仍然处于“第三代高级语言”阶段。



### 计算机语言排行榜

![1564370752557](Java SE笔记.assets/TIOBE语言排行.png)

### 计算机语言走势

![1561382254180](Java SE笔记.assets/TIOBE社区语言发展指数.png)

## 1.3 Java语言概述（了解）

### Java生态圈

**Java是目前应用最为广泛的软件开发平台之一。**随着Java以及Java社区的不断壮大，Java 也早已不再是简简单单的一门计算机语言了，它更是一个平台、一种文化、一个社区。

**作为一个平台，**Java虚拟机扮演着举足轻重的作用。除了 Java语言，任何一种能够被编译成字节码的计算机语言都属于Java这个平台。Groovy、Scala、 JRuby、Kotlin等都是Java平台的一部分，它们依赖于Java虚拟机，同时，Java平台也因为它们变得更加丰富多彩。

**作为一种文化，**Java几乎成为了 “开源”的代名词。在Java程序中，有着数不清的开源软件和框架。如Tomcat、Struts, Hibernate, Spring等。就连JDK和JVM自身也有不少开源的实现，如OpenJDK、Apache Harmony。可以说，“共享”的精神在Java世界里体现得淋漓尽致。

**作为一个社区，**Java拥有全世界最多的技术拥护者和开源社区支持，有数不清的论坛和资料。从桌面应用软件、嵌入式开发到企业级应用、后台服务器、中间件，都可以看到Java的身影。其应用形式之复杂、参与人数之众多也令人咋舌。可以说，Java社区已经俨然成为了一个良好而庞大的生态系统。**其实这才是Java最大的优势和财富。**




### Java语言发展历史

Java诞生于SUN（Stanford University Network），09年SUN被Oracle（甲骨文）收购。

Java之父是詹姆斯.高斯林(James Gosling)。

![1602832163311](Java SE笔记.assets/1602832163311.png)

1996年发布JDK1.0版。

目前最新的版本是Java17。我们学习的Java8。

|   发行版本   |    发行时间    |                             备注                             |
| :----------: | :------------: | :----------------------------------------------------------: |
|     oak      | 1990年代初开发 | 小家电的编程语言，来解决诸如电视机、电话、闹钟、烤面包机等家用电器的控制和通讯问题 |
|     Java     |   1995.05.23   |     Sun公司在Sun world会议上正式发布Java和HotJava浏览器      |
|   Java 1.0   |   1996.01.23   |             Sun公司发布了Java的第一个开发工具包              |
|   Java 1.1   |   1997.02.19   |                                                              |
|   Java 1.2   |   1998.12.08   |    拆分成：J2SE（标准版）、J2EE（企业版）、J2ME（小型版）    |
|   Java 1.3   |   2000.05.08   |                                                              |
|   Java1.4    |   2004.02.06   |                                                              |
| **Java 5.0** |   2004.09.30   | ①版本号从1.4直接更新至5.0；②平台更名为JavaSE、JavaEE、JavaME |
|   Java 6.0   |   2006.12.11   |               2009.04.20 Oracle宣布收购SUN公司               |
|   Java 7.0   |   2011.07.02   |                                                              |
| **Java 8.0** |   2014.03.18   |                         **LTS版本**                          |
|   Java 9.0   |   2017.09.22   |    ①每半年更新一次；②Java 9.0开始不再支持windows 32位系统    |
|  Java 10.0   |   2018.03.21   |                                                              |
|  Java 11.0   |   2018.09.25   |           ①JDK安装包取消独立JRE安装包;②**LTS版本**           |
|  Java 12.0   |   2019.03.19   |                                                              |
|  Java 13.0   |   2019.9.18    |                                                              |
|   java17.0   |   2021.9.14    |                           最新版本                           |

- LTS（Long Term Support）:长期支持版本

### Java技术体系平台

* JavaSE（Java Platform, Standard Edition标准版）：允许您在桌面和服务器上开发和部署Java应用程序。Java提供了丰富的用户界面、性能、多功能性、可移植性和当今应用程序所需的安全性。

* JavaEE（Java Platform, Enterprise Edition企业版）：是为开发企业环境下的应用程序提供的一套解决方案，主要针对于Web应用程序开发。

* JavaME（Java Platform, Micro Edition 小型版）：为互联网上的嵌入式和移动设备上运行的应用提供了一个健壮、灵活的环境：微控制器、传感器、网关、移动电话、个人数字助理（PDA）、电视机顶盒、打印机等等。JavaME包括灵活的用户界面、健壮的安全性、内置的网络协议，以及支持动态下载的网络和离线应用程序。基于JavaME的应用程序在许多设备上都是可移植的，但是利用了每个设备的本机功能。

  

  

  

  ![1597635498066](Java SE笔记.assets/1597635498066.png)





## 1.4 Java语言跨平台原理

### Java语言的特点

* **完全面向对象：**Java语言支持封装、继承、多态，面向对象编程，让程序更好达到`高内聚`，`低耦合`的标准。
* **支持分布式：**Java语言支持Internet应用的开发，在基本的Java应用编程接口中有一个网络应用编程接口（java net），它提供了用于网络应用编程的类库，包括URL、URLConnection、Socket、ServerSocket等。Java的RMI（远程方法激活）机制也是开发分布式应用的重要手段。
* **健壮型：**Java的强类型机制、异常处理、垃圾的自动收集等是Java程序健壮性的重要保证。对指针的丢弃是Java的明智选择。
* **安全：**Java通常被用在网络环境中，为此，Java提供了一个安全机制以防恶意代码的攻击。如：安全防范机制（类ClassLoader），如分配不同的名字空间以防替代本地的同名类、字节代码检查。
* **跨平台性：**Java程序（后缀为java的文件）在Java平台上被编译为体系结构中立的字节码格式（后缀为class的文件），然后可以在实现这个Java平台的任何系统中运行。

### Java语言的跨平台原理

- **跨平台**：任何软件的运行，都必须要运行在操作系统之上，而我们用Java编写的软件可以运行在任何的操作系统上，这个特性称为**Java语言的跨平台特性**。该特性是由JVM实现的，我们编写的程序运行在JVM上，而JVM运行在操作系统上。
- **JVM**（Java Virtual Machine ）：Java虚拟机，简称JVM，是运行所有Java程序的假想计算机，是Java程序的运行环境之一，也是Java 最具吸引力的特性之一。我们编写的Java代码，都运行在**JVM** 之上。

![1602837063456](Java SE笔记.assets/1602837063456.png)

如图所示，Java的虚拟机本身是不具备跨平台功能的，每个操作系统下都有不同版本的虚拟机。

- **JRE ** (Java Runtime Environment) ：是Java程序的运行时环境，包含`JVM` 和运行时所需要的`核心类库`。
- **JDK**  (Java Development Kit)：是Java程序开发工具包，包含`JRE` 和开发人员使用的工具。

我们想要运行一个已有的Java程序，那么只需安装`JRE` 即可。

我们想要开发一个全新的Java程序，那么必须安装`JDK` ，其内部包含`JRE`。

![](Java SE笔记.assets/JDKJRE.jpg)

![1561383524152](Java SE笔记.assets/JDKJREJVM.png)

## 1.5 JDK下载和安装

### JDK的下载

* 下载网址：www.oracle.com 

* 下载步骤：

  * 登录Oracle公司官网，www.oracle.com，如图所示：在底部选择Developers开发者

    ![1572254490435](Java SE笔记.assets/下载1.png)

  * 在**Developers**页面中间的技术分类部分，选择`Java`，单击进入，如图所示：

    ![1572309624793](Java SE笔记.assets/下载2.png)

  * 下拉页面，找到**Java**，在此选择`JavaSEDownload`，单击进入，如图所示：

  ![1572309711034](Java SE笔记.assets/下载3.png)

  * 选择Downloads选项卡，默认是最新版的Java13下载，在此处选择`Oracle JDK DOWNLOAD`，单击进入可以下载JDK13，如图所示：

![1572309841433](Java SE笔记.assets/下载4.png)

选择**Accept License Agreement**，并选择对应的操作系统类型，如图所示

![1572310014408](Java SE笔记.assets/下载5.png)

  * 如果要下载之前JDK版本，那么在刚才JavaSE/Download页面，下拉到最下面，找到Java Archive（Java档案馆），单击Download

![1572310151403](Java SE笔记.assets/下载6.png)

![](Java SE笔记.assets/下载7.png)

例如：这里选择JavaSE 8(8U211 and later)，选择**Accept License Agreement**，并选择对应的操作系统类型。早期版本分为32位/64位操作系统区分，其中x86表示32位，x64表示64位。

![1572310481782](Java SE笔记.assets/下载8.png)




### JDK的安装

* 安装步骤：

  * 双击`jdk-8u202-windows-x64.exe`文件，并单击`下一步`，如图所示：

    ![](Java SE笔记.assets/jdk1.jpg)

  * 取消独立JRE的安装，单击`公共JRE前的下拉列表`，选择`此功能将不可用`如图所示：

    ![](Java SE笔记.assets/jdk2.jpg)

  * 修改安装路径，单击更改，如图所示：

    ![](Java SE笔记.assets/jdk3.jpg)

  * 将安装路径修改为`D:\develop\Java\jdk1.8.0_202\`，并单击确定，如图所示：

    ![](Java SE笔记.assets/jdk4.jpg)

  * 单击下一步，如图所示：

    ![](Java SE笔记.assets/jdk5.jpg)

  * 稍后几秒，安装完成，如图所示：

    ![](Java SE笔记.assets/jdk6.jpg)

  * 目录结构，如图所示：

    ![1561386792819](Java SE笔记.assets/jdk7.png)

## 1.6  常用DOS命令（了解）

### 什么是DOS

Java语言的初学者，学习一些DOS命令，会非常有帮助。DOS是一个早期的操作系统，现在已经被Windows系统取代，对于我们开发人员，目前需要在DOS中完成一些事情，因此就需要掌握一些必要的命令。

Dos： Disk Operating System 磁盘操作系统, 简单说一下windows的目录结构。

![image-20200213095127784](Java SE笔记.assets/image-20200213095127784.png)

### 进入DOS操作窗口

* 按下`Windows+R`键，打开运行窗口，输入`cmd`回车，进入到DOS的操作窗口。

  ![运行窗口](Java SE笔记.assets/运行窗口.jpg)

* 打开DOS命令行后，看到一个路径 `C:\Users\...`  就表示我们现在操作的磁盘是C盘的Users的final目录。

![image-20200213083838587](Java SE笔记.assets/image-20200213083838587.png)

### 常用命令

#### 进入目录命令：cd

（1）回到根目录

```cmd
cd /  或  cd \
```

![image-20200213103050182](Java SE笔记.assets/image-20200213103050182.png)

（2）切换到上一级

```
cd ..
```

![image-20200213102356751](Java SE笔记.assets/image-20200213102356751.png)

（3）当前盘的其他目录下

绝对路径：从根目录开始定位，例如：cd d:\test200\1  或者  cd d:/test200/1

例如：现在在d:/test100/hello/a目录，要切换到d:/test200/1目录

![image-20200213101458688](Java SE笔记.assets/image-20200213101458688.png)

![image-20200213101428222](Java SE笔记.assets/image-20200213101428222.png)

![image-20200213101246431](Java SE笔记.assets/image-20200213101246431.png)

#### 切换盘符命令

（1）直接盘符:

```cmd
例如：要切换到D盘，直接d:
```

（2）使用cd命令

```cmd
例如：要切换到E盘，可以使用cd /D e:
```

使用 /D 开关，除了改变驱动器的当前目录之外，还可改变当前驱动器。

![image-20200213101745482](Java SE笔记.assets/image-20200213101745482.png)

#### 查看当前目录下有什么命令：dir

![image-20200213101952912](Java SE笔记.assets/image-20200213101952912.png)

![image-20200213102043229](Java SE笔记.assets/image-20200213102043229.png)

####  新建目录命令：md (make directory)

```cmd
//在当前目录下创建hello文件夹
md hello

//在当前目录下创建a,b,c三个文件夹
md a b c

//在d盘test200下创建ok200文件夹
md d:\test200\ok200
```

![image-20200213103500670](Java SE笔记.assets/image-20200213103500670.png)



#### 删除文件命令：del

```cmd
//删除指定文件
del 文件名.扩展名
del 目标目录\文件名.扩展名

删除所有文件并询问
del *.*

删除所有文件不询问
del /Q *.*
```

![image-20200213104335172](Java SE笔记.assets/image-20200213104335172.png)

![image-20200213104501033](Java SE笔记.assets/image-20200213104501033.png)

![image-20200213104938765](Java SE笔记.assets/image-20200213104938765.png)

#### 删除目录命令：rd（remove directory)

```
//删除空目录
rd 空目录名

//删除目录以及下面的子目录和文件，带询问
rd /S 非空目录名

//删除目录以及下面的子目录和文件，不带询问
rd /S/Q 非空目录名
```

注意：你在d:\test100\hello\a中，你不能删除test100、hello、a这几个目录

![image-20200213105002765](Java SE笔记.assets/image-20200213105002765.png)

![image-20200213105012075](Java SE笔记.assets/image-20200213105012075.png)



#### 清屏命令：cls

```cmd
cls
```

#### 退出命令：exit

```java
exit
```

## 1.7 配置环境变量

为什么配置path？

希望在命令行使用javac.exe等工具时，任意目录下都可以找到这个工具所在的目录。

例如：我们在C:\Users\Irene目录下使用java命令，结果如下：

![1572317249341](Java SE笔记.assets/1572317249341.png)

我们在JDK的安装目录的bin目录下使用java命令，结果如下：

![1572317330332](Java SE笔记.assets/1572317330332.png)

我们不可能每次使用java.exe，javac.exe等工具的时候都进入到JDK的安装目录下，太麻烦了。我们希望在任意目录下都可以使用JDK的bin目录的开发工具，因此我们需要告诉操作系统去哪里找这些开发工具，这就需要配置path环境变量。

### 1.7.1 只配置path

* 步骤：

  * 打开桌面上的计算机，进入后在左侧找到`计算机`，单击鼠标`右键`，选择`属性`，如图所示：

    ![](Java SE笔记.assets/环境变量1.jpg)

  * 选择`高级系统设置`，如图所示：

    ![](Java SE笔记.assets/环境变量2.jpg)

  * 在`高级`选项卡，单击`环境变量`，如图所示：

    ![](Java SE笔记.assets/环境变量3.jpg)

  * 在`系统变量`中，选中`Path` 环境变量，`双击`或者`点击编辑` ,如图所示：

    ![](Java SE笔记.assets/环境变量6.jpg)

  * 在变量值的最前面，键入`D:\develop\Java\jdk1.8.0_202\bin;`  分号必须要写，而且还要是**英文符号**。如图所示：

    ![1561386643207](Java SE笔记.assets/环境变量9.png)

  * 环境变量配置完成，**重新开启**DOS命令行，在任意目录下输入`javac` 命令，运行成功。

    ![](Java SE笔记.assets/环境变量8.jpg)


### 1.7.2 配置JAVA_HOME+path

* 步骤：

  * 打开桌面上的计算机，进入后在左侧找到`计算机`，单击鼠标`右键`，选择`属性`，如图所示：

    ![](Java SE笔记.assets/环境变量1.jpg)

  * 选择`高级系统设置`，如图所示：

    ![](Java SE笔记.assets/环境变量2.jpg)

  * 在`高级`选项卡，单击`环境变量`，如图所示：

    ![](Java SE笔记.assets/环境变量3.jpg)

  * 在`系统变量`中，单击`新建` ，创建新的环境变量，如图所示：

    ![](Java SE笔记.assets/环境变量4.jpg)

  * 变量名输入`JAVA_HOME`，变量值输入 `D:\develop\Java\jdk1.8.0_202` ，并单击`确定`，如图所示：

    ![](Java SE笔记.assets/环境变量5.jpg)

  * 选中`Path` 环境变量，`双击`或者`点击编辑` ,如图所示：

    ![](Java SE笔记.assets/环境变量6.jpg)

  * 在变量值的最前面，键入`%JAVA_HOME%\bin;`  分号必须要写，而且还要是**英文符号**。如图所示：

    ![](Java SE笔记.assets/环境变量7.jpg)

  * 环境变量配置完成，**重新开启**DOS命令行，在任意目录下输入`javac` 命令，运行成功。

    ![](Java SE笔记.assets/环境变量8.jpg)


## 1.8 入门程序HelloWorld

### 1.8.1 HelloWorld案例

#### 程序开发步骤说明

JDK安装完毕，可以开发我们第一个Java程序了。

Java程序开发三步骤：**编写**、**编译**、**运行**。

![开发步骤](Java SE笔记.assets/开发步骤.jpg)

#### 编写Java源程序

1. 在`D:\atguigu\day01_code` 目录下新建文本文件，完整的文件名修改为`HelloWorld.java`，其中文件名为`HelloWorld`，后缀名必须为`.java` 。
2. 用记事本或notepad++等文本编辑器打开

3. 在文件中键入文本并保存，代码如下：

```java
public class HelloWorld {
  	public static void main(String[] args) {
    	System.out.println("HelloWorld");
  	}
}
```

> 友情提示：
>
> 每个字母和符号必须与示例代码一模一样。

第一个`HelloWord` 源程序就编写完成了，但是这个文件是程序员编写的，JVM是看不懂的，也就不能运行，因此我们必须将编写好的`Java源文件` 编译成JVM可以看懂的`字节码文件` ，也就是`.class`文件。

编译Java源文件

在DOS命令行中，**进入**`D:\atguigu\javaee\JavaSE20190624\code\day01_code`**目录**，使用`javac` 命令进行编译。

命令：

```java
javac Java源文件名.后缀名
```

举例：

```
javac HelloWorld.java
```

![1561387081272](Java SE笔记.assets/HelloWorld编译结果.png)

编译成功后，命令行没有任何提示。打开`D:\atguigu\javaee\JavaSE20190624\code\day01_code`目录，发现产生了一个新的文件 `HelloWorld.class`，该文件就是编译后的文件，是Java的可运行文件，称为**字节码文件**，有了字节码文件，就可以运行程序了。 

> Java源文件的编译工具`javac.exe`

#### 运行Java程序

在DOS命令行中，**进入Java源文件的目录**，使用`java` 命令进行运行。

命令：

```java
java 类名字
```

举例：

```
java HelloWorld
```

> 友情提示：
>
> java HelloWord  不要写 不要写 不要写 .class

![1561387134284](Java SE笔记.assets/HelloWorld运行结果.png)

> Java字节码文件的运行工具：java.exe

### 1.8.2 HelloWorld案例常见错误

* 	单词拼写问题
   * 正确：class		错误：Class
   * 正确：String              错误：string
   * 正确：System            错误：system
   * 正确：main		错误：mian
* 	Java语言是一门严格区分大小写的语言
* 	标点符号使用问题
   * 不能用中文符号，英文半角的标点符号（正确）
   * 括号问题，成对出现

### 1.8.3 Java程序的结构与格式

结构：

```java
类{
    方法{
        语句;
    }
}
```

格式：

（1）每一级缩进一个Tab键

（2）{}的左半部分在行尾，右半部分单独一行，与和它成对的"{"的行首对齐

### 1.8.4 Java程序的入口

Java程序的入口是main方法

```java
public static void main(String[] args){
    
}
```

### 1.8.5 编写Java程序时应该注意的问题

1、字符编码问题

当cmd命令行窗口的字符编码与.java源文件的字符编码不一致，如何解决？

![1557881223916](Java SE笔记.assets/命令行编译乱码.png)

解决方案一：

	在Notepad++等编辑器中，修改源文件的字符编码

![1557881271819](Java SE笔记.assets/Notepad修改源文件字符编码.png)

解决方案二：

	在使用javac命令式，可以指定源文件的字符编码

```cmd
javac -encoding utf-8 Review01.java
```



2、大小写问题

（1）源文件名：

	不区分大小写，我们建议大家还是区分

（2）字节码文件名与类名

	区分大小写

（3）代码中

	区分大小写



3、源文件名与类名一致问题？

（1）源文件名是否必须与类名一致？public呢？

如果这个类不是public，那么源文件名可以和类名不一致。

如果这个类是public，那么要求源文件名必须与类名一致。

我们建议大家，不管是否是public，都与源文件名保持一致，而且一个源文件尽量只写一个类，目的是为了好维护。



（2）一个源文件中是否可以有多个类？public呢？

一个源文件中可以有多个类，编译后会生成多个.class字节码文件。

但是一个源文件只能有一个public的类。



（3）main必须在public的类中吗？

不是。

但是后面写代码时，基本上main习惯上都在public类中。





# 第2章 Java基础语法

## 学习目标

* [ ] 会使用单行注释和多行注释

* [ ] 能够辨识关键字

* [ ] 理解标识符的含义

* [ ] 理解Java中的基本数据类型分类

* [ ] 能够理解常量的概念

* [ ] 能够定义8种基本数据集类型的变量

* [ ] 能够分清楚两种输出语句的区别

* [ ] 了解进制

* [ ] 理解基本数据类型的自动类型转换

* [ ] 理解基本数据类型的强制类型转换

* [ ] 了解ASCII编码表和Unicode编码表

* [ ] 理解int类型和char类型的运算原理

* [ ] 理解运算符++ --的运算方式

* [ ] 理解+符号在字符串中的作用

* [ ] 掌握算术运算符

* [ ] 掌握赋值运算符

* [ ] 掌握比较运算符

* [ ] 理解逻辑运算符

* [ ] 掌握三元运算符的格式和计算结果

* [ ] ## 了解位运算符

## 2.1 注释（*annotation*）

- **注释**：就是对代码的解释和说明。其目的是让人们能够更加轻松地了解代码。为代码添加注释，是十分必须要的，它不影响程序的编译和运行。

- Java中有`单行注释`、`多行注释`和`文档注释`

  - 单行注释以 `//`开头，以`换行`结束，格式如下：

    ```java
    // 注释内容
    ```

  - 多行注释以 `/*`开头，以`*/`结束，格式如下：

    ```java
    /*
    	注释内容
    	注释内容
     */
    ```

  - 文档注释以`/**`开头，以`*/`结束 

    ```java
    /**
    	注释内容
    	@author   可指定 java java程序的作者
    	@version  可指定源文件的版本
    */
    ```

    文档注释可以被JDK提供的javadoc工具解析生成一套网页形式的说明文档，操作入下：

    ```shell
    D:\>javadoc -d mydoc -author -version Hello.java
    ```

    

## 2.2 关键字（*keyword*）

- **什么是关键字**：被java语言赋予了特殊含义的字符串（单词）。

  ​		例如：HelloWorld案例中，出现的关键字有 `public ` 、`class` 、 `static` 、  `void`  等，这些单词已经被Java定义，具有明确的含义。

- **关键字的特点**：全部都是`小写字母`。

- **Java有哪些关键字：**

![1555209180504](Java SE笔记.assets/关键字表.png)

>  **关键字一共50个，其中const和goto是保留字。**

> **true,false,null看起来像关键字，但从技术角度，它们是特殊的布尔值和空值。**

*建议：关键字比较多，不需要死记硬背，学到哪里记到哪里即可。*

## 2.3 标识符( identifier)

- **标识符概念：**即给类、变量、方法、包等命名的字符序列，称为标识符。

  简单的说，凡是程序员自己命名的部分都可以称为标识符。

- **标识符的命名规则（必须遵守）**

  （1）组成：英文大小写字母，数字，下划线_，美元符号$

  （2）数字不能开头

  （3）严格区分大小写

  （4）不能使用Java的关键字（包含保留字）

- **标识符的命名规范（遭受鄙视）**

  （1）见名知意

  （2）类名、接口名等：每个单词的首字母都大写（大驼峰法则），形式：XxxYyyZzz，

  例如：HelloWorld，String，System等

  （3）变量、方法名等：从第二个单词开始首字母大写（小驼峰法则），其余字母小写，形式：xxxYyyZzz，

  例如：age,name,bookName,main

  （4）包名等：每一个单词都小写，单词之间使用点.分割，形式：xxx.yyy.zzz，

  例如：java.lang

  （5）常量名等：每一个单词都大写，单词之间使用下划线_分割，形式：XXX_YYY_ZZZ，

  例如：MAX_VALUE,PI

## 2.4 常量（*constant*）

* **常量概念：**在程序执行的过程中，其值不可以发生改变的量。例如 π的值3.1415926

* **常量的分类**：

  * 自定义常量：通过final关键字定义（后面在面向对象部分讲解）

  * 字面值常量：

    | 字面值常量 |        举例         |
    | :--------: | :-----------------: |
    | 字符串常量 |    "HelloWorld"     |
    |  整数常量  |       12，-23       |
    |  浮点常量  |        12.34        |
    |  字符常量  | 'a'，'A'，'0'，'好' |
    |  布尔常量  |     true，false     |
    |   空常量   |        null         |

    ```java
    public class ConstantDemo {
    	public static void main(String[] args) {
    		//字符串常量
    		System.out.println("HelloWorld");
    		
    		//整数常量
    		System.out.println(12);
    		System.out.println(-23);
    		
    		//小数常量
    		System.out.println(12.34);
    		
    		//字符常量
    		System.out.println('a');
    		System.out.println('A');
            System.out.println('0');
    		
    		//布尔常量
    		System.out.println(true);
    		System.out.println(false);
    	}
    }
    ```

    > 注意事项：
    >
    > ​	字符常量，单引号里面有且仅有一个字符
    >
    > ​	空常量，不可以在输出语句中直接打印

## 2.5 输出语句

* **换行输出语句**：输出内容，完毕后进行换行，格式如下：

  ```java
  System.out.println(输出内容);
  ```

* **直接输出语句**：输出内容，完毕后不做任何处理，格式如下

  ```java
  System.out.print(输出内容);
  ```

**示例代码：**

```java
对比如下两组代码：
System.out.println("tom");
System.out.println(18);

System.out.print("jack");
System.out.print(19);
//--------------------------
System.out.println("jack"+19);//jack19
System.out.println(100+19+"jack");//119jack
```


>注意事项：
>
>​	换行输出语句，括号内可以什么都不写，只做换行处理
>
>​	直接输出语句，括号内什么都不写的话，编译报错
>
>​	不能同时输出多个数据，可以使用 + 把多个数据连接起来，变成为一个数据进行输出。



## 2.6 变量（*variable*）

- **变量的概念:**

  在程序执行的过程中，其值可以发生改变的量

- **变量的作用:**

  用来存储数据，代表内存的一块存储区域，**这块内存中的值是可以改变的**。

  ![](Java SE笔记.assets/变量内存.png)

- **定义格式：**

  1. 声明变量，并同时赋值

     **数据类型 变量名=初始化值；**

     ```java
     //声明并同时赋值，存储一个整数的年龄
     int age = 18;
     //打印输出变量值
     System.out.println("age = " + age);//+号表示连接符，把""中的字符串与变量age中的数据连接进行输出
     ```

  2. **数据类型 变量名;**

     **变量名=初始值；**

     ```java
     //先声明，再赋值
     int age;
     age=18;
     //打印输出变量值
     System.out.println("age = " + age);//18
     ```

     ```java
     //变量可以重新赋值
     age=19;
     System.out.println("age = " + age);//19 
     ```

- **变量的使用注意事项**

  - 先声明后使用

    ​	如果没有声明，会报“找不到符号”错误

  - 在使用之前必须初始化	

    ​	如果没有初始化，会报“未初始化”错误

  - 变量有作用域	

    ​	作用域为变量直接所属的{}范围内，如果超过作用域，也会报“找不到符号”错误

  - 在同一个作用域中不能重名	

    ​	同一个{}，不能同时声明两个同名变量

## 2.7  计算机如何存储数据

计算机世界中只有二进制。那么在计算机中存储和运算的所有数据都要转为二进制。包括数字、字符、图片、声音、视频等。

### 2.7.1 进制（了解）

1. **进制分类与表示方式**

   （1）十进制：
   数字组成：0-9
   进位规则：逢十进一

   ```java
   System.out.println(10);////10表示十进制的10，输出十进制结果10
   ```

   （2）二进制：
   数字组成：0-1
   进位规则：逢二进一

   表示方式：以0b或0B开头

   ```java
   System.out.println(0B10);//0B10表示二进制的10，输出十进制结果2
   ```

   十进制的256，二进制：100000000，为了缩短二进制的表示，又要贴近二进制，在程序中引入八进制和十六进制

   （3）八进制：很少使用
   数字组成：0-7
   进位规则：逢八进一

   表示方式：以0开头

   ```java
   System.out.println(010);//010表示八进制的10，输出十进制结果8
   ```

   与二进制换算规则：每三位二进制是一位八进制值

   （4）十六进制
   数字组成：0-9，a-f或A-F
   进位规则：逢十六进一

   表示方式：以0x或0X开头

   ```java
   System.out.println(10);//0x10表示十六进制的10，输出十进制结果16
   ```

   与二进制换算规则：每四位二进制是一位十六进制值

2. **进制的转换**

   | 十进制 | 二进制 | 八进制 | 十六进制 |
   | ------ | ------ | ------ | -------- |
   | 0      | 0      | 0      | 0        |
   | 1      | 1      | 1      | 1        |
   | 2      | 10     | 2      | 2        |
   | 3      | 11     | 3      | 3        |
   | 4      | 100    | 4      | 4        |
   | 5      | 101    | 5      | 5        |
   | 6      | 110    | 6      | 6        |
   | 7      | 111    | 7      | 7        |
   | 8      | 1000   | 10     | 8        |
   | 9      | 1001   | 11     | 9        |
   | 10     | 1010   | 12     | a或A     |
   | 11     | 1011   | 13     | b或B     |
   | 12     | 1100   | 14     | c或C     |
   | 13     | 1101   | 15     | d或D     |
   | 14     | 1110   | 16     | e或E     |
   | 15     | 1111   | 17     | f或F     |
   | 16     | 10000  | 20     | 10       |

- **十进制转二进制：**

  使用除以2倒取余数的方式
  ![](Java SE笔记.assets/十进制转二进制.jpg)

- **二进制转十进制：**

  从右边开始依次是2的0次，2的1次，2的2次。。。。

  ![](Java SE笔记.assets/二进制转十进制.jpg)

- 二进制数据转八进制数据

  从右边开始，3位二进制对应1位八进制

  ![](Java SE笔记.assets/2、二进制与八进制转换.png)

- 二进制数据转十六进制数据

  从右边开始，4位二进制对应1位十六进制

  ![](Java SE笔记.assets/3、二进制与十六进制转换.png)

### 2.7.2 计算机存储单位


- **位（bit）：**是数据存储的最小单位，也就是一个二进制位。其中8 bit 就称为1个字节(Byte)。
- **字节（Byte）：**是计算机信息技术用于计量存储容量的一种计量单位，1字节等于8bit。
- **转换关系：**
  - 8 bit = 1 Byte
  - 1024 Byte = 1 KB
  - 1024 KB = 1 MB
  - 1024 MB = 1 GB
  - 1024 GB = 1 TB

### 2.7.3 二进制数据存储

计算机底层都是使用二进制进行数据的存储的。不同类型的数据，存储方式也有不同。

1. **整数存储**

   计算机底层存储整数并不是把整数转换为二进制直接存储，而是以二进制的补码形式进行存储。要了解补码还有知道原码和反码：

   **原码**：把十进制转为二进制，然后最高位设置为符号位，1是负数，0是正数。

   **反码**：正整数的反码与原码相同，负整数的反码在原码的基础上，符号位不变，其余位取反（0变1,1变0）

   **补码**：正整数的补码与原码相同，负整数的补码为其反码+1

   ```
   例如：用1个字节的二进制表示一个数
   
   25 ==> 原码  0001 1001 ==> 反码  0001 1001 -->补码  0001 1001
   -25 ==>原码  1001 1001 ==> 反码1110 0110 ==>补码 1110 0111
   ```

    **一个字节可以存储的整数范围**

   分为两种情况：

   （1）无符号：不考虑正负数

   ​		0000 0000 ~ 1111 1111 ==> 0~255

   （2）有符号：-128~127

   ​		0000 0000  ~  0111 111 ==> 0~127

   ​		1000 0001 ~ 1111 1111 ==> -127 ~ -1 （补码形式存储）

   ​		1000 0000  					 ==> -128     特殊值，最高位既是符号位，又是数值位

2. **如何存储小数（了解）**

   - 为什么float（4个字节）比long（8个字节）的存储范围大？
   - 为什么double（8个字节）比float（4个字节）精度范围大？
   - 为什么float和double不精确

   因为float、double底层也是二进制，先把小数转为二进制，然后把二进制表示为科学记数法，然后只保存：

   ①符号位②指数位③尾数位

   > 详见《[float型和double型数据的存储方式.docx](float型和double型数据的存储方式.docx)》

3. ***如何存储字符*（暂）**

   在计算机的内部都是二进制的0、1数据，如何让计算机可以直接识别人类文字的问题呢？就产生出了编码表的概念。

   - **编码表** 

     就是将人类的文字和一个十进制数进行对应起来组成一张表格。例如：

     | 字符 | 十进制数值 | 二进制数值 |
     | :--: | :--------: | :--------: |
     |  0   |     48     | 0011 0000  |
     |  A   |     65     | 0100 0001  |
     |  a   |     97     | 0110 0001  |

     将所有的英文字母，数字，符号都和十进制进行了对应，因此产生了世界上第一张编码表**ASCII**（American Standard Code for Information Interchange 美国标准信息交换码）。

     Unicode(统一码、万国码、单一码)是计算机科学领域里的一项业界标准，包括字符集、编码方案等。Unicode 是为了解决传统的字符编码方案的局限而产生的，它为每种语言中的每个字符设定了统一并且唯一的二进制编码，以满足跨语言、跨平台进行文本转换、处理的要求。

     **Java中使用的字符集：Unicode编码集**

     

   - **Java中字符常量的几种表示方式**

     （1）'一个字符'

     ​	例如：'A'，'0'，'尚'

     （2）转义字符

     ```java
     \n：换行
     \r：回车
     \t：Tab键
     \\：\
     \"："
     \'：'
     \b：删除键Backspace
     
     System.out.println('\\');
     System.out.println("hello\tworld\njava");
     ```

     （3）\u字符的Unicode编码值的十六进制型

     ​	例如：'\u674e'代表'尚'

     ```java
     char c = '\u674e';
     char c = '尚';
     String s = '尚';//错误的，哪怕是一个字符，也要使用双引号
     
     char c2 = '';//错误，单引号中有且只能有一个字符
     String s2 = "";//可以，双引号中可以没有其他字符，表示是空字符串
     ```

     （4）直接给char类型变量赋值十进制的0~65535之间的Unicode编码值

     ​	例如：'尚' 的编码值是23578

     ​	   'a'的编码值是97

     ```java
     char c1 = 23578;
     System.out.println(c1);//李
     
     char c2 = 97;
     System.out.println(c2);//a
     ```

## 2.8 数据类型(data type)

### 2.8.1 数据类型分类

Java是一种强类型的语言，针对每一种数据都定义了数据类型，不同类型的数据二进制表示方式不同，分配的空间大小也有区别，java数据类型主要分为两大类：（定义变量需要确定数据类型，即确定数据使用的空间大小和二进制表示形式）

- **基本数据类型**：包括 `整数`、`浮点数`、`字符`、`布尔`。 
- **引用数据类型**：包括 `类`、`数组`、`接口`。 

### 2.8.2 基本数据类型

四类八种基本数据类型：

![](Java SE笔记.assets/基本数据类型范围.jpg)

> Java中的默认类型：整数类型是`int` 、浮点类型是`double` 。

- 常量整数值都是int类型，占用4个字节空间。

  程序运行期间byte、short、char、boolean实际都是占用4个字节内存空间，
  但在逻辑上：
  ​	byte只有低8位有效空间。
  ​	short只有低16位有效空间。
  ​	所以，可以直接把一个byte范围内的整数常量值直接赋给byte类型变量。short同理。byte b=10;
  赋值给int，只要在int范围即可。
  赋值给long，在int范围内的，可以加也可以不用加L，会自动升级为long，如果数字超过int范围，必须加L。

- 小数常量值，无论多少，不加F，就是double类型。

### 2.8.3 基本数据类型的存储范围

1. #### 整型系列


   （1）byte：字节类型

   占内存：1个字节

   存储范围：-128~127

   ```java
byte b=10;
byte b1=128//编译失败: 不兼容的类型: 从int转换到byte可能会有损失
   ```


   （2）short：短整型类型

   占内存：2个字节

   存储范围：-32768~32767

   ```java
short s=10;
short s1=32768//编译失败: 不兼容的类型: 从int转换到short可能会有损失
   ```


   （3）int：整型

   占内存：4个字节

   存储范围：-2的31次方 ~ 2的31次方-1

   ```java
int i=10;
int i1=12345678900;//编译错误: 过大的整数: 12345678900
   ```

   （4）long：整型

   占内存：8个字节

   存储范围：-2的63次方 ~ 2的63次方-1

   ```java
long j=10;
long j1=12345678900L;
   ```

   > 注意：如果要表示某个超过int范围的常量整数它是long类型，那么需要在数字后面加L

2. #### 浮点型系列（小数）


   （1）float：单精度浮点型

   占内存：4个字节

   精度：科学记数法的小数点后6~7位

   > 注意：如果要表示某个常量小数是float类型，那么需要在数字后面加F或f，否则就是double类型

   （2）double：双精度浮点型

   占内存：8个字节

   精度：科学记数法的小数点后15~16位

   ```java
float f = 12.3F;//右边如果赋值小数常量值，那么必须加F或f
double d = 12.3;		
   ```


3. #### 单字符类型：char

   占内存：2个字节

   ```java
   char ch='a';
   ```

   > **char类型：使用单引号''**

4. #### 布尔类型


   boolean：只能存储true或false

   > 虽然计算机底层使用0和1表示false和true，但是在代码中不能给boolean类型的变量赋值0和1，只能赋值false和true

## 2.9  基本数据类型转换（Conversion）

在Java程序中，不同的基本数据类型的值经常需要进行相互转换。Java语言所提供的**七种数值类型**之间可以相互转换，基本数据类型转换有两种转换方式：自动类型转换和强制类型转换。

### 2.9.1 自动类型转换

将`取值范围小的类型`自动提升为`取值范围大的类型` 。

基本数据类型按照取值范围从小到大的关系，如图所示：

![](Java SE笔记.assets/自动类型转换图1.jpg)

以下情况会发生自动类型转换（隐式类型转换）：

1. 当把存储范围小的值（常量值、变量的值、表达式计算的结果值）赋值给了存储范围大的变量时，

```java
int i = 'A';//char自动升级为int
double d = 10;//int自动升级为double

```

2. 当存储范围小的数据类型与存储范围大的数据类型一起混合运算时，会按照其中最大的类型运算


```java
int i = 1;
byte b = 1;
double d = 1.0;

double sum = i + b + d;//混合运算，升级为double
```

3. 当byte,short,char数据类型进行算术运算时，按照int类型处理


```java
byte b1 = 1;
byte b2 = 2;
byte b3 = b1 + b2;//编译报错，b1 + b2自动升级为int

char c1 = '0';
char c2 = 'A';
System.out.println(c1 + c2);//113 
```

> boolean类型不参与

### 2.9.2 强制类型转换

将`取值范围大的类型`转换成`取值范围小的类型`时需要进行强制（显示）类型转换。

例如：将小数`1.5` 赋值到`int` 类型变量会发生什么？产生编译失败，肯定无法赋值。

```java
int i = 1.5; // 错误
```

想要赋值成功，只有通过强制类型转换，将`double` 类型强制转换成`int` 类型才能赋值。

**转换格式：**

```java
数据类型 变量名 = （数据类型）被强转数据值；
```

（1）当把存储范围大的值（常量值、变量的值、表达式计算的结果值）赋值给了存储范围小的变量时，需要强制类型转换，提示：有风险，可能会损失精度或溢出

```java
int i = (int)3.14;//强制类型转换，损失精度

double d = 1.2;
int num = (int)d;//损失精度

int i = 200;
byte b = (byte)i;//溢出
```

（2）当某个值想要提升数据类型时，也可以使用强制类型转换

```java
int i = 1;
int j = 2;
double shang = (double)i/j;
```

提示：这个情况的强制类型转换是没有风险的。

### 2.9.3 特殊的数据类型转换

任意数据类型的数据与String类型进行“+”运算时，结果一定是String类型

```java
System.out.println("" + 1 + 2);//12
```

### 练习

```java
1、练习题：判断如下代码是否编译通过，如果能，结果是多少？
short s1 = 120;
short s2 = 2;
short s3 = s1 + s2;

2、练习题：判断如下代码是否编译通过，如果能，结果是多少？
short s1 = 120;
short s2 = 2;
byte s3 = (byte)(s1 + s2);

3、练习题：判断如下代码是否编译通过，如果能，结果是多少？
char c1 = '0';
char c2 = '1';
char c3 = c1 + c2;

4、练习题：判断如下代码是否编译通过，如果能，结果是多少？
char c1 = '0';
char c2 = '1';
System.out.println(c1 + c2);

5、练习题：判断如下代码是否编译通过，如果能，结果是多少？
int i = 4;
long j = 120; //因为右边120默认是int类型，int的值赋值给long类型是可以的，会自动类型转换，但是要求这个int值不能超过int的存储范围，如果超过int的存储范围必须加L.
double d = 34;
float f = 1.2f;//因为右边1.2默认是double类型，double的值是不能直接赋值给float的，要么加F要么使用强制类型转换。

System.out.println(i + j + d + f);//最后是double

6、练习题：判断如下代码是否编译通过，如果能，结果是多少？
int i = 1;
int j = 2;
double d = i/j;
System.out.println(d);
```

## 2.10 运算符（Operator）

* **运算符：**是一种特殊的符号，用以表示数据的运算、赋值和比较等。
* **表达式：**用运算符连接起来的式子

### 2.10.1 **运算符的分类**

* 按照功能划分：

  |       分类       |                运算符                 |
  | :--------------: | :-----------------------------------: |
  |    算术运算符    |  `+`、`-`、`*`、`/`、`%`、`++`、`--`  |
  |    赋值运算符    |  `=`、`+=`、`-=`、`*=`、`/=`、`%=`等  |
  |    关系运算符    |   `>`、`>=`、`<`、`<=`、`==`、`!=`    |
  |    逻辑运算符    |    `&`、`|`、`^`、`!`、`&&`、`||`     |
  |    条件运算符    |     `(条件表达式)?结果1:结果2；`      |
  | 位运算符（了解） | `&`、`|`、`~`、`^`、`<<`、`>>`、`>>>` |

* 按照操作数个数划分：

  | 分类               | 运算符                  | 例子                    |
  | ------------------ | ----------------------- | ----------------------- |
  | 一元（单目）运算符 | ++、--、！              | i++、--i                |
  | 二元（双目）运算符 | +、-、*、/、%、>、<=等  | a+b、10>=9              |
  | 三元（三目）运算符 | 表达式1?表达式2:表达式3 | age>=18?"成年":"未成年" |

### 2.10.2 算术运算符

|  算术运算符   |            符号解释             |
| :-----------: | :-----------------------------: |
|      `+`      | 加法运算，字符串连接运算，正号  |
|      `-`      |         减法运算，负号          |
|      `*`      |            乘法运算             |
|      `/`      | 除法运算，整数/整数结果还是整数 |
|      `%`      | 求余运算，余数的符号只看被除数  |
| `++` 、  `--` |          自增自减运算           |

1. **加、减、乘、除、模**

   ```java
   public class OperatorDemo01 {
   	public static void main(String[] args) {
   		int a = 3;
   		int b = 4;
   		
   		System.out.println(a + b);// 7
   		System.out.println(a - b);// -1
   		System.out.println(a * b);// 12
   		System.out.println(a / b);// 计算机结果是0，为什么不是0.75呢？
   		System.out.println(a % b);// 3
           
           System.out.println(5%2);//1
   		System.out.println(5%-2);//1
   		System.out.println(-5%2);//-1
   		System.out.println(-5%-2);//-1		
   		//商*除数 + 余数 = 被除数
   		//5%-2  ==>商是-2，余数时1    (-2)*(-2)+1 = 5
   		//-5%2  ==>商是-2，余数是-1   (-2)*2+(-1) = -4-1=-5
   	}
   }
   ```

2. **“+”号的两种用法**

   - 第一种：对于`+`两边都是数值的话，`+`就是加法的意思

   - 第二种：对于`+`两边至少有一边是字符串得话，`+`就是拼接的意思

     ```java
     public class OperatorDemo02 {
     	public static void main(String[] args) {
     		// 字符串类型的变量基本使用
     		// 数据类型 变量名称 = 数据值;
     		String str1 = "Hello";
     		System.out.println(str1); // Hello
     		
     		System.out.println("Hello" + "World"); // HelloWorld
     		
     		String str2 = "Java";
     		// String + int --> String
     		System.out.println(str2 + 520); // Java520
     		// String + int + int
     		// String		+ int
     		// String
     		System.out.println(str2 + 5 + 20); // Java520
     	}
     }
     ```

3. **自加自减运算**

   **理解：**`++`  **运算，变量自己的值加1**。反之，`--` 运算，变量自己的值减少1，用法与`++` 一致。

   - **单独使用**

     - 变量在单独运算的时候，变量`前++`和变量`后++`，变量的是一样的；

     - 变量`前++`   ：例如 `++a` 。

     - 变量`后++`   ：例如 `a++` 。

       ```java
       public class OperatorDemo3 {
       	public static void main(String[] args) {
       		// 定义一个int类型的变量a
       		int a = 3;
       		//++a;
       		a++;
               // 无论是变量前++还是变量后++，结果都是4
       		System.out.println(a);
       	}
       }
       ```

   - **复合使用**

     - 和`其他变量放在一起使用`或者和`输出语句放在一起使用`，`前++`和`后++`就产生了不同。

     - 变量`前++` ：变量先自身加1，然后再取值。

     - 变量`后++` ：变量先取值，然后再自身加1。

       ```java
       public class OperatorDemo03 {
       	public static void main(String[] args) {
       		// 其他变量放在一起使用
       		int x = 3;
       		//int y = ++x; // y的值是4，x的值是4，
       		int y = x++; // y的值是3，x的值是4
       		
       		System.out.println(x);
       		System.out.println(y);
       		System.out.println("==========");
               
       		// 和输出语句一起
       		int z = 5;
       		//System.out.println(++z);// 输出结果是6，z的值也是6
       		System.out.println(z++);// 输出结果是5，z的值是6
       		System.out.println(z);
               
               int a = 1;
               a = a++;//(1)先取a的值“1”放操作数栈(2)a再自增,a=2(3)再把操作数栈中的"1"赋值给a,a=1
       
               int i = 1;
               int j = i++ + ++i * i++;
               /*
               从左往右加载
               (1)先算i++
               ①取i的值“1”放操作数栈
               ②i再自增 i=2
               （2）再算++i
               ①i先自增 i=3
               ②再取i的值“3”放操作数栈
               （3）再算i++
               ①取i的值“3”放操作数栈
               ②i再自增 i=4
               （4）先算乘法
               用操作数栈中3 * 3 = 9，并把9压会操作数栈
               （5）再算求和
               用操作数栈中的 1 + 9 = 10
               （6）最后算赋值
               j = 10
               */
       	} 
       }
       ```

   - 小结：

     - **++在前，先自加，后使用；**
     - **++在后，先使用，后自加。**

4. **练习**

   - （1）获取一个四位数的个位，十位，百位，千位

     ```java
     public class Test01 {
     	public static void main (String [] args) {
     		//1.定义一个四位数，例如1234
     		int num = 1234;
             
     		//2.通过运算操作求出个位，十位，百位，千位
     		int ge = ？
     		int shi = ？
     		int bai = ？
     		int qian = ？
     		
     		System.out.println(num + "这个四位数个位上的数字是：" + ge);
     		System.out.println(num + "这个四位数十位上的数字是：" + shi);
     		System.out.println(num + "这个四位数百位上的数字是：" + bai);
     		System.out.println(num + "这个四位数千位上的数字是：" + qian);
     	}
     }
     ```

   - （2）自增自减练习

     判断如下代码的运行结果

     ```java
     	public static void main(String[] args){
     		int i = 1;
     		int j = i++;
     		int k = i++ * ++j + ++i * j++;
     		
     		System.out.println("i = " + i);
     		System.out.println("j = " + j);
     		System.out.println("k = " + k);
     	}
     ```

     ```java
     	public static void main(String[] args){
     		int i = 1;
     		int j = i++;
     		int k = i++ * ++j + --i * j--;
     		
     		System.out.println("i = " + i);
     		System.out.println("j = " + j);
     		System.out.println("k = " + k);
     	}
     ```

     ```java
     	public static void main(String[] args){
     		int i = 1;
     		int j = ++i + i++ * ++i + i++;
     		
     		System.out.println("i = " + i);
     		System.out.println("j = " + j);
     	}
     ```

     ```java
     public static void main(String[] args){
     	int i = 0;
     	int result = ++i/--i;
     	System.out.println("result="+result);
     }
     ```


### 2.10.3 赋值运算符

注意：所有的赋值运算符的=左边一定是一个变量

| 赋值运算符 |                           符号解释                           |
| :--------: | :----------------------------------------------------------: |
|    `=`     |               将符号右边的值，赋值给左边的变量               |
|    `+=`    | 将符号**`左边的值`**和**`右边的值`**进行相加操作，最后将结果**`赋值给左边的变量`** |
|    `-=`    | 将符号**`左边的值`**和**`右边的值`**进行相减操作，最后将结果**`赋值给左边的变量`** |
|    `*=`    | 将符号**`左边的值`**和**`右边的值`**进行相乘操作，最后将结果**`赋值给左边的变量`** |
|    `/=`    | 将符号**`左边的值`**和**`右边的值`**进行相除操作，最后将结果**`赋值给左边的变量`** |
|    `%=`    | 将符号**`左边的值`**和**`右边的值`**进行取余操作，最后将结果**`赋值给左边的变量`** |

1. 基本赋值运算符课堂案例


   ```java
public class OperatorDemo04 {
	public static void main(String[] args) {
		int a = 3;
		int b = 4;
		a = a + b; 
		System.out.println(a); // 7
		System.out.println(b); // 4	
	}
}
   ```

2. 扩展赋值运算符课堂案例

   ```java
   public class OperatorDemo04 {
   	public static void main(String[] args) {
   		int a = 3;
   		int b = 4;
   		b += a;// 相当于 b = b + a ; 
   		System.out.println(a); // 3
   		System.out.println(b); // 7	
   		
   		short s = 3;
   		// s = s + 4; 代码编译报错，因为将int类型的结果赋值给short类型的变量s时，可能损失精度
   		s += 4; // 代码没有报错
           //因为在得到int类型的结果后，JVM自动完成一步强制类型转换，将int类型强转成short
   		System.out.println(s);
           
           int j = 1;
   		j += ++j * j++;//相当于  j = j + (++j * j++);
   		System.out.println(j);//5
   	}
   }
   ```

   扩展赋值运算符在**将最后的结果赋值给左边的变量前，都做了一步强制类型转换**。

3. 练习

   交换两个变量的值
   int m = 1;
   int n = 2;

   ```
   int temp=m;
   m=n;
   n=temp;
   ```

### 2.10.4 关系运算符/比较运算符

比较运算符，是两个数据之间进行比较的运算，运算结果一定是boolean值`true`或者`false` 。

| 关系运算符 |                           符号解释                           |
| :--------: | :----------------------------------------------------------: |
|    `<`     |  比较符号左边的数据是否小于右边的数据，如果小于结果是true。  |
|    `>`     |  比较符号左边的数据是否大于右边的数据，如果大于结果是true。  |
|    `<=`    | 比较符号左边的数据是否小于或者等于右边的数据，如果大于结果是false。 |
|    `>=`    | 比较符号左边的数据是否大于或者等于右边的数据，如果小于结果是false。 |
|    `==`    |          比较符号两边数据是否相等，相等结果是true。          |
|   `！=`    |     不等于符号 ，如果符号两边的数据不相等，结果是true。      |

- 课堂案例

  ```java
  public class OperatorDemo05 {
  	public static void main(String[] args) {
  		int a = 3;
  		int b = 4;
  
  		System.out.println(a < b); // true
  		System.out.println(a > b); // false
  		System.out.println(a <= b); // true
  		System.out.println(a >= b); // false
  		System.out.println(a == b); // false
  		System.out.println(a != b); // true
  	}
  }
  ```

- ​	练习：判断如下程序的运行结果

  ```java
  public static void main(String[] args){
  	int a = 1;
  	int b = 2;
  	int c = 0;
  	boolean flag = false;
  	if(flag=true){
  		c = a++ + b;
  	}
  
  	if(flag=false){
  		c = ++a - b;
  	}
  	System.out.println("a = " + a);
  	System.out.println("b = " + b);
  	System.out.println("c = " + c);
  }	
  ```

### 2.10.5 逻辑运算符

逻辑运算符，是用来连接两个布尔类型结果的运算符（`!`除外），运算结果一定是boolean值`true`或者`false`

| 逻辑运算符 |   符号解释   |               符号特点               |
| :--------: | :----------: | :----------------------------------: |
|    `&`     |    与，且    |          有`false`则`false`          |
|    `|`     |      或      |           有`true`则`true`           |
|    `^`     |     异或     |     相同为`false`，不同为`true`      |
|    `!`     |      非      | 非`false`则`true`，非`true`则`false` |
|    `&&`    | 双与，短路与 |      左边为false，则右边就不看       |
|    `||`    | 双或，短路或 |       左边为true，则右边就不看       |

1. **课堂案例**

   ```java
   public class OperatorDemo06 {
   	public static void main(String[] args) {
   		int a = 3;
   		int b = 4;
   		int c = 5;
   
   		// & 与，且；有false则false
   		System.out.println((a > b) & (a > c)); 
   		System.out.println((a > b) & (a < c)); 
   		System.out.println((a < b) & (a > c)); 
   		System.out.println((a < b) & (a < c)); 
   		System.out.println("===============");
   		// | 或；有true则true
   		System.out.println((a > b) | (a > c)); 
   		System.out.println((a > b) | (a < c)); 
   		System.out.println((a < b) | (a > c));
   		System.out.println((a < b) | (a < c));
   		System.out.println("===============");
   		// ^ 异或；相同为false，不同为true
   		System.out.println((a > b) ^ (a > c));
   		System.out.println((a > b) ^ (a < c)); 
   		System.out.println((a < b) ^ (a > c)); 
   		System.out.println((a < b) ^ (a < c)); 
   		System.out.println("===============");
   		// ! 非；非false则true，非true则false
   		System.out.println(!false);
   		System.out.println(!true);
   	}
   }
   ```

2. **&&和&区别，||和|区别**

   短路与，短路或运算符左边表达式结果可以确定最终结果，则运算符右边表达式不再进行运算，**效率高**

   - **`&&`和`&`**区别：

     `&&`和`&`结果一样，`&&`有短路效果，左边为false，右边不执行；`&`左边无论是什么，右边都会执行。

   - **`||`和`|`**区别：

     `||`和`|`结果一样，`||`有短路效果，左边为true，右边不执行；`|`左边无论是什么，右边都会执行。

3. **面试题1**

   ![1561431178935](Java SE笔记.assets/1561431178935.png)

   

   ```java
   public class LogicExer1{
   	public static void main(String[] args){
   		int x = 1;
   		int y = 1;
   
   		//x==2 ,x++  false  x = 2 左边为false
   		//右边继续
   		//++y  y==2  y=2  y==2成立  右边为true
   		//false & true 结果false
   		if(x++==2 & ++y==2){
   			x =7;
   		}
   		System.out.println("x="+x+",y="+y);//x=2,y=2
   	}
   }
   ```

   ```java
   public class LogicExer2{
   	public static void main(String[] args){
   		int x = 1,y = 1;
   
   		//x==2,x++  左边条件为false，x=2
   		//因为短路与，右边不算
   		//false && ? 结果是false
   		if(x++==2 && ++y==2){
   			x =7;
   		}
   		System.out.println("x="+x+",y="+y);//x=2,y=1
   	}
   }
   ```

   ```java
   public class LogicExer3{
   	public static void main(String[] args){
   		int x = 1,y = 1;
   
   		//x==1,x++  左边为true，x=2
   		//因为是逻辑与,右边继续  
   		//++y, y==1  y=2 右边为false
   		//条件true | false，最终为true
   		if(x++==1 | ++y==1){
   			x =7;
   		}
   		System.out.println("x="+x+",y="+y);//x=7,y=2
   	}
   }	
   ```

   ```java
   public class LogicExer4{
   	public static void main(String[] args){
   		int x = 1,y = 1;
   
   		//x==1,x++  左边为true，x=2
   		//因为是短路或，左边为true，右边就不看了
   		//整个条件为true
   		if(x++==1 || ++y==1){
   			x =7;
   		}
   		System.out.println("x="+x+",y="+y);//x=7,y=1
   
   	}
   }
   ```

   


4. **面试题2**

   ![1561431208735](Java SE笔记.assets/1561431208735.png)

   ```java
   public class LogicExer5{
   	public static void main (String []  args)  {
   		boolean x = true;
   		boolean y = false;
   		short z = 42;
   		
   		//如果if((z++==42)&&(y==true))条件成立，执行z++，不成立，就不执行z++
   		//左边的条件：z==42,z++  z==42成立,z++变成43
   		//中间虽然是短路与，因为左边现在是true,右边还要看
   		//右边 y==true   不成立
   		//true && false 结果为false
   		if((z++==42)&&(y==true))	z++;
   	
   		//左边为x=false,赋值  结果就为false
   		//中间虽然为短路或，因为左边是false,右边继续看
   		//++z,z==45  ++z变成44，z==45是否成立，不成立
   		//false || false  结果为false
   		if((x=false) || (++z==45))  z++;
   
   		System. out.println("z="+z);//44
   	}
   }
   ```

   ```java
   class  Test4_2  {
   	public static void main (String []  args)  {
   		boolean x = true;
   		boolean y = false;
   		short z = 42;
   		
   		//如果if(y=true)条件成立，接着判断if((z++==42)&&(y==true))	z++;	
   		//如果不成立，if((z++==42)&&(y==true))	z++;	不看的
   		/*
   		if(y = true)
   				
   		if((z++==42)&&(y==true))	z++;	
   		
   		if((x=false) || (++z==45))  z++;
   		*/
   		
   		//标准
   		//y=true赋值，y就被修改为true,if(true)成立
   		if(y=true){
   			//左边：z==42,z++  成立,z变成43
   			//&&短路与，不满足短路的情况，右边继续
   			//y==true 成立
   			//true && true，结果为true
   			if((z++==42)&&(y==true)){
   				//z++变成44
   				z++;
   			}
   		}
   		//左边：x=false不成立
   		//中间虽然是短路或，但是没满足短路的情况，右边继续
   		//++z,z==45  ++z变成45，z==45成立
   		if((x=false) || (++z==45)){
   			//z++，变成46
   			z++;
   		}
   		System. out.println("z="+z);//46
   	}
   }
   ```

   

### 2.10.6 条件运算符

- 条件运算符格式：

```java
条件表达式？结果1：结果2
```

- 条件运算符计算方式：
  - 条件判断的结果是true，条件运算符整体结果为结果1，赋值给变量。
  - 判断条件的结果是false，条件运算符整体结果为结果2，赋值给变量。

```java
public static void main(String[] args) {
    int i = (1==2 ? 100 : 200);
    System.out.println(i);//200
    int j = (3<=4 ? 500 : 600);
    System.out.println(j);//500
}
```

#### 练习

1、声明三个整型的变量,a,b,c,要求找出最大值
2、声明一个整型的变量，判断它是正数还是负数，还是0

### 2.10.7  位运算符（了解）

| 位运算符 |                    符号解释                    |
| :------: | :--------------------------------------------: |
|   `&`    |        按位与，当两位相同时为1时才返回1        |
|   `|`    |         按位或，只要有一位为1即可返回1         |
|   `~`    | 按位非，将操作数的每个位（包括符号位）全部取反 |
|   `^`    |    按位异或。当两位相同时返回0，不同时返回1    |
|   `<<`   |                   左移运算符                   |
|   `>>`   |                   右移运算符                   |
|  `>>>`   |                无符号右移运算符                |

- 位运算符的运算过程都是基于补码运算，但是看结果，我们得换成原码，再换成十进制看结果
- 从二进制到十进制都是基于原码
- byte,short,char在计算时按照int类型处理
- 位运算直接对二进制进行位移操作实现数值运算，所以运算效率高

> 如何区分&,|,^是逻辑运算符还是位运算符？
>
> 如果操作数是boolean类型，就是逻辑运算符，如果操作数是整数，那么就位运算符。

1. #### 左移：<<


   运算规则：左移几位就相当于乘以2的几次方

   **注意：**当左移的位数n超过该数据类型的总位数时，相当于左移（n-总位数）位

   byte,short,char在计算时按照int类型处理

   ```java
3<<4  类似于  3*2的4次= 3*16 = 48
   ```

![](Java SE笔记.assets/image-20200225113651675.png)

   ```java
-3<<4  类似于  -3*2的4次= -3*16 = -48
   ```

   ![image-20200225114707524](Java SE笔记.assets/image-20200225114707524.png)

   

2. #### 右移：>>

   快速运算：类似于除以2的n次，如果不能整除，**向下取整**

   ```java
   69>>4  类似于  69/2的4次 = 69/16 =4
   ```

   ![image-20200225115636844](Java SE笔记.assets/image-20200225115636844.png)

   ```
   -69>>4  类似于  -69/2的4次 = -69/16 = -5
   ```

   ![image-20200225120112188](Java SE笔记.assets/image-20200225120112188.png)

   

3. #### 无符号右移：>>>


   运算规则：往右移动后，左边空出来的位直接补0，不看符号位

   正数：和右移一样

   负数：右边移出去几位，左边补几个0，结果变为正数

   ```
69>>>4  类似于  69/2的4次 = 69/16 =4
   ```

   ![image-20200225121104734](Java SE笔记.assets/image-20200225121104734.png)

   ```
-69>>>4   结果：268435451
   ```

   ![image-20200225121244290](Java SE笔记.assets/image-20200225121244290.png)

   

4. #### 按位与：&


   运算规则：对应位都是1才为1

   1 & 1 结果为1

   1 & 0 结果为0

   0 & 1 结果为0

   0 & 0 结果为0

      4. 

   ```
9&7 = 1
   ```

   ![image-20200225122440953](Java SE笔记.assets/image-20200225122440953.png)

   ```
-9&7 = 7
   ```

   ![image-20200225122221616](Java SE笔记.assets/image-20200225122221616.png)

   

5. #### 按位或：|


   运算规则：对应位只要有1即为1

   1 | 1 结果为1

   1 | 0 结果为1

   0 | 1 结果为1

   0 & 0 结果为0

      5. 

   ```
9|7  结果： 15
   ```

   ![image-20200225122758851](Java SE笔记.assets/image-20200225122758851.png)

   ```
-9|7 结果： -9
   ```

   ![image-20200225123409130](Java SE笔记.assets/image-20200225123409130.png)

   

6. #### 按位异或：^


   运算规则：对应位一个为1一个为0，才为1

   1 ^ 1 结果为0

   0 ^ 1 结果为1

   0 ^ 0 结果为0

      6. 

   ```
9^7  结果为14
   ```

   ![image-20200225123445305](Java SE笔记.assets/image-20200225123445305.png)

   ```
-9^7 结果为-16
   ```

   ![image-20200225133145727](Java SE笔记.assets/image-20200225133145727.png)

   

7. #### 按位取反

   运算规则：

   ​	~0就是1  

   ​	~1就是0

   7. 

   ```java
   ~9  结果：-10
   ```

   ![image-20200225124112662](Java SE笔记.assets/image-20200225124112662.png)

   ```java
   ~-9  结果：8
   ```

   ![image-20200225124156862](Java SE笔记.assets/image-20200225124156862.png)

   

### 2.10.8 运算符优先级

![1553858424335](Java SE笔记.assets/1553858424335.png)

提示说明：

（1）表达式不要太复杂

（2）先算的使用()

大体的排序：算术 > 位  > 比较 > 逻辑 > 三元 > 赋值