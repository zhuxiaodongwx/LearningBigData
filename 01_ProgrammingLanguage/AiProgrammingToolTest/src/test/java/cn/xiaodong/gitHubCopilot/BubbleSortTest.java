package cn.xiaodong.gitHubCopilot;

import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;

public class BubbleSortTest {

    /**
     * 测试BubbleSort类的排序方法。
     * 通过多个测试用例验证排序算法的正确性。
     * 测试用例包括空数组、单个元素数组、已排序数组、逆序数组和包含重复元素的数组。
     */
    @Test
    public void testSort() {
        // 测试空数组
        int[] emptyArr = {};
        BubbleSort.sort(emptyArr);
        assertArrayEquals(new int[]{}, emptyArr);

        // 测试单个元素数组
        int[] singleElementArr = {4};
        BubbleSort.sort(singleElementArr);
        assertArrayEquals(new int[]{4}, singleElementArr);

        // 测试已排序数组
        int[] sortedArr = {1, 2, 3, 4, 5};
        BubbleSort.sort(sortedArr);
        assertArrayEquals(new int[]{1, 2, 3, 4, 5}, sortedArr);

        // 测试逆序数组
        int[] reverseArr = {5, 4, 3, 2, 1};
        BubbleSort.sort(reverseArr);
        assertArrayEquals(new int[]{1, 2, 3, 4, 5}, reverseArr);

        // 测试包含重复元素的数组
        int[] duplicateArr = {3, 1, 4, 1, 5, 9, 2, 6, 5};
        BubbleSort.sort(duplicateArr);
        assertArrayEquals(new int[]{1, 1, 2, 3, 4, 5, 5, 6, 9}, duplicateArr);
    }

}
