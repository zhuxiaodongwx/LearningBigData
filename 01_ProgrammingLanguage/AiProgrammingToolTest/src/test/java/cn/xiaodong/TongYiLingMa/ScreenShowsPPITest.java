package cn.xiaodong.TongYiLingMa;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ScreenShowsPPITest {

    // 测试方法1：测试一般情况
    @Test
    public void testGetPpiGeneralCase() {
        double x = 1920.0; // 屏幕长度像素
        double y = 1080.0; // 屏幕宽度像素
        double z = 5.5;    // 屏幕尺寸（英寸）
        double expectedPpi = 400.52857637859967; // 预期的PPI结果

        double actualPpi = ScreenShowsPPI.getPpi(x, y, z);
        assertEquals("General case failed", expectedPpi, actualPpi, 0.1);
    }

    // 测试方法2：测试屏幕尺寸为零的情况，期望抛出异常或某种错误处理
    @Test(expected = IllegalArgumentException.class)
    public void testGetPpiZeroSize() {
        double x = 1920.0;
        double y = 1080.0;
        double z = 0.0;    // 尺寸为零，应该引发异常或某种错误处理
        ScreenShowsPPI.getPpi(x, y, z);
    }

    // 测试方法3：测试屏幕像素为零的情况，期望得到特定的错误处理
    @Test(expected = IllegalArgumentException.class)
    public void testGetPpiZeroPixels() {
        double x = 0.0;    // 屏幕长度像素为零
        double y = 0.0;    // 屏幕宽度像素为零
        double z = 5.5;    // 屏幕尺寸（英寸）

        double actualPpi = ScreenShowsPPI.getPpi(x, y, z);
    }

    // 测试方法4：测试屏幕像素和尺寸均为零的情况
    @Test(expected = IllegalArgumentException.class)
    public void testGetPpiZeroPixelsAndSize() {
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        ScreenShowsPPI.getPpi(x, y, z);
    }
}
