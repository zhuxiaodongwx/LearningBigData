package cn.xiaodong.TongYiLingMa;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 数组生成测试
 */
public class SortingAlgorithmTest1 {

    @Test
    public void testGenerateRandomArrayLength() {
        // 测试数组长度是否正确
        int length = 10;
        int rangeMin = 0;
        int rangeMax = 100;
        int[] arr = SortingAlgorithm.generateRandomArray(length, rangeMin, rangeMax);

        assertEquals(length, arr.length, "Array length should be equal to the specified length");
    }

    @Test
    public void testGenerateRandomArrayRange() {
        // 测试数组元素是否在指定范围内
        int length = 100;
        int rangeMin = -50;
        int rangeMax = 50;
        int[] arr = SortingAlgorithm.generateRandomArray(length, rangeMin, rangeMax);

        for (int num : arr) {
            assertTrue(num >= rangeMin && num <= rangeMax, "Array element should be within the specified range");
        }
    }

    @Test
    public void testGenerateRandomArrayWithZeroRange() {
        // 测试范围为0时的情况
        int length = 5;
        int rangeMin = 10;
        int rangeMax = 10;
        int[] arr = SortingAlgorithm.generateRandomArray(length, rangeMin, rangeMax);

        for (int num : arr) {
            assertEquals(rangeMin, num, "Array element should be equal to rangeMin when range is 0");
        }
    }

    @Test
    public void testGenerateRandomArrayNegativeRange() {
        // 测试范围为负数时的情况，期望抛出异常
        int length = 10;
        int rangeMin = -10;
        int rangeMax = -20;

        assertThrows(IllegalArgumentException.class, () -> {
            SortingAlgorithm.generateRandomArray(length, rangeMin, rangeMax);
        }, "Should throw IllegalArgumentException for negative range");
    }
}
