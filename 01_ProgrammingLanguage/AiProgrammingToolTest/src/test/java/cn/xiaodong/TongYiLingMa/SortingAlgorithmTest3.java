package cn.xiaodong.TongYiLingMa;

import org.junit.Assert;
import org.junit.Test;

/**
 * 冒泡排序算法的单元测试
 */
public class SortingAlgorithmTest3 {

    /**
     * 测试随机数组的冒泡排序
     */
    @Test
    public void testBubbleSortWithRandomArray() {
        // 生成一个随机数组
        int[] arr = {4, 2, 9, 6, 23, 12, 34, 0, 1};
        SortingAlgorithm.bubbleSort(arr);
        // 检查数组是否按降序排序
        for (int i = 0; i < arr.length - 1; i++) {
            Assert.assertTrue("数组未按降序排序", arr[i] >= arr[i + 1]);
        }
    }

    /**
     * 测试已从小到大排序的数组
     */
    @Test
    public void testBubbleSortWithAlreadySortedArray() {
        // 生成一个已排序的数组
        int[] arr = {10, 20, 30, 40, 50};
        SortingAlgorithm.bubbleSort(arr);
        // 检查数组在降序排序后保持不变
        for (int i = 0; i < arr.length - 1; i++) {
            Assert.assertTrue("已排序数组应保持不变", arr[i] >= arr[i + 1]);
        }
    }

    /**
     * 测试已从大到小排序的数组
     */
    @Test
    public void testBubbleSortWithReverseSortedArray() {
        // 生成一个逆序排序的数组
        int[] arr = {50, 40, 30, 20, 10};
        SortingAlgorithm.bubbleSort(arr);
        // 检查数组现在是否按降序排序
        for (int i = 0; i < arr.length - 1; i++) {
            Assert.assertTrue("数组应按降序排序", arr[i] >= arr[i + 1]);
        }
    }

    /**
     * 测试空数组
     */
    @Test
    public void testBubbleSortWithEmptyArray() {
        // 生成一个空数组
        int[] arr = {};
        SortingAlgorithm.bubbleSort(arr);
        // 无内容可检查，但不应抛出异常
    }

    /**
     * 测试只有一个元素的数组
     */
    @Test
    public void testBubbleSortWithSingleElementArray() {
        // 生成一个只有一个元素的数组
        int[] arr = {5};
        SortingAlgorithm.bubbleSort(arr);
        // 无内容可检查，但不应抛出异常
    }
}
