package cn.xiaodong.TongYiLingMa;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.Assert.assertEquals;

/**
 * 数组打印测数
 */
public class SortingAlgorithmTest2 {

    private final PrintStream originalOut = System.out;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));  // Redirect System.out to capture output
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);  // Restore System.out
    }

    @Test
    public void testPrintArray() {
        int[] arr = {1, 2, 3, 4, 5};
        SortingAlgorithm.printArray(arr);
        assertEquals("1 2 3 4 5 \r\n", outContent.toString());
    }

    @Test
    public void testPrintEmptyArray() {
        int[] arr = {};
        SortingAlgorithm.printArray(arr);
        assertEquals("\r\n", outContent.toString()); // Expecting a blank line
    }

    @Test
    public void testPrintArrayWithNegativeNumbers() {
        int[] arr = {-1, -2, -3, -4, -5};
        SortingAlgorithm.printArray(arr);
        assertEquals("-1 -2 -3 -4 -5 \r\n", outContent.toString());
    }
}
