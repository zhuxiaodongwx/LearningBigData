package cn.xiaodong.TongYiLingMa;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * 水仙花数测试类（三位数）
 */
public class NumberOfDaffodilsTest1 {

    @Test
    @DisplayName("测试水仙花数")
    public void testIsNarcissistic3() {
        // 测试一些已知的水仙花数
        assertTrue(NumberOfDaffodils.isNarcissistic3(153), "153应该是一个水仙花数");
        assertTrue(NumberOfDaffodils.isNarcissistic3(370), "370应该是一个水仙花数");
        assertTrue(NumberOfDaffodils.isNarcissistic3(371), "371应该是一个水仙花数");

        // 测试一些已知的非水仙花数
        assertFalse(NumberOfDaffodils.isNarcissistic3(163), "163不应该是一个水仙花数");
        assertFalse(NumberOfDaffodils.isNarcissistic3(820), "820不应该是一个水仙花数");

        // 测试边界条件
        assertThrows(IllegalArgumentException.class, () -> NumberOfDaffodils.isNarcissistic3(99),
                "输入小于100应该抛出IllegalArgumentException");
        assertThrows(IllegalArgumentException.class, () -> NumberOfDaffodils.isNarcissistic3(1000),
                "输入大于999应该抛出IllegalArgumentException");
    }
}
