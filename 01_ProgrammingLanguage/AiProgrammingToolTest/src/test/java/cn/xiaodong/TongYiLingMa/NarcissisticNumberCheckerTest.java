package cn.xiaodong.TongYiLingMa;

import org.junit.Assert;
import org.junit.Test;

public class NarcissisticNumberCheckerTest {

    // 测试正面案例：验证已知的水仙花数判断为真
    @Test
    public void testIsNarcissisticNumber_Positive() {
        // 验证0是水仙花数
        Assert.assertTrue("0应该是水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(0));
        // 验证153是水仙花数
        Assert.assertTrue("153应该是水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(153));
        // 验证370是水仙花数
        Assert.assertTrue("370应该是水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(370));
        // 验证371是水仙花数
        Assert.assertTrue("371应该是水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(371));
    }

    // 测试反面案例：验证非水仙花数判断为假
    @Test
    public void testIsNarcissisticNumber_Negative() {
        // 验证1634不是水仙花数（注意：实际上1634是一个水仙花数，此测试用例可能用于演示，实际情况应调整）
        Assert.assertFalse("1633不应该为水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(1633));
        // 验证8209不是水仙花数（注意：实际上8208是一个水仙花数，此测试用例可能用于演示，实际情况应调整）
        Assert.assertFalse("8209不应该为水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(8209));
        // 验证9475不是水仙花数（同上，9474实际上是水仙花数，此例应根据实际测试意图调整）
        Assert.assertFalse("9475不应该为水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(9475));
    }

    // 边界值测试：验证极值情况
    @Test
    public void testIsNarcissisticNumber_Boundary() {
        // 验证负数-1不是水仙花数
        Assert.assertFalse("负数-1不应视为水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(-1));
        // 验证最小负整数不是水仙花数
        Assert.assertFalse("最小负整数不应为水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(Integer.MIN_VALUE));
        // 验证最小正整数1是水仙花数
        Assert.assertTrue("最小正整数1应该是水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(1));
        // 验证最大正整数不是水仙花数
        Assert.assertFalse("最大正整数不应为水仙花数", NarcissisticNumberChecker.isNarcissisticNumber(Integer.MAX_VALUE));
    }
}

