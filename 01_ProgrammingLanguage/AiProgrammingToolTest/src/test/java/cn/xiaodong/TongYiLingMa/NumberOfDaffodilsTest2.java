package cn.xiaodong.TongYiLingMa;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * 水仙花数测试类（四位数）
 */
public class NumberOfDaffodilsTest2 {

    @Test
    public void testIsNarcissistic4() {
        // 测试有效的四位数水仙花数
        assertTrue("1634 应该是一个水仙花数", NumberOfDaffodils.isNarcissistic4(1634));
        assertTrue("8208 应该是一个水仙花数", NumberOfDaffodils.isNarcissistic4(8208));
        assertTrue("9474 应该是一个水仙花数", NumberOfDaffodils.isNarcissistic4(9474));

        // 测试有效的非四位数水仙花数
        assertFalse("1231 不应该是一个水仙花数", NumberOfDaffodils.isNarcissistic4(1231));
        assertFalse("8901 不应该是一个水仙花数", NumberOfDaffodils.isNarcissistic4(8901));

        // 测试四位数边界的非水仙花数
        assertFalse("1000 不应该是一个水仙花数", NumberOfDaffodils.isNarcissistic4(1000));
        assertFalse("9999 不应该是一个水仙花数", NumberOfDaffodils.isNarcissistic4(9999));

        // 测试无效输入（非四位数）
        try {
            NumberOfDaffodils.isNarcissistic4(0);
            fail("预期会抛出 IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // 预期的异常
        }

        try {
            NumberOfDaffodils.isNarcissistic4(99); // 小于 1000
            fail("预期会抛出 IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // 预期的异常
        }

        try {
            NumberOfDaffodils.isNarcissistic4(10000); // 大于 9999
            fail("预期会抛出 IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // 预期的异常
        }
    }
}
