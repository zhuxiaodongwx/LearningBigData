package cn.xiaodong.TongYiLingMa;


/**
 * 水仙花数
 * <p>
 * <p>
 * 水仙花数（Narcissistic number）是数学中的一个概念，它指的是一个正整数，其每个数字的位数的幂之和等于该数本身。对于三位数的水仙花数来说，就是指这个数的每一位数字的三次幂的和等于这个数。例如，153是一个水仙花数，因为 (1^3 + 5^3 + 3^3 = 153)。
 * 更一般地，n位的水仙花数是指一个n位的正整数，它的每一位数字的n次幂的和等于这个数本身。水仙花数有时也称为阿姆斯特朗数，以数学家R. Nelson Armstrong的名字命名，尽管这个术语并不是他创造的。
 * 数学公式表示为： 若一个n位数 ( a_n \times 10^{n-1} + a_{n-1} \times 10^{n-2} + \ldots + a_1 \times 10^0 ) 是水仙花数，那么： [ \sum_{i=1}^{n}(a_i)^n = a_1 \times 10^{n-1} + a_2 \times 10^{n-2} + \ldots + a_{n-1} \times 10^0 + a_n ]
 * 例如，对于三位数的水仙花数，条件是： [ (a_1)^3 + (a_2)^3 + (a_3)^3 = a_1 \times 10^2 + a_2 \times 10^1 + a_3 \times 10^0 ]
 * 已知的三位水仙花数包括153, 370, 371, 407等。
 */
public class NumberOfDaffodils {


    /**
     * 编写一个函数，判断一个数（三位数）是不是获取水仙花数
     * 是返回true
     * 不是返回false
     */
    public static boolean isNarcissistic3(int num) {
        if (num < 100 || num > 999) {
           throw new IllegalArgumentException("参数必须为一个三位数整数" );
        }

        int a = num / 100;
        int b = num / 10 % 10;
        int c = num % 10;
        if (a * a * a + b * b * b + c * c * c == num) {
            return true;
        }
        return false;
    }

    /**
     * 编写一个函数，判断一个数（四位数）是不是获取水仙花数
     * 是返回true
     * 不是返回false
     */
    public static boolean isNarcissistic4(int number) {
        // Check if the number is a four-digit number
        if (number < 1000 || number > 9999) {
            throw new IllegalArgumentException("The number must be a four-digit number.");
        }

        // Get the individual digits of the number
        int thousands = number / 1000;
        int hundreds = (number / 100) % 10;
        int tens = (number / 10) % 10;
        int units = number % 10;

        // Calculate the sum of the fourth powers of the digits
        int sumOfPowers = (int) (Math.pow(thousands, 4) + Math.pow(hundreds, 4) +
                Math.pow(tens, 4) + Math.pow(units, 4));

        // Check if the sum of the powers is equal to the original number
        return sumOfPowers == number;
    }


    /**
     * 寻找三位数、四位数的水仙花数
     *
     * @param args
     */
    public static void main(String[] args) {
        for (int i = 100; i < 1000; i++) {
            if (isNarcissistic3(i)) {
                System.out.println(i);
            }
        }

        /**
         * 寻找四位数的水仙花数
         *
         */
        for (int i = 1000; i < 10000; i++) {
            if (isNarcissistic4(i)) {
                System.out.println(i);
            }
        }
    }
}
