package cn.xiaodong.TongYiLingMa;

/**
 * 计算屏幕显示PPI
 */
public class ScreenShowsPPI {


    public static void main(String[] args) {
        double ppi = getPpi(1920, 1080, 24.5);
        System.out.println("屏幕显示PPI为：" + ppi);

        ppi = getPpi(2560, 1440, 27);
        System.out.println("屏幕显示PPI为：" + ppi);

        ppi = getPpi(3840, 2160, 42);
        System.out.println("屏幕显示PPI为：" + ppi);
    }

    /**
     * 计算屏幕的每英寸像素数（PPI）。
     *
     * @param x 屏幕的水平像素分辨率。
     * @param y 屏幕的垂直像素分辨率。
     * @param z 屏幕的对角线长度（以英寸为单位）。
     * @return 屏幕的PPI值。
     * @throws IllegalArgumentException 如果x、y或z的值小于或等于零，则抛出此异常。
     */
    public static double getPpi(double x, double y, double z) {
        // 判断输入参数是否合法
        if (x <= 0 || y <= 0 || z <= 0) {
            throw new IllegalArgumentException("输入参数不合法");
        }

        // 根据屏幕的像素分辨率和对角线长度计算PPI
        double ppi = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) / z;
        return ppi;
    }
}
