package cn.xiaodong.TongYiLingMa;

/**
 * 排序算法
 * SortingAlgorithm
 */
public class SortingAlgorithm {

    /**
     * 生成一个随机数组，数组的长度为length，每个元素的随机范围为[rangeMin, rangeMax]
     */
    public static int[] generateRandomArray(int length, int rangeMin, int rangeMax) {
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = (int) (Math.random() * (rangeMax - rangeMin + 1) + rangeMin);
        }
        return arr;
    }

    /**
     * 使用冒泡排序，对数组进行排序
     * 从大到小排序
     */
    public static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] < arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    /**
     * 对整数数组，遍历输出内容
     */
    public static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    /**
     * 主程序
     * 1、调用函数，生成随机数组
     * 2、打印生成的数组
     * 3、冒泡排序
     * 4、输出排序后的数组
     */
    public static void main(String[] args) {
        /**
         * 生成一个包含随机整数的数组，并打印数组。
         * 首先，通过调用generateRandomArray函数生成一个长度为10，元素范围在1到100之间的随机整数数组。
         * 然后，使用printArray函数打印这个数组，展示排序前的状态。
         * 接着，调用bubbleSort函数对数组进行冒泡排序。
         * 最后，再次使用printArray函数打印排序后的数组，展示排序后的状态。
         */
        int[] arr = generateRandomArray(10, 1, 100);
        printArray(arr);
        bubbleSort(arr);
        printArray(arr);

    }
}
