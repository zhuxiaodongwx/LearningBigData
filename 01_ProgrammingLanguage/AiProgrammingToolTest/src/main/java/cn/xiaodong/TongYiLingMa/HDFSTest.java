package cn.xiaodong.TongYiLingMa;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;

/**
 * hadoop HDFS操作
 */
public class HDFSTest {

    /**
     * 本地存储路径
     */
    static String localPath = "D:\\test.txt";
    /**
     * HDFS存储路径
     */
    static String hdfsPath = "/test";


    /**
     * 上传文件到HDFS
     *
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        // 创建配置对象，用于设置Hadoop文件系统的配置信息
        Configuration conf = new Configuration();

        // 设置默认文件系统的地址和端口，指定HDFS的名称节点地址和端口
        conf.set("fs.defaultFS", "hdfs://hadoop102:8020");

        // 注释掉的代码用于设置HDFS数据复制的因子，这里未设置，默认为3
//        conf.set("dfs.replication", "2");

        // 根据配置信息获取文件系统实例，指定用户名为"hadoop"
        FileSystem fs = FileSystem.get(URI.create("hdfs://hadoop102:8020"), conf, "hadoop");

        // 从本地路径复制文件到HDFS路径
        fs.copyFromLocalFile(new Path(localPath), new Path(hdfsPath));

        // 关闭文件系统实例，释放资源
        fs.close();

    }
}
