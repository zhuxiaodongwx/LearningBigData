package cn.xiaodong.TongYiLingMa;

/**
 * java编写一个函数，判断一个整数（位数不确定）是不是获取水仙花数
 * 是返回true
 * 不是返回false
 */
public class NarcissisticNumberChecker {

    public static boolean isNarcissisticNumber(int number) {
        // 将数字转换为字符串，以便获取位数
        String numStr = Integer.toString(number);
        int length = numStr.length();
        int sum = 0;

        for (int i = 0; i < length; i++) {
            // 获取当前位上的数字
            int digit = Character.getNumericValue(numStr.charAt(i));
            // 计算该位数字的幂次和
            sum += Math.pow(digit, length);
        }

        // 判断是否等于原数
        return sum == number;
    }

    public static void main(String[] args) {
        int testNumber = 153; // 用于测试的水仙花数
        System.out.println(isNarcissisticNumber(testNumber) ? testNumber + "是水仙花数" : testNumber + "不是水仙花数");

        testNumber = 1634; // 非水仙花数示例
        System.out.println(isNarcissisticNumber(testNumber) ? testNumber + "是水仙花数" : testNumber + "不是水仙花数");

        testNumber = 10086; // 非水仙花数示例
        System.out.println(isNarcissisticNumber(testNumber) ? testNumber + "是水仙花数" : testNumber + "不是水仙花数");
    }
}

