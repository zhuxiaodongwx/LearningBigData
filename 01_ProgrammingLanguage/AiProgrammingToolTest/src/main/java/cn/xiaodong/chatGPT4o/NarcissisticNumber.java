package cn.xiaodong.chatGPT4o;

/**
 * NarcissisticNumber 类提供了一个方法来检查一个数字是否为水仙花数。
 * 水仙花数是指一个n位数，它的各位数字的n次幂之和等于它本身的整数。
 */
public class NarcissisticNumber {

    /**
     * 判断给定的整数是否为水仙花数。
     *
     * @param number 要检查的整数
     * @return 如果该数字是水仙花数则返回true，否则返回false
     */
    // 用于检查数字是否为水仙花数的函数
    public static boolean isNarcissistic(int number) {
        // 将数字转换为字符串以便于提取每个数字
        String numberStr = String.valueOf(number);
        // 获取数字的位数
        int numDigits = numberStr.length();
        // 初始化求和变量，用于计算每位数字的n次幂之和
        int sum = 0;

        // 遍历数字的每一位
        // 计算每位数字的n次幂之和
        for (int i = 0; i < numDigits; i++) {
            // 将字符形式的数字转换为整型
            int digit = Character.getNumericValue(numberStr.charAt(i));
            // 将该位数字的n次幂累加到sum中
            sum += Math.pow(digit, numDigits);
        }

        // 检查求得的和是否与原数字相等，并返回结果
        // 若和等于原数字，则为水仙花数
        return sum == number;
    }

    /**
     * 主函数用于测试给出的数字是否为水仙花数。
     *
     * @param args 命令行参数
     */
    public static void main(String[] args) {
        // 定义一个测试用的数字
        int testNumber = 153; // 示例数字
        // 检查该测试数字是否为水仙花数
        if (isNarcissistic(testNumber)) {
            // 输出是水仙花数的结果
            System.out.println(testNumber + " 是一个水仙花数。");
        } else {
            // 输出不是水仙花数的结果
            System.out.println(testNumber + " 不是一个水仙花数。");
        }
    }
}
