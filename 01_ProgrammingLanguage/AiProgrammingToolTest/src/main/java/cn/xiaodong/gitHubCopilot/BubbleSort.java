package cn.xiaodong.gitHubCopilot;

import java.util.Scanner;

/**
 * 对一个数组进行冒泡排序
 *
 * 作者：GitHub Copilot
 */
public class BubbleSort {

    public static void main(String[] args) {
        // 输入一个数组，进行冒泡排序，从小到大输出
        int[] arr = input();
        // 冒泡排序
        sort(arr);
        // 打印数组
        print(arr);
    }

    /**
     * 输入一个数组，进行冒泡排序，从小到大输出
      */
    public static void sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    /**
     * 键盘录入一个数组，返回一个数组
     * @return
     */
    public static int[] input() {
        // 录入数组的长度
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入数组的长度：");
        int len = sc.nextInt();
        int[] arr = new int[len];

        // 录入数组的元素
        System.out.println("请输入数组的元素：");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        return arr;
    }

    /**
     * 打印数组
     * @param arr
     */
    public static void print(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }
    }
}