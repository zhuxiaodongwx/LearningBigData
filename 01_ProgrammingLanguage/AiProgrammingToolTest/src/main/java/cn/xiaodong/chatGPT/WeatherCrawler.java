package cn.xiaodong.chatGPT;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 获取天气
 */
public class WeatherCrawler {
    public static void main(String[] args) {
        try {
            // 创建URL对象
            URL url = new URL("http://www.weather.com.cn/weather/101010400.shtml");

            // 打开连接
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // 设置请求参数
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            // 获取响应数据
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null) {
                response.append(line);
            }
            in.close();

            // 解析响应数据
            String html = response.toString();
            String weather = html.substring(html.indexOf("<ul class=\"t clearfix\">"), html.indexOf("</ul>", html.indexOf("<ul class=\"t clearfix\">")));

            // 输出结果
            System.out.println(weather);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
