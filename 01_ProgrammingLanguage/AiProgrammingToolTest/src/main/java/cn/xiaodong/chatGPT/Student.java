package cn.xiaodong.chatGPT;

// 定义一个学生类，用于存储学生姓名和成绩
class Student {
    private String name;
    private double chineseScore;
    private double mathScore;
    private double englishScore;

    public Student(String name, double chineseScore, double mathScore, double englishScore) {
        this.name = name;
        this.chineseScore = chineseScore;
        this.mathScore = mathScore;
        this.englishScore = englishScore;
    }

    public String getName() {
        return name;
    }

    public double getChineseScore() {
        return chineseScore;
    }

    public double getMathScore() {
        return mathScore;
    }

    public double getEnglishScore() {
        return englishScore;
    }
}
