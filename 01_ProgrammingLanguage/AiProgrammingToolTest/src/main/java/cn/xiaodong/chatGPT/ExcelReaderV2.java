package cn.xiaodong.chatGPT;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/**
 * 需求：
 * java读取Excel文件，文件地址：“D:\LearningBigData\01_ProgrammingLanguage\AiProgrammingToolTest\studentScore.xlsx”
 * 读取工作簿“期末考试成绩”
 * 第一行为表头
 * 第一列为学生姓名，第二列为语文成绩，第三列为数学成绩，第四列为英语成绩
 * 计算语文、数学、英语三科成绩的平均数、中位数并输出
 * <p>
 * <p>
 * 代码编写：
 * chatGPT3.5
 *
 * 人工干预：
 * 1、项目添加maven依赖，maven添加poi、poi-ooxml支持（GPT生成）
 * 2、调用GPT生成代码
 */

public class ExcelReaderV2 {
    public static void main(String[] args) {
        try {
            // 创建一个文件对象，指定要读取的Excel文件路径
            File file = new File("D:\\LearningBigData\\01_ProgrammingLanguage\\AiProgrammingToolTest\\studentScore.xlsx");

            if (!file.exists() || !file.canRead()) {
                throw new Exception("File not found or cannot be read");
            }

            // 创建一个文件输入流对象，用于读取Excel文件
            FileInputStream inputStream = new FileInputStream(file);

            // 使用POI工厂类创建一个Excel工作簿对象
            Workbook workbook = WorkbookFactory.create(inputStream);

            // 获取Excel文件中名为“期末考试成绩”的工作表
            Sheet sheet = workbook.getSheet("期末考试成绩");

            // 获取工作表中的所有行
            Iterator<Row> rowIterator = sheet.iterator();

            // 创建列表用于存储所有学生的成绩
            List<Student> students = new ArrayList<Student>();

            // 遍历每一行，将每个学生的成绩存储到列表中
            while (rowIterator.hasNext()) {
                // 获取当前行
                Row row = rowIterator.next();

                // 跳过第一行，即表头
                if (row.getRowNum() == 0) {
                    continue;
                }

                // 获取当前行中的学生姓名、语文成绩、数学成绩、英语成绩
                String name = row.getCell(0).getStringCellValue();
                double chineseScore = row.getCell(1).getNumericCellValue();
                double mathScore = row.getCell(2).getNumericCellValue();
                double englishScore = row.getCell(3).getNumericCellValue();

                // 创建学生对象，并将其成绩添加到学生列表中
                Student student = new Student(name, chineseScore, mathScore, englishScore);
                students.add(student);
            }

            // 计算语文、数学、英语三科成绩的平均数和中位数
            double chineseAvg = 0.0;
            double mathAvg = 0.0;
            double englishAvg = 0.0;
            List<Double> chineseScores = new ArrayList<Double>();
            List<Double> mathScores = new ArrayList<Double>();
            List<Double> englishScores = new ArrayList<Double>();

            for (Student student : students) {
                chineseAvg += student.getChineseScore();
                mathAvg += student.getMathScore();
                englishAvg += student.getEnglishScore();

                chineseScores.add(student.getChineseScore());
                mathScores.add(student.getMathScore());
                englishScores.add(student.getEnglishScore());
            }

            chineseAvg /= students.size();
            mathAvg /= students.size();
            englishAvg /= students.size();

            Collections.sort(chineseScores);
            Collections.sort(mathScores);
            Collections.sort(englishScores);

            double chineseMedian = chineseScores.get(students.size() / 2);
            double mathMedian = mathScores.get(students.size() / 2);
            double englishMedian = englishScores.get(students.size() / 2);

//            // 输出平均数和中位数
//            System.out.println

            System.out.println("语文平均分：" + chineseAvg);
            System.out.println("数学平均分：" + mathAvg);
            System.out.println("英语平均分：" + englishAvg);

            System.out.println("语文中位数：" + chineseMedian);
            System.out.println("数学中位数：" + mathMedian);
            System.out.println("英语中位数：" + englishMedian);

            // 关闭工作簿和文件输入流
            workbook.close();
            inputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}