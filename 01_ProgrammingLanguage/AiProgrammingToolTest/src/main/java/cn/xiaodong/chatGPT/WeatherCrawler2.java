package cn.xiaodong.chatGPT;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 获取天气
 */
public class WeatherCrawler2 {
    public static void main(String[] args) {
        try {
            // 获取当前时间
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
            String today = sdf.format(new Date());

            // 创建URL对象
            URL url = new URL("http://www.weather.com.cn/weather/101010400.shtml");

            // 打开连接
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // 设置请求参数
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            // 获取响应数据
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null) {
                response.append(line);
            }
            in.close();

            // 解析响应数据
            String html = response.toString();
            String weather = html.substring(html.indexOf("<ul class=\"t clearfix\">"), html.indexOf("</ul>", html.indexOf("<ul class=\"t clearfix\">")));
            Pattern pattern = Pattern.compile("<h1>.*?</h1>");
            Matcher matcher = pattern.matcher(html);
            String city = "";
            if (matcher.find()) {
                city = matcher.group().replaceAll("<.*?>", "");
            }
            pattern = Pattern.compile(today + ".*?</p>");
            matcher = pattern.matcher(weather);
            String todayWeather = "";
            if (matcher.find()) {
                todayWeather = matcher.group().replaceAll("<.*?>", "").replaceAll("&nbsp;", " ");
            }

            // 输出结果
            System.out.println(city + "今天的天气情况：" + todayWeather);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
