package cn.xiaodong.chatGPT;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


/**
 * 需求：
 * java读取Excel文件，类名ExcelReaderV1
 * <p>
 * 代码编写：
 * chatGPT3.5
 *
 */
public class ExcelReaderV1 {
    public static void main(String[] args) {
        try {
            // 创建一个文件对象，指定要读取的Excel文件路径
            File file = new File("example.xlsx");

            // 创建一个文件输入流对象，用于读取Excel文件
            FileInputStream inputStream = new FileInputStream(file);

            // 使用POI工厂类创建一个Excel工作簿对象
            Workbook workbook = WorkbookFactory.create(inputStream);

            // 获取Excel文件的第一个工作表
            Sheet sheet = workbook.getSheetAt(0);

            // 获取工作表中的所有行
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                // 获取当前行
                Row row = rowIterator.next();

                // 获取当前行中的所有单元格
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    // 获取当前单元格
                    Cell cell = cellIterator.next();

                    // 输出当前单元格中的值
                    System.out.print(cell.toString() + "\t");
                }
                System.out.println();
            }

            // 关闭输入流和工作簿对象
            inputStream.close();
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
